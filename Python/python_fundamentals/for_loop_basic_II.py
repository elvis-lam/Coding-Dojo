# Biggie Size - Given an array, write a function that changes all positive numbers in the array to "big". Example: makeItBig([-1, 3, 5, -5]) returns that
# same array, changed to [-1, "big", "big", -5].
def make_it_big(array):
    for num in range(0, len(array)):
        if array[num] > 0:
            array[num] = "big"
    return array
# make_it_big([-1, 3, 5, -5])

# Count Positives - Given an array of numbers, create a function to replace last value with number of positive values. Example, countPositives([-1,1,1,1])
# changes array to [-1,1,1,3] and returns it.  (Note that zero is not considered to b a positive number).
def count_positives(array):
    last = 0
    for num in range(0, len(array)):
        if array[num] > 0:
            last +=1
        array[len(array)-1] = last
    return array
# count_positives([-1,1,1,1])

# SumTotal - Create a function that takes an array as an argument and returns the sum of all the values in the array.  For example sumTotal([1,2,3,4]) should return 10
def sum_total(array):
    return sum(array)
# sum_total([1,2,3,4])

# Average - Create a function that takes an array as an argument and returns the average of all the values in the array.  For example multiples([1,2,3,4]) should return 2.5
def multiples(array):
    avg = (sum(array)) // len(array)
    print(avg)
# multiples([1,2,3,4])

# Length - Create a function that takes an array as an argument and returns the length of the array.  For example length([1,2,3,4]) should return 4
def length(array):
    return len(array)
# length([1,2,3,4])

# Minimum - Create a function that takes an array as an argument and returns the minimum value in the array.  If the passed array is empty, have the function
# return false.  For example minimum([1,2,3,4]) should return 1; minimum([-1,-2,-3]) should return -3.
def minimum(array):
    return min(array)
# minimum([-1,-2,-3])

# Maximum - Create a function that takes an array as an argument and returns the maximum value in the array.  If the passed array is empty, have the function
# return false.  For example maximum([1,2,3,4]) should return 4; maximum([-1,-2,-3]) should return -3.
def maximum(array):
    return max(array)
# maximum([1,2,3,4])
# maximum([-1,-2,-3])

# UltimateAnalyze - Create a function that takes an array as an argument and returns a dictionary that has the sumTotal, average, minimum, maximum and length of the array.
def sum_total(array):
    results = {
        'max': max(array),
        'min': min(array),
        'average': (sum(array)) // len(array)
    }
    return results
# sum_total([6,3,2])

# ReverseList - Create a function that takes an array as an argument and return an array in a reversed order.  Do this without creating an empty temporary array.
# For example reverse([1,2,3,4]) should return [4,3,2,1]. This challenge is known to appear during basic technical interviews.
def reverse_list(array):
    for i in range(0, (len(array) // 2)):
        temp = array[i]
        array[i] = array[len(array)- i - 1]
        array[len(array)- i - 1] = temp
    print(array)
reverse_list([1,2,3,4])
