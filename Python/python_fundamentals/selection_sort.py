# If you're given a list with a bunch of numbers and you're supposed to sort the numbers (with the smallest number on the left and the largest number on the right),
# how would you do this? There are numerous sorting algorithms to sort numbers in the list. We'll introduce one of the simplest sorting algorithm called selection sort.

def select_sort(list):
    # Decrement the list and screen each value at the last element
    for x in range(len(list)-1, 0, -1):
        # Max will determine if the next value is bigger than max
        max = 0
        # Screen and loop through each element to find the max
        for y in range(1, x+1):
            if list[y] > list[max]:
                max = y
                # If max is found, break out the statement and swap the elements with a temp variable
        temp = list[x]
        list[x] = list[max]
        list[max] = temp
    return list


select_sort([10,67,40,30,46,11,23])
