# Print all numbers/ integers from 0 to 150
for a in range(150):
    print(a)

# Print all multiples of 5 from 5 to 1000000
for b in range(5, 100000):
    if b % 5 == 0:
        print(b)

# Print integers 1 to 100. If divisible by 5, print "Coding" instead. If by 10, print "Dojo"
for c in range(1, 100):
    if c % 10 == 0:
        print("Dojo")
        print(c)
    elif c % 5 == 0:
        print("Coding")
        print(c)

# Add odd intgers from 0 to 5000000, print the final sum
sum = 0
for d in range(0, 500000):
    sum = sum + d
print(sum)

# Print positive numbers starting at 2018, counting down by fours (exclude 0)
e = 2018
while (e > 0):
    print(e)
    e -= 4

# Given lowNum, highNum, Mult, print multiples of mult from lowNum to highNum, using a FOR loop. For (2, 9, 3), print 3 6 9
lowNum = 2
highNum = 9
mult = 3
for f in range(lowNum, highNum+1):
    if f % mult == 0:
        print(f)
