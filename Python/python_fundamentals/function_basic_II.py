# # Create a function that accepts a number as an input. Return a new array that counts down by one, from the number (as arrays 'zero'th element) down to 0
# # (as the last element). For example countDown(5) should return [5,4,3,2,1,0]
def countdown(num):
    array = []
    while num > 0:
        array.append(num)
        num = num - 1
    return array
# # countdown(5)
#
# # Your function will receive an array with two numbers. Print the first value and return the second
def print_return(array):
    print(array[0])
    return array[1]
# # print_return([1,2,3,4])
#
# # Given an array, return the sum of the first value in the array, plus the array's length
def total(array):
    return array[0] + len(array)
#
# # total([1,2,3,4,5])
#
# # Write a function that accepts any array, and returns a new array with the array values that are greater than its 2nd value, Print how many values this is
# # If the array is only element long, have the function return False
def greater(array):
    new_array = []
    total = 0
    for value in array:
        if len(array) == 1:
            rturn "false"
        elif value > array[1]:
            new_array.append(value)
            total += 1
    rturn new_array, "This array has a total of", total
# # greater([7,2,4,5,])

# Given two numbers, return array of length num1 with each value num2. Print "Jinx!" if they are the same.
def sameies(num1, num2):
    arr = []
    if num1 == num2:
        print("Jinx")
    else:
        for val in range(0,num1):
            arr.append(num2)
    return arr
# sameies(6,4)
