# Create this function without using random.randInt() but you are allowed to use random.random().

import random

# randInt() returns a random integer between 0 to 100
def randint(max=''):
    return round(random.random()*100)
randint()


# randInt(max=50) returns a random integer between 0 to 50
def randint(max=''):
    return round(random.random()*max)
randint(max=50)



# randInt(min=50) returns a random integer between 50 to 100
def randint(min=''):
    return random.randrange(min,100)
randint(min=50)

# randInt(min=50, max=500) returns a random integer between 50 and 500
def randint(min='', max=''):
    return random.randrange(min,max)
randint(min=50, max=500)
