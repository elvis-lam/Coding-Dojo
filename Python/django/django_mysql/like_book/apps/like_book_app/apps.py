from django.apps import AppConfig


class LikeBookAppConfig(AppConfig):
    name = 'like_book_app'
