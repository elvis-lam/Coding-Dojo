from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^register/$', views.register),
    url(r'^login/$', views.login),
    url(r'^wall/$', views.wall),
    url(r'^messages/$', views.message),
    url(r'^delete/(?P<id>\d+)/$', views.delete),
    url(r'^comment/(?P<post_id>\d+)/$', views.comment),
]
