from django.shortcuts import render, redirect, HttpResponse
from .models import *

def index(request):
    context = {
        'Course': Course.objects.all()
    }
    print(Course.objects.all().values())
    return render(request, 'course/index.html', context)

def add(request):
    add_user = Course.objects.create(name = request.POST['name'], desc = request.POST['describe'])
    print(Course.objects.all().values())
    return redirect('/')

def destroy(request, id):
    show_user = Course.objects.get(id=id)
    context = {
        'course': show_user
    }
    return render(request, 'course/destroy.html', context)

def delete(request, id):
    delete_user = Course.objects.get(id=id)
    delete_user.delete()
    return redirect('/')
