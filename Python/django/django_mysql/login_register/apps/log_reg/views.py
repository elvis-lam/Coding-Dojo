from django.shortcuts import render, redirect
from .models import *
from django.contrib import messages

import re
import bcrypt

EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

def index(request):
    return render(request, 'log_reg/index.html')

def register(request):
    if request.method != 'POST':
        return redirect('/')


    errors = False
    if len(request.POST['first']) < 3 or len(request.POST['last']) < 3:
        messages.error(request, "Name needs to have a minimum of 3 characters")
        errors = True
    if not request.POST['first'].isalpha() or not request.POST['last'].isalpha():
        messages.error(request, "Name must be characters only")
        errors = True
    if not EMAIL_REGEX.match(request.POST['email']):
        messages.error(request, "Invalid Email")
        errors = True

    # Validate if e-mail already exist
    if len(User.objects.filter(email = request.POST['email'])) > 0:
        messages.error(request, "E-mail already exists")
        errors = True
    if len(request.POST['pw']) < 8:
        messages.error(request, "Password needs to have a minimum of 8 characters")
        errors = True
    if request.POST['pw'] != request.POST['conf_pw']:
        messages.error(request, "Passwords do not match")
        errors = True

    if errors:
        return redirect('/')

    # Hash the password into the database
    else:
        messages.success(request, "Successfully registered")
        hash = bcrypt.hashpw(request.POST['pw'].encode(), bcrypt.gensalt())
        user = User.objects.create(first_name = request.POST['first'], last_name = request.POST['last'], email = request.POST['email'], password= hash)
        request.session['user_id'] = user.id
        request.session['user_first_name'] = user.first_name
        return redirect('/success')

def login(request):
    if len(request.POST['email']) < 1:
        messages.error(request, "Invalid Credentials")
        return redirect ('/')

    user = User.objects.filter(email = request.POST['email'])
    if len(user):
        # Checking if the password matches from the one in the database
        if bcrypt.checkpw(request.POST['pw'].encode(), user[0].password.encode()):
            request.session['user_id'] = user[0].id
            request.session['user_first_name'] = user[0].first_name
            messages.success(request, 'Successfully Logged in')
            return redirect('/success')
        else:
            messages.error(request, 'Invalid Credentials')
            return redirect('/')

    return redirect ('/')



def success(request):
    user = User.objects.all()
    return render(request, 'log_reg/success.html')
