from django.shortcuts import render, redirect
from django.db import models
from .models import User

def index(request):
    context = {
        "User": User.objects.all()
    }
    return render(request, 'users_app/index.html', context)

# GET request to /users/new - calls the new method to display a form allowing users to create a new user.
def new(request):
    return render(request, 'users_app/new.html')

# GET /users/<id> - calls the show method to display the info for a particular user with given id.
def show(request, id):
    show_user = User.objects.get(id=id)
    context = {
        'user' : show_user
    }
    return render(request, 'users_app/show.html', context)

# POST to /users/create - calls the create method to insert a new user record into our database. This POST should be sent from the form on the page /users/new. Have this redirect to /users/<id> once created.
def create(request):
    # user = User.objects.get(id=id)
    User.objects.create(first_name = request.POST['first_name'], last_name = request.POST['last_name'], email = request.POST['email'])
    return redirect('/users')

# GET request /users/<id>/edit - calls the edit method to display a form allowing users to edit an existing user with the given id.
def edit(request, id):
    # print(id)
    edit_user = User.objects.get(id=id)
    context = {
        'User' : edit_user
    }
    return render(request, 'users_app/edit.html', context)
# POST /users/update - calls the update method to process the submitted form sent from /users/<id>/edit. Have this redirect to /users/<id> once updated.
def update(request, id):
    update_user = User.objects.get(id=id)
    update_user.first_name = request.POST['first_name']
    update_user.last_name = request.POST['last_name']
    update_user.email = request.POST['email']
    update_user.save()
    return redirect('/users')

# GET /users/<id>/destroy - calls the destroy method to remove a particular user with the given id.
def destroy(request, id):
    delete_user = Quote.objects.get(id=id)
    delete_user.delete()
    return redirect('/dashboard')
