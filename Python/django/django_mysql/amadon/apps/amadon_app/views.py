# You've decided to build your own e-commerce site called Amadon as a Django app.  Have your app store, in the database, all the items customers have ordered.

from django.shortcuts import render, redirect, HttpResponse
from .models import *

def index(request):
    context = {
        'items': Amadon.objects.all(),
    }
    return render(request, 'amadon_app/index.html', context)

def process(request, id):
    # add = Amadon.objects.get(id=id)
    return redirect('/checkout/'+ str(id))

def checkout(request, id):
    check_out = Amadon.objects.get(id=id)
    context = {
        'item' : check_out,
        # 'number': request.POST['amounts'],
        'price': check_out.price
    }
    return render(request, 'amadon_app/checkout.html', context)


def back(request):
    return redirect('/')
