from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^register$', views.register),
    url(r'^login$', views.login),

    url(r'^dashboard$', views.dashboard),
    url(r'^view/(?P<id>\d+)$', views.view),
    url(r'^add/(?P<id>\d+)$', views.add),

    url(r'^edit/(?P<id>\d+)$', views.edit),
    url(r'^update/(?P<id>\d+)$', views.update),

    url(r'^addjob$', views.addjob),
    url(r'^process$', views.process),

    url(r'^back$', views.back),
    url(r'^logout$', views.logout),
    url(r'^cancel/(?P<id>\d+)$', views.cancel),
    url(r'^done/(?P<id>\d+)$', views.done)

]
