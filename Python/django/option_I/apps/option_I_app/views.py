from django.shortcuts import render, redirect
from .models import *
from django.contrib import messages

import re
import bcrypt


EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

def index(request):
    return render(request, 'option_I_app/index.html')

def register(request):
    if request.method != 'POST':
        return redirect('/')


    errors = False
    if len(request.POST['first_name']) < 3 or len(request.POST['last_name']) < 3:
        messages.error(request, "Name needs to have a minimum of 3 characters")
        errors = True
    # Check for this error only if the above error passes
    elif not request.POST['first_name'].isalpha() or not request.POST['last_name'].isalpha():
        messages.error(request, "Name must be characters only")
        errors = True
    if not EMAIL_REGEX.match(request.POST['email']):
        messages.error(request, "Invalid Email")
        errors = True

    # Validate if e-mail already exist
    if len(User.objects.filter(email = request.POST['email'])) > 0:
        messages.error(request, "E-mail already exists")
        errors = True
    if len(request.POST['pw']) < 8:
        messages.error(request, "Password needs to have a minimum of 8 characters")
        errors = True
    # Check for this error only if the above error passes
    elif request.POST['pw'] != request.POST['conf_pw']:
        messages.error(request, "Passwords do not match")
        errors = True

    if errors:
        return redirect('/')

    # Hash the password into the database
    else:
        # Success
        hash = bcrypt.hashpw(request.POST['pw'].encode(), bcrypt.gensalt())
        user = User.objects.create(first_name = request.POST['first_name'], last_name = request.POST['last_name'], email = request.POST['email'], password= hash)
        request.session['user_id'] = user.id
        request.session['user_first_name'] = user.first_name
        return redirect('/dashboard')

def login(request):
    if len(request.POST['email']) < 1:
        messages.error(request, "Invalid Credentials")
        return redirect ('/')

    user = User.objects.filter(email = request.POST['email'])
    if len(user):
        # Checking if the password matches from the one in the database
        if bcrypt.checkpw(request.POST['pw'].encode(), user[0].password.encode()):
            request.session['user_id'] = user[0].id
            request.session['user_first_name'] = user[0].first_name
            # Success
            return redirect('/dashboard')
        # If the e-mail and password do not match - invalid
        else:
            messages.error(request, "Invalid Credentials")
            return redirect('/')

    # If e-mail does not exist in database
    else:
        messages.error(request, "E-mail does not exist")
        return redirect('/')

    return redirect ('/')



def dashboard(request):
    context = {
        'm_job' : User.objects.get(id= request.session['user_id']).post_job.all(),
        'jobs' : Job.objects.all(),
    }
    return render(request, 'option_I_app/dashboard.html', context)

def add(request, id):
    job = Job.objects.get(id = id)
    user = User.objects.get(id=request.session['user_id'])
    if request.session['user_id']:
        this_job = job.my_job.add(user)
        # this_job= job.my_job.remove(user)
    return redirect('/dashboard', this_job)


def view(request, id):
    show_job = Job.objects.get(id=id)
    context = {
        'job' : show_job,
    }
    return render(request, 'option_I_app/view.html', context)


def edit(request, id):
    context = {
        'job': Job.objects.get(id=id)
    }
    return render(request, 'option_I_app/edit.html', context)

def update(request, id):
    if request.method == 'POST':
        # Validating characters for title and description
        errors = False
        if len(request.POST['title']) < 3:
            messages.error(request, "Title needs to have a minimum of 3 characters")
            errors = True
        if len(request.POST['description']) < 10:
            messages.error(request, "Description needs to have a minimum of 10 characters")
            errors = True
        if len(request.POST['location']) < 1:
            messages.error(request, "Location cannot be blank")
            errors = True
        if errors:
            return redirect('/edit/'+str(id))
        else:
            Job.objects.create(title=request.POST['title'], description=request.POST['description'], location=request.POST['location'], the_user = User.objects.get(id = request.session['user_id']))

    return redirect ('/dashboard')




def addjob(request):
    return render(request, 'option_I_app/addjob.html')


def process(request):
    if request.method == 'POST':
        # Validating characters for title and description
        errors = False
        if len(request.POST['title']) < 3:
            messages.error(request, "Title needs to have a minimum of 3 characters")
            errors = True
        if len(request.POST['description']) < 10:
            messages.error(request, "Description needs to have a minimum of 10 characters")
            errors = True
        if len(request.POST['location']) < 1:
            messages.error(request, "Location cannot be blank")
            errors = True
        if errors:
            return redirect('/addjob')
        else:
            Job.objects.create(title=request.POST['title'], description=request.POST['description'], location=request.POST['location'], the_user = User.objects.get(id = request.session['user_id']))

    return redirect ('/dashboard')


# Hitting the back button
def back(request):
    return redirect('/dashboard')


# Hitting the Logout button
def logout(request):
    request.session.flush()
    return redirect('/')


# Hitting the CANCEL tag to delete job from database
def cancel(request, id):
    Job.objects.get(id=id).delete()
    return redirect('/dashboard')


# Hitting the DONE tag to remove from the user's my job
def done(request, id):
    job = Job.objects.get(id = id)
    user = User.objects.get(id=request.session['user_id'])
    if request.session['user_id']:
        this_job = job.my_job.remove(user)

    return redirect('/dashboard')
