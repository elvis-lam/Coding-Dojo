from django.shortcuts import render, HttpResponse, redirect
from time import gmtime, strftime
import time
# Create your views here.
def index(request):
    # reponse = "I am home"
    # current_time = time.localtime()
    context = {
    "time": strftime("%b %d %Y %I:%M %p", time.localtime())
    }
    # print(context)
    if request.method == 'POST':
        print(request.POST)
    return render(request, 'display_app/index.html', context)

def display(request):
    return redirect('/')
