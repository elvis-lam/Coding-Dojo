from django.shortcuts import render, HttpResponse, redirect

# Create your views here.
def index(request):
    return render(request, 'survey_app/index.html')

def process(request):
    if request.method == "POST":
        # print(request.session)
        request.session['first'] = request.POST['first']
        request.session['location'] = request.POST['location']
        request.session['language'] = request.POST['language']
        request.session['comment'] = request.POST['comment']
        return redirect('/results')
    return redirect('survey_app/index.html')

def results(request):
    if 'count' in request.session:
        request.session['count'] += 1
        print(request.session)
    else:
        request.session['count'] = 0
    return render(request, 'survey_app/results.html')

def back(request):
    return render(request, 'survey_app/index.html')
