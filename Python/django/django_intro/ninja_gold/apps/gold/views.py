from django.shortcuts import render, redirect
import random

# Set total gold to 0 and declare total and activity if not already
def index(request):
    if 'total' not in request.session:
        request.session['total'] = 0
        # print(request.session['total'])
    if 'activity' not in request.session:
        request.session['activity'] = []
        # print(activity)
    return render(request, 'gold/index.html')

def process(request):
    # Setting up the Farm
    if request.POST['place'] == 'farm':
        farm_add = random.randrange(10, 21)
        request.session['total'] += farm_add
        sentence = 'Earned ' + str(farm_add) + ' golds from the ' + request.POST['place']
        request.session['activity'].append(sentence)
        # print(request.session['total'])

    # Setting up the cave
    if request.POST['place'] == 'cave':
        cave_add = random.randrange(5, 11)
        request.session['total'] += cave_add
        sentence = 'Earned ' + str(cave_add) + ' golds from the ' + request.POST['place']
        request.session['activity'].append(sentence)
        # print(request.session['total'])

    # Setting up the house
    if request.POST['place'] == 'house':
        house_add = random.randrange(2, 6)
        request.session['total'] += house_add
        sentence = 'Earned ' + str(house_add) + ' golds from the ' + request.POST['place']
        request.session['activity'].append(sentence)
        # print(request.session['total'])

    # Setting up the casino
    if request.POST['place'] == 'casino':
        # Use 50/50 to determine if the earned gold will be negative or positive
        fiftyfifty = random.randrange(1,3)
        if fiftyfifty == 1:
            casino_add = random.randrange(-51, 1)
            request.session['total'] += casino_add
            sentence = 'Earned ' + str(casino_add) + ' golds from the ' + request.POST['place']
            request.session['activity'].append(sentence)

        elif fiftyfifty == 2:
            casino_add = random.randrange(0, 51)
            request.session['total'] += casino_add
            sentence = 'Earned ' + str(casino_add) + ' golds from the ' + request.POST['place']
            request.session['activity'].append(sentence)
        # print(request.session['total'])

    # print(request.session['total'])
    return redirect('/')

def clear(request):
    request.session.flush()
    return redirect('/')
