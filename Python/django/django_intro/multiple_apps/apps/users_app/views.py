from django.shortcuts import render, redirect, HttpResponse

def register(request):
    response = "placeholder for users to create a new user record"
    return HttpResponse(response)

def login(request):
    response = "placeholder for users to login"
    return HttpResponse(response)

def new(request):
    return redirect('/')

def display(request):
    response = "placeholder to later display all the list of users"
    return HttpResponse(response)
