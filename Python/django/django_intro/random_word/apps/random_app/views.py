# The first time you use this app, it should say 'attempt #1'. Each time you generate a new random keyword, it should increment the attempt figure.

from django.shortcuts import render, HttpResponse, redirect
from django.utils.crypto import get_random_string


# Create your views here.
def index(request):
    # Generate a random 14 character string every time the page is refreshed
    if 'attempt' in request.session:
        request.session['attempt'] += 1
        print(request.session)
    else:
        request.session['attempt'] = 0
    context = {
    'generate': get_random_string(length=14)
    }
    print(context)
    return render(request, 'random_app/index.html', context)

def reset(request):
    # Reset the attempt number back to 0
    request.session['attempt'] = 0
    print(request.session)
    return redirect('/random_word')
