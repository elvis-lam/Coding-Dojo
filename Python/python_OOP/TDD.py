# Write a function and tests to determine whether it works as expected with Unittest
# import the python testing framework
import unittest
# our "unit"

# reverseList - Write a function that reverses the values in the list (without creating a temporary array).
# For example, reverseList([1,3,5]) should return [5,3,1].  In other words assertEqual( reverseList[1,3,5],
# [5,3,1] ).  Create at least 3 other test cases before you implement the functionality.

def reverse_list(list):
    for i in range(len(list)//2):
        list[i], list[len(list)-i-1] = list[len(list)-i-1], list[i]
    return list

class Reverse(unittest.TestCase):
    def testOne(self):
        self.assertEqual(reverse_list([3,4,5]), [2,1])

    def testTwo(self):
        self.assertEqual(reverse_list([1,2,3]), [3,2,1])

    def testThree(self):
        self.assertEqual(reverse_list([3,2,1]), [3,2,1])

    def setUp(self):
        print("running setUp")

    def tearDown(self):
        print("running tearDown tasks")
if __name__ == '__main__':
    unittest.main()

# isPalindrome - Write a function that checks whether the given word is a palindrome (a word that spells the
# same backward).  For example, isPalindrome("racecar") should return true.  Another way to say this is
# assertEqual( isPalindrome("racecar"), True ) or assertTrue( isPalindrome("racecar")).  Similarly,
# assertFalse( isPalindrome("rabcr") ).  Add at least 5 other test cases before you implement the functionality.
# Remember that you need to write the tests first, make sure the tests fail, and then write the functionality
# within the function, to now make all the tests pass.  (also remember that if a = "hello", a[0] returns 'h'
# and a[1] returns 'e').
#
def is_palindrome(word):
    new_str = ''
    for i in range((len(word)-1), -1, -1):
        new_str = new_str + word[i]
    if word == new_str:
        return True
    else:
        return False

class Palindrom(unittest.TestCase):
    def testOne(self):
        self.assertEqual(is_palindrome('racecar'), True)

    def testTwo(self):
        self.assertEqual(is_palindrome('rabcar'), False)

    def testThree(self):
        self.assertEqual(is_palindrome('m1ne'), False)

    def testFour(self):
        self.assertEqual(is_palindrome('mom'), True)

    def testFive(self):
        self.assertEqual(is_palindrome('d4d'), True)

    def setUp(self):
        print("running setUp")

    def tearDown(self):
        print("running tearDown tasks")
if __name__ == '__main__':
    unittest.main()

# coins - Write a function that determines how many quarters, dimes, nickels, and pennies to give to a customer
# for a change where you minimize the number of coins you give out.  For example, if you need to give the
# customer 87 cents, you can give 3 quarters, 1 dime, 0 nickel and 2 pennies.  If you need to give the customer
# 92 cents, you can give 3 quarters, 1 dime, 1 nickel, and 2 pennies.  Write the function such that for example,
# coin(87) returns [3,1,0,2].  coin(92) should return [3,1,1,2].  Add at least 5 other test cases first,
# before you fill in the codes inside function coin().

def change(money):
    this_list = []
    coins = [25,10,5,1]
    for i in range(4):
        this_list.append(money//coins[i])
        money = money % coins[i]
    return this_list
# change(87)

class Generate(unittest.TestCase):
    def testOne(self):
        self.assertEqual(change(92), [3,1,1,2])

    def testTwo(self):
        self.assertEqual(change(87), [3,1,0,2])

    def testThree(self):
        self.assertEqual(change(7), [0,0,1,2])

    def testFour(self):
        self.assertEqual(change(61), [2,1,0,1])

    def testFive(self):
        self.assertEqual(change(14), [0,1,0,4])

    def setUp(self):
        print("running setUp")

    def tearDown(self):
        print("running tearDown tasks")
if __name__ == '__main__':
    unittest.main()

# Factorial (hacker challenge).  Write a function factorial() that returns the factorial of the given number.
# For example, factorial(5) should return 120.  Do this using recursion; remember that
# factorial(n) = n * factorial(n-1).

def fact(n):
    if n == 0 or n == 1:
        return n
    return fact(n-1) * n

class Factorial(unittest.TestCase):
    def testOne(self):
        self.assertEqual(fact(5), 120)

    def testTwo(self):
        self.assertEqual(fact(4), 24)

    def testThree(self):
        self.assertEqual(fact(6), 720)

    def setUp(self):
        print("running setUp")

    def tearDown(self):
        print("running tearDown tasks")
if __name__ == '__main__':
    unittest.main()

# Fib (hacker challenge). Write a function fib() in Python that returns the appropriate Fibonacci number.
# Do this using recursion.  Let's say that the first two Fibonacci numbers are 0 and 1.  Remember that
# fib(n) = fib(n-2) + fib(n-1).

def fib(n):
    if n == 0 or n == 1:
        return n
    return fib(n-2) + fib(n-1)

class Fibonacci(unittest.TestCase):

    def testOne(self):
        self.assertEqual(fib(5), 5)

    def testTwo(self):
        self.assertEqual(fib(6), 8)

    def testThree(self):
        self.assertEqual(fib(7), 13)

    def setUp(self):
        print("running setUp")

    def tearDown(self):
        print("running tearDown tasks")
if __name__ == '__main__':
    unittest.main()
