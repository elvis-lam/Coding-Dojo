# Create an Animal class and give it the below attributes and methods. Extend the Animal class to two child classes,
# Dog and Dragon.
# To create a new class with attributes and methods that are already defined in another class, you can have
# this new class inherit from that other class (called the parent) instead of copying and pasting code from the original
# class. Child classes can access all the attributes and methods of a parent class AND have new attributes and methods of
# its own, for child instances to call.

class Animal:
    def __init__(self, name, set_health = 0):
        self.name = name
        self.set_health = set_health

    def walk(self):
        self.set_health -= 1
        return self

    def run(self):
        self.set_health -= 5
        return self

    def display_all(self):
        print("Name:", self.name)
        print("Health:", self.set_health)

class Dog(Animal):
    def __init__(self):
        super().__init__('Dog_Lacy', set_health = 150)
        # Default health of 150

    def pet(self):
        self.set_health += 5
        return self

# dog1 = Dog()
# dog1.pet().pet().pet().run().display_all()


class Dragon(Animal):
    def __init__(self):
        super().__init__('Dragon_Spot', set_health = 170)
    # Default Health of 170

    def fly(self):
        self.set_health -= 10
        return self
    # Display health prints health by calling the parent method and prints "I am a Dragon"
    def display_health(self):
        super().display_all()
        print("I am a dragon")

dragon1 = Dragon()
dragon1.fly().fly().fly().display_health()

# Test to make confirm you cannot call another sibling's class methods
# dog2 = Dog("Dog_Jack")
# dog2.fly().display_health()
