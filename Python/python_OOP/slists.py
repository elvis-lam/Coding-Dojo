# Part 1 - Create SList and Node.  Create addNode and printAllValues methods.
# Singly Linked List is one of the most fundamental data structures you'll be using.

class Node:
    def __init__(self, val):
    # the __init__ method represents the constructor for the Node class when an object is created
        # the self.val or self acts as the attribute and val is the argument passed in
        self.val = val
        # The next pointer here in each node is currently defaulted to pointed at Nothing (None)
        self.next = None

class Slist:
    def __init__(self):
        # The head pointer will be defaulted to point at Nothing (None)
        self.head = None

    # add_node will create new nodes when called. In this case will require a val to be passsed in
    def add_node(self, val):
        # Use node(variable) to instantiat the Node class value(val)
        node = Node(val)
        # Runner (variable) will represent the head (root)
        runner = self.head
        # If the Runner is not currently pointing to a node besides the head (root), the Runner will "link" the head to a Node(Value)
        if not runner:
            self.head = node
        # Else if the Runner is pointed at a node (not the head) then...
        else:
            # While the Runner Next(Pointing) is not pointing to Nothing (None) then...
            while runner.next != None:
                # The Runner will point to the next Node and the following
                runner = runner.next
            # Otherwise if the Runner is "Pointing" to Nothing, Runner will "link" the Node(value)
            runner.next = node

    # The method print values will display the position of the Node the Runner linked last
    def print_values(self):
        # New method new variable Runner
        runner = self.head
        while runner.next != None:
            runner = runner.next
            # Print the Runner's linked position
            print(runner.val)
        print(runner.val)

# Part 2 - implement removeNode(val)
# Implement removeNode(val) where it removes a node with the value val.  For example list.removeNode(5) will see
# if there's a node with the value of 5.  If it is, it will remove that from the linked list.  When you do this,
# you'll need to consider the following cases:

    def remove_node(self, val):
        remove = Node(val)
        runner = self.head
        while runner.next != None:
            if runner.next == remove:
                self.head = runner.next
        runner.remove(remove)

# 1. when the node you want to remove is in the beginning of the linked list
# 2. when the node you want to remove is in the middle of the linked list
# 3. when the node you want to remove is at the end of the linked list

# For each of these cases, you will probably need to have different logics to handle the removal.

# list = Slist(5)
# list.addNode(7)
# list.addNode(9)
# list.addNode(1)
# list.removeNode(9) # removes 9, which is one of the middle nodes in the list
# list.removeNode(5) # removes 5, which is the first value in the list
# list.removeNode(1) # removes 1, which is the last node in the list
# list.printAllValues("Attempt 1")









s1 = Slist()

s1.add_node(10)
s1.add_node(20)
s1.add_node(25)
s1.add_node(105)
s1.add_node(15)
s1.add_node(5)
s1.add_node(50)
s1.add_node(43)
s1.remove_node(15)

print(s1.print_values())
