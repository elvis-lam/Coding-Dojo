# Practice creating a class and creating new instances
# Practice chaining methods
# Practice writing flexible functions that can take a variable number of arguments

# Create a Python class called MathDojo that has the methods add and subtract. Have these 2 functions take
# at least 1 parameter. Then create a new instance called md.

class MathDojo:
    def __init__(self, total):
        self.total = total

    def add(self, *adding):
        for i in adding:
             if type(i) == int or type(i) == float:
                 self.total += i
        return self

    def subtract(self, *substracting):
        for i in substracting:
             if type(i) == int or type(i) == float:
                 self.total -= i
        return self

    def results(self):
        return self.total

md = MathDojo(0)
print(md.add(2).add(2,5,1).subtract(3,2).results())
