# Create a new class called Bike with the following properties/attributes:
# price, max_speed, miles
# Use the __init__() method to specify the price and max_speed of each instance (e.g. bike1 = Bike(200, "25mph");
# In the __init__(), also write the code so that the initial miles is set to be 0 whenever a new instance is created.
# Add the following methods to this class:
# displayInfo() - have this method display the bike's price, maximum speed, and the total miles.
# ride() - have it display "Riding" on the screen and increase the total miles ridden by 10
# reverse() - have it display "Reversing" on the screen and decrease the total miles ridden by 5...

class Bike:
    def __init__(self, price, max_speed, miles=0):
        self.price = price
        self.max_speed = max_speed
        self.miles = miles

    def ride(self):
        self.miles += 10 # Miles will increase by 10 when this method is called
        print("Riding")
        return self

    def reverse(self):
        self.miles -= 5 # Miles will decrease by 5 when this method is called
        print("Reversing")
        # What would you do to prevent the instance from having negative miles?
        if self.miles > 5:
            self.miles = self.miles -5
        else:
            self.miles = 0
        return self

    def display_all(self):
        print("Price:", self.price, "- Max Speed:", self.max_speed, "- Total Miles biked:", self.miles)

# Have the first instance ride three times, reverse once and have it displayInfo(). Have the second instance ride twice,
# reverse twice and have it displayInfo(). Have the third instance reverse three times and displayInfo().

bike1 = Bike(200, "25mph")
bike1.ride().ride().ride().reverse().display_all()

# bike2 = Bike(150, "20mph")
# bike2.ride().ride().reverse().reverse().display_all()
#
# bike3 = Bike(250, "30mph")
# bike3.reverse().reverse().reverse().display_all()

# Which methods can return self in order to allow chaining methods?
# The ride and reverse method must return self in order to execute the Miles
