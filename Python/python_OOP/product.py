# The owner of a store wants a program to track products. Create a product class to fill the following requirements.
# Price - Item Name - Weight - Brand - Status: default "for sale"
# Methods
# Sell: changes status to "sold"
# Add tax: takes tax as a decimal amount as a parameter and returns the price of the item including sales tax
# Return Item: takes reason_for_return as a parameter and changes status accordingly. If the item is being returned because it is defective, change status to "defective" and change price to 0. If it is being returned in the box, like new, mark it "for sale". If the box has been opened, set the status to "used" and apply a 20% discount.  (use "defective", "like_new", or "opened" as three possible value for reason_for_return).
# Display Info: show all product details.

class Product:
    def __init__(self, price, item_name, weight, brand, status="for sale"):
        self.price = price
        self.item_name = item_name
        self.weight = weight
        self.brand = brand
        self.status = status

    def sell(self):
        self.status = "sold"
        return self

    def add_tax(self):
        tax = 0.093 # California Tax
        tax *= self.price
        self.price += tax
        return self

    def return_item(self, reason_for_return):
        self.reason_for_return = reason_for_return
        # Reason for return must include options for return
        if self.reason_for_return == "defective":
            self.status = "defective"
            self.price = 0
        if self.reason_for_return == "like_new":
            self.status = "for sale"
        if self.reason_for_return == "opened":
            self.status = "used"
            discount = .20
            discount *= self.price
            self.price -= discount
        return self

    def display_all(self):
        print("Item:", self.item_name)
        print("Price: $", self.price)
        print("Weight:", self.weight, "lbs")
        print("Brand:", self.brand)
        print("Status:", self.status)

product1 = Product(700, "hand Bag", 15, "Gucci")
# product1.add_tax().display_all()

product2 = Product(800, "Wallet", 5, "MK")
# product2.sell().display_all()

product3 = Product(2000, "Laptop", 10, "Apple")
# product3.return_item("opened").display_all()

product3 = Product(1000, "Laptop", 10, "HP")
# product3.return_item("defective").display_all()
