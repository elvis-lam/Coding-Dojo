1. Understand why data structures are important
Being able to analysis the complexity of data and how adding structure can make our algoritms much simpler.

2. Understand why we do algorithms and how this is applicable
Knowing the fundamentals and logic of data structure can help us view algoritms easier.

3. Understand bits and bytes
A bit is the smallest unit of storage used to contain a 0 or 1. A bytes consist of 8 bits. One bytes can store one letter or number.

4. Undertand O(N) O(N2) notations
O(N) is the order of operation or order of N used to iterate nested loops.

5. Understand why memory management is important
Utilizing every single bit/ byte to allocate portions of memory to program.
