from flask import Flask, render_template, request, redirect
from mysqlconnection import connectToMySQL
app = Flask(__name__)

mysql = connectToMySQL('lead_gen_business')

# Show all the cusomters and all theleads generated for each customer, since the beginning from this mysql db.

@app.route('/')
def index():
    all_clients = mysql.query_db("SELECT CONCAT(clients.first_name, ' ', clients.last_name) AS Customer_Name, COUNT(leads.first_name) AS number_of_leads FROM clients JOIN sites ON sites.client_id = clients.client_id JOIN leads ON leads.site_id = sites.site_id GROUP BY clients.first_name")
    # print(all_clients)
    return render_template('index.html', clients=all_clients)

app.run(debug=True)
