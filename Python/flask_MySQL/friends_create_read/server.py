from flask import Flask, render_template, request, redirect
from mysqlconnection import connectToMySQL
app = Flask(__name__)

mysql = connectToMySQL('friendsdb')

# Create an application that allows users to view all friends and add new ones.

@app.route('/')
def index():
    all_friends = mysql.query_db("SELECT * FROM friends")
    print(all_friends)
    return render_template('index.html', friends = all_friends)


@app.route('/add', methods=['POST'])
def create():
    insert_query = "INSERT INTO friends(first_name, last_name, occupation, created_at, updated_at) VALUES (%(first_name)s, %(last_name)s, %(occupation)s, NOW(), NOW());"
    insert_data = {
             'first_name': request.form['first_name'],
             'last_name':  request.form['last_name'],
             'occupation': request.form['occupation']
           }
    print(insert_data)
    mysql.query_db(insert_query, insert_data)
    return redirect('/')

app.run(debug=True)
