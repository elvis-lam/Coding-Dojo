from flask import Flask, render_template, redirect, request, flash, session
from flask_bcrypt import Bcrypt
from mysqlconnection import connectToMySQL
import re

mysql = connectToMySQL('emaildb')
EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

app = Flask(__name__)
app.secret_key = "thisTheSecretLife"
bcrypt = Bcrypt(app)


@app.route('/')
def index():
    # return index
    return render_template('index.html')


@app.route('/login', methods=['POST'])
def login():
    get_email = mysql.query_db("SELECT * FROM users WHERE email = %(email)s", {'email': request.form['email']})
    # Query the database to see if there is already an existing email
    # Email validation with error
    error = False
    # Using regex to make sure this is in email format
    if not EMAIL_REGEX.match(request.form['email']):
        flash('Please enter a valid Email!')
        error = True
    # If there is an exisiting email
    if get_email:
        flash('E-mail is already registered!')
        error = True

    if error:
        return redirect('/')
    else:
        # If email is a new email, insert into the database
        # Python 3 string interpolation format %(___)s
        insert_query = "INSERT INTO users(email, created_at, updated_at) VALUES (%(email)s, NOW(), NOW());"
        insert_data = {
            'email': request.form['email']
        }
        mysql.query_db(insert_query, insert_data)
        session['email'] = request.form['email']
        return redirect('/success')

@app.route('/success')
def success():
    get_email = mysql.query_db("SELECT * FROM users WHERE email = %(email)s, {'email': request.form['email']})
    return render_template('success.html', show_email = get_email)


app.run(debug=True)
