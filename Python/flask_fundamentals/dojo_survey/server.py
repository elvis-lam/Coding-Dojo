#Build a flask application that accepts a form submission and presents the submitted data on a results page.
from flask import Flask, render_template, request, redirect, session
app = Flask(__name__)

app.secret_key = "HiItsMe"

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/process', methods=["POST"])
def process():
    print(request.form)
    session['first'] = request.form['first']
    session['location'] = request.form['location']
    session['language'] = request.form['language']
    session['comment'] = request.form['comment']
    return redirect('/results')

@app.route('/results')
def results():
    return render_template('results.html')

@app.route('/danger', methods=["POST"])
def danger():
    print('a user tried to visit/ danger, we have to redirect to /.')
    return redirect('/')

app.run(debug=True)
