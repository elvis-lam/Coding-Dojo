# Create a site that when a user loads it creates a random number between 1-100
# and stores the number in session. Allow the user to guess at the number and
# tell them when they are too high or too low. If they guess the correct number
# tell them and offer to play again.

from flask import Flask, render_template, redirect, session, request
import random
app = Flask(__name__)
app.secret_key = 'thisTheKeyHere'

@app.route('/')
def index():
    if 'number' not in session:
        session['number']= round(random.random()*100)
    if 'message' not in session:
        session["message"] = ''
    return render_template('index.html', message=session['message'])

@app.route('/process', methods=['POST'])
def process():
    session['guess'] = int(request.form['guess'])
    guess = int(request.form['guess'])
    # If the guess is the random number, post this message to the html
    if guess == session['number']:
        session['message'] = (str(session['number']) + " was the number!")
    # If the guess is greater than the random number, post this message to the html
    if  guess > session['number']:
        session['message'] = 'Too high!'
    # If the guess is less than the random number, post this message to the html
    elif guess < session['number']:
        session['message'] = 'Too Low!'
    return redirect('/')

@app.route('/reset', methods=['POST'])
def reset():
    session.clear() #Clear the Game and play again!
    return redirect('/')

app.run(debug=True)
