# Build a small webb applicatio Dojo Fruit Store

from flask import Flask, render_template, redirect, session, request
import datetime

app = Flask(__name__)
app.secret_key ="ThisIsIt"

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/process', methods=["POST"])
def process():
    print(request.form)
    session['quant1'] = request.form['quant1']
    session['quant2'] = request.form['quant2']
    session['quant3'] = request.form['quant3']
    session['first'] = request.form['first']
    session['student'] = request.form['student']

    # Adding the quantity of the items today with a variable total
    session['total'] = (int(session['amt'])+ int(session['amt'])+ int(session['amt']))
    return redirect('/checkout')

@app.route('/checkout')
def checkout():
    today = datetime.date.today()
    now = datetime.date.today().strftime("%a, %d %B %Y %H:%M:%S%p")

    return render_template('checkout.html', now = now)


@app.route('/home', methods=["POST"])
def home():
    return redirect('/')


@app.route('/fruits', methods=["POST"])
def fruit():
    return render_template('fruits.html')

app.run(debug=True)
