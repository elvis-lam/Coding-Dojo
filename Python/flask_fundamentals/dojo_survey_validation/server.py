#Build a flask application that accepts a form submission and presents the submitted data on a results page.
from flask import Flask, render_template, request, redirect, session, flash
app = Flask(__name__)

app.secret_key = "HiItsMe"

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/process', methods=["POST"])
def process():

    # Add validations to check that the name and comment fields are not blank
    error = False
    if len(request.form['first']) < 3:
        flash("Name requires a minimum of 3 characters.")
        error = True
    if len(request.form['comment']) < 10:
         flash("Comment requires a minimum of 10 characters.")
         error = True
    # Add validations to check that the comment is no more than 120 characters
    if len(request.form['comment']) > 120:
        flash("Maximum Character of 120 reached.")
        error = True

    if error:
        return redirect('/')
    else:
        session['first'] = request.form['first']
        session['location'] = request.form['location']
        session['language'] = request.form['language']
        session['comment'] = request.form['comment']
        return redirect('/results')

@app.route('/results')
def results():
    return render_template('results.html')

@app.route('/danger', methods=["POST"])
def danger():
    print('a user tried to visit/ danger, we have to redirect to /.')
    return redirect('/')

app.run(debug=True)
