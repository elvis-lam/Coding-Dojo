# Create a server file that generates 5 different http responses for the following 5 url requests:
from flask import Flask
app = Flask(__name__)

# localhost:5000 - have it say "Hello World!"
@app.route('/')
def index():
    return 'Hello World'

# localhost:5000/dojo - have it say "Dojo!"
@app.route('/dojo')
def dojo():
    return 'Dojo!'

# localhost:5000/say/flask - have it say "Hi Flask".  Have function say() handle this routing request.
# @app.route('/say/<flask>')
# def say(flask):
#     print(flask)
#     return 'Hi ', flask

# localhost:5000/say/michael - have it say "Hi Michael" (have the same function say() handle this routing request)
# @app.route('/say/<michael>')
# def say(michael):
#     return "Hi"+ michael

# localhost:5000/say/john - have it say "Hi John!" (have the same function say() handle this routing request)
@app.route('/say/<john>')
def say(john):
    return "Hi "+ john

# localhost:5000/repeat/35/hello - have it say "hello" 35 times! - You will need to convert a string "35" to an integer 35.
@app.route('/repeat/<id>/<hello>')
def repeat(id, hello):
    id = 35
    return ("hello \n" * id + ('\n'+ str(id)) * id)

# If the user request localhost:5000/repeat/80/hello, it should say "hello" 80 times.
# @app.route('/repeat/<id>/<hello>')
# def repeat(id, hello):
#     id = 80
#     return "hello \n" * id

# localhost:5000/repeat/99/dogs - have it say "dogs" 99 times! (have this be handled by the same route function as #6)
# @app.route('/repeat/<id>/<hello>')
# def repeat(id, hello):
#         id = 99
#         return "dogs \n" * id



app.run(debug=True)
