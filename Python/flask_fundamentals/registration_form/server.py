#Build a flask application that accepts a form submission and presents the submitted data on a results page.
from flask import Flask, render_template, request, redirect, session, flash
import re

app = Flask(__name__)
app.secret_key = "HelloItsMe"
EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

PASSWORD_REGEX = re.compile(r'^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[@#$])[\w\d@#$]{6,12}$')

@app.route('/')
def index():

    return render_template('index.html')

@app.route('/process', methods=["POST"])
def process():
    print(request.form)
    # Add validations to check that the name and comment fields are not blank
    error = False
    if len(request.form['fname']) < 3 or len(request.form['lname']) < 3:
        error = True
        flash("Name requires a minimum of 3 characters.")

    # Add validation to check if the email is valid format
    if not EMAIL_REGEX.match(request.form['email']):
        error = True
        flash("Invalid Email Address.")

    # Add the validation that requires a password to have at least 1 uppercase letter and 1 numeric value.
    if not PASSWORD_REGEX.match(request.form['password']):
        error = True
        flash("Password requires 1 uppercase letter and 1 numeric value.")

    # Add validations to check that the passwords match
    if request.form['password'] != request.form['c_password']:
        error = True
        flash("Passwords do not password.")

    if error:
        return redirect('/')
    else:
        session['fname'] = request.form['fname']
        session['lname'] = request.form['lname']
        session['email'] = request.form['email']
        flash("You've successfully signed up!")
        return redirect('/')

app.run(debug=True)
