# Build a flask application that counts the number of times the root route ('/') has been viewed.
from flask import Flask, render_template, request, redirect, session
app = Flask(__name__)
app.secret_key = "thisTheSecretBoo"

@app.route('/')
def index():
    if 'times' in session:
        session['times'] += 1
    else:
        session['times'] = 0
    return render_template('index.html')

# Increase the count by 2
@app.route('/process', methods=['POST'])
def process():
    session['times'] += 1
    return redirect('/')

# Reset the count back to 1
@app.route('/clear', methods=['POST'])
def clear():
    session['times'] = 0
    return redirect('/')

app.run(debug=True)
