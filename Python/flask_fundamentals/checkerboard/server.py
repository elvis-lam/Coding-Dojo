# Write a program that generates an HTML page that looks like a checkerboard.
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

# http://localhost:5000/(x)/(y) - should display x by y checkerboard.  For example,
# http://localhost:5000/10/10 should display 10 by 10 checkerboard.

@app.route('/<int:x>/<int:y>')
def checker(x, y):
    return render_template('checker.html', x=x, y=y)

app.run(debug=True)
