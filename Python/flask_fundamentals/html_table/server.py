# Create the following tuple of dictionaries and have it available for your route.
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def index():
    users = (
        {'first_name' : 'Michael', 'last_name' : 'Choi'},
        {'first_name' : 'John', 'last_name' : 'Supsupin'},
        {'first_name' : 'Mark', 'last_name' : 'Guillen'},
        {'first_name' : 'KB', 'last_name' : 'Tonel'}
    );

    for user in users:
        print(user['first_name'])

    return render_template('index.html', user= users)

@app.route('/table')
def table():
    print(users)
    return users


app.run(debug=True)
