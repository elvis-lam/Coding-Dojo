// Set myNumber to 42. Set myName to your name. Now swap myNumber into myName & vice versa.
function set() {
  var myNumber = 42;
  var myName = "Elvis";
  var temp = '';

  temp = myNumber;
  myNumber = myName;
  myName = temp;

  console.log(myNumber);
  console.log(myName);
}
// set()

/* *********************************************************************************************************************************** */
// Print integers from -52 to 1066 using a FOR loop
function count(){
  for (var i = -52; i < 1067; i++) {
    console.log(i);
  }
}
// count()

/* *********************************************************************************************************************************** */
// Create beCheerful(). Within it, console.log string "good morning!". Call it 98 times.
function beCheerful() {
  for (var i = 0; i < 99; i++) {
    console.log("good morning!");
    // console.log(i); ***check
  }
}
// beCheerful()

/* *********************************************************************************************************************************** */
// Using FOR, print multiples of 3 from -300 to 0. Skip -3 and -6.
function threes() {
  for (var i = -300; i < 0; i++) {
    if (i === -3 || i == -6) {
      continue;
    }
    else if (i % 3 === 0) {
      console.log(i);
    }
  }
}
// threes()

/* *********************************************************************************************************************************** */
// Print all integers from 2000 to 5280 using WHILE.
function otherLoop() {
  var i = 2000;
  while (i < 5281) {
    console.log(i);
    i++
  }
}
// otherLoop()

/* *********************************************************************************************************************************** */
// If 2 given numbers represent your birth month and day in either order, log "How do you know?" else log "Just another day..."
function specialDay(month, day) {
  var birthday = '0708';
  if (month + day == birthday || day + month == birthday) {
    console.log("How do you know?");
  }
  else {
    console.log("Just another day....")
  }
}
// specialDay('07', '08')


/* *********************************************************************************************************************************** */
// Write a function that determines whether a given year is a leap year. If a year is divisible by four, it is a leap year, unless
// it is divisible by 100. However, if it is divisible by 400, then it is.

function leap(year) {
  if (year % 4 === 0 && year % 100 !== 0) {
    console.log("This is a leap year.");
  }
  else if (year % 400 === 0) {
    console.log("This is a leap year.");
  }
  else {
    console.log("This is not leap year.");
  }
}
// leap(2000)

/* *********************************************************************************************************************************** */
// Print all integer mutliples of 5, from 512 to 4096. Aftterward, also log how many there were.

function fives() {
  var count = 0;
  for (var i = 512; i < 4096; i++) {
    if (i % 5 === 0) {
      console.log(i);
      count++;
    }
  }
  console.log("There are total of " + count + " integers.");
}
// fives()

/* *********************************************************************************************************************************** */
// Print multiples of 6 up to 60,000, using a WHILE.

function six() {
  var i = 0;
  while (i < 60000) {
    if (i % 6 === 0) {
      console.log(i);
    }
    i++;
  }
}
// six()

/* *********************************************************************************************************************************** */
// Print integers 1 to 100. If divisible by 5, print "Coding" instead. If by 10, also print "Dojo".

function code() {
  for (var i = 0; i < 100; i++) {
    if (i % 10 === 0 && i % 5 === 0) {
      console.log("Coding Dojo");
      // console.log(i); ***check
    }
    else if (i % 5 === 0) {
      console.log("Coding");
      // console.log(i); ***check
    }
  }
}
// code()

/* *********************************************************************************************************************************** */
// Your function will be given an input parameter incoming. Please console.log this value.

function input(message){
  if (message) {
  console.log(message);
  }
}
// input('incoming')

/* *********************************************************************************************************************************** */
// Add odd integers from -300,000 to 300,000, and console.log the final sum. Is there a shortcut?

function odd() {
  count = 0;
  for (var i = -300000; i < 300000; i++) {
    if (i % 2 === 1 || i % 2 === -1) {
      // console.log(i); ***check
      count += i;
    }
  }
  console.log(count);
}
// odd()
// Is there a shortcut?
// Add -300000 and 300000 together.

/* *********************************************************************************************************************************** */
//  Log positive numbers starting at 2016, counting down by fours (exclude 0), without a FOR loop.

function fours() {
  var start = 2016;
  while (start > -80) {
  start-= 4;
  if (start === 0) {
    continue;
  }
  console.log(start);
  }
}
// fours()

/* *********************************************************************************************************************************** */
// Given lowNum, highNum, mult, print mutliples of mult from highNum down to lowNum using a FOR. For (2, 9, 3), print 9 6 3 (successive line).

function countdown(lowNum, highNum, mult) {
  var line = [];
  for (var i = highNum; i > lowNum; i--) {
    if (i % mult === 0) {
      line.push(i);
    }
  }
  console.log(line);
}
// countdown(2, 9, 3)

/* *********************************************************************************************************************************** */
// Given 4 parameters (param1, param2, param3, param4). print the mutliples of param1, starting at param2, and extending to param3. One
// exception: if a multiple is equal to param4, then skip (don't print) that one. Do this using a WHILE. Given (3, 5, 17, 9), print 6, 12, 15
// (which are all of the mutliples of 3 between 5 and 17. except for value 9).

function finalCountdown(param1, param2, param3, param4) {
  var line = [];
  var i = param2;
  while (i < param3) {
    if (i % param1 === 0 && i !== param4){
    line.push(i);
    }
    i++
  }
  console.log(line)
}
// finalCountdown(3, 5, 17, 9)

/* *********************************************************************************************************************************** */
// Create a function that accepts a number as a input. Return a new array that counts down by one. from the number (as array's zero'th
// element). How long is this array?

function countOne(num) {
  var arr = [];
  for (var i = num; i > 0; i--) {
    arr.push(i);
  }
  return arr;
}
// countOne(4)

/* *********************************************************************************************************************************** */
// Your function will receive an array with two numbers. Print the first value, and return the second.

function print(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] === arr[0]) {
      console.log(arr[i]);
    }
    else if (arr[i] === arr[1]) {
      return arr[i];
    }
  }
}
// print([10, 22, 33, 44])

/* *********************************************************************************************************************************** */
// Given an array, return the sum of the first value in the array, plus the array's length.

function first(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] === arr[0]) {
      return arr[0] + arr.length;
    }
  }
}
// first([10, 2, 30, 4, 5])
// What happens if the array's first value is not a number but a string (like "what?") or boolean (like false).
// If it's a string, it will return the string plus the length. If it's a boolean, it will return the arr.length.

/* *********************************************************************************************************************************** */
// For [1, 3, 5, 7, 9, 13], print values that are greater than its 2nd value. Return how many values this is.

function greater(arr) {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > arr[1]) {
      console.log(arr[i]);
      count++

    }
  }
  return "There are " + count + " greater value in this array.";
}
// greater([1, 3, 4, 5, 6, 7])

/* *********************************************************************************************************************************** */
// Write a function that accepts any array, and returns a new array with the array values that are greater than its 2nd value. Print how
// many values this is.

function greatest(arr) {
  var count = 0;
  var newArr = [];
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > arr[1]) {
      newArr.push(arr[i]);
      count++
    }
  }
  return newArr;
  console.log("There are " + count + " greater value in this array.");
}
// greatest([1, 3, 4, 5, 6, 10])

// What will you do if the array is only one element long?
// greatest([100])
// The new array will be blank.

/* *********************************************************************************************************************************** */
// Given two numbers, return array of length num1 with each value num2. Print "Jinx!" if they are the same.

function same(num1, num2) {
  if (num1.length === num2.length) {
    return "Jinx";
  }
}
// same([1, 2, 3], [2, 5])

/* *********************************************************************************************************************************** */
// Your function should accept an array. If value at [0] is greater than array's length, print "Too big!", if value at [0] is less than
// array's length, print "Too small!", otherwise print "Just right!".

function compare(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[0] > arr.length) {
      console.log("Too big!");
      break;
    }
    else if (arr[0] < arr.length) {
      console.log("Too small!");
      break;
    }
    else {
      console.log("Just right!");
      break;
    }
  }
}
// compare([4, 3, 1, 2, 3])

/* *********************************************************************************************************************************** */
// Create fahrenheitToCelsius(fDegrees) that accepts a number of degrees in Fahrenheit, and returns the equivalent temperature as expressed
// in Celsius degrees. For review, Fahrenheit = (9/5 * Celsius) + 32.

function fahrenheitToCelsius(fDegrees) {
  var celsius = (fDegrees - 32) * 5 / 9;
  console.log(celsius + "°");
}
// fahrenheitToCelsius(50)

/* *********************************************************************************************************************************** */
// Create celsiusToFahrenheit(cDegrees) that accepts number of degrees Celsius, and returns the equivalent temperature express in Fahrenheit degrees.

function celsiusToFahrenheit(cDegrees) {
  var fahrenheit = (9/5 * cDegrees) + 32
  console.log(fahrenheit + "°");
}
// celsiusToFahrenheit(10)


// ***OPTIONAL
// Do Fahrenheit and Celsius values equate at certain number? Scientific calculation can be complex, so for this challenge just try a series
// of Celsius integers values starting at 200, going downward, checking whether it is equal to the corresponding Fahrenheit value.

function celsiusMeetFahrenheit() {
  // var same = 0;
  var degrees = 200;
  while (degrees <= 200) {
    var celsius = (degrees - 32) * 5 / 9;
    if(celsius == degrees) {

      console.log("The temperature at which Celsius annd Fahrenheit meets is " + celsius + "°.");
      break;
    }
  degrees--;
  }
}
// celsiusMeetFahrenheit()
/* *********************************************************************************************************************************** */
// Given an array, write a function that changes all positive numbers in the array to "big". Example: makeItBig([-1, 3, 5, -5]) returns
// that same array, changed to [-1, "big", "big", -5].

function makeItBig(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      arr[i] = "big";
    }
  }
  return arr;
}
makeItBig([-1, 3, 5, -5])

/* *********************************************************************************************************************************** */
// Create a function that takes array of numbers. The function should print the lowest value in the array, and return the highest values in the array.

function lowHigh(arr) {
var low = 0;
var high = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > high) {
      high = arr[i];
    }
    if (arr[i] < arr.length-1) {
      low = arr[i];
    }
  }
  console.log(low);
  return high;
}
lowHigh([2, 5, 10, 40, 1, -100])

/* *********************************************************************************************************************************** */
// Build a function that array of numbers. The function should print second-to-last value in the array, and return first odd value in the array.

function secondOdd(arr) {
  var firstOdd = 0;
  var secondLast = arr[arr.length-2]
  console.log(secondLast);

  for (var i = 0; i < arr.length; i++) {
    if (arr[i] % 2 === 1) {
      firstOdd = arr[i];
      return firstOdd;
    }
  }
}
// secondOdd([1, 2, 3, 4, 2])

/* *********************************************************************************************************************************** */
// Given array, create a function to return a new array where each value in the original has been doubled. Calling double([1, 2, 3]) should
// return [2, 4, 6] without changing original.

function double(arr) {
  var newArr = [];
  for (var i = 0; i < arr.length; i++) {
    newArr.push(arr[i] + arr[i]);
  }
  console.log(newArr);
}
// double([1, 2, 3])

/* *********************************************************************************************************************************** */
// Given array of numbers, create function to replace last value with number of positive values. Example, countPositives([-1, 1, 1, 1])
// changes array to [-1, 1, 1, 3].

function countPositives(arr) {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      count++;
    }
    if (i === arr.length-1) {
      arr[arr.length-1] = count;
    }
  }
  // console.log(count);
  console.log(arr);
}
// countPositives([-1, 1, 1, 1])

/* *********************************************************************************************************************************** */
// Create a function that accepts an array. Every time that array has three odd values in a row, print "That's odd!" Every time the array
// that has three evens in a row, print "Even more so!"

function threeRow(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] % 2 === 1 && arr[i+1] % 2 === 1 && arr[i+2] % 2 === 1) {
      console.log("That's odd!");
    }
    else if (arr[i] % 2 === 0 && arr[i+1] % 2 === 0 && arr[i+2] % 2 === 0) {
      console.log("Even more so!");
    }
  }
}
// threeRow([1, 3, 5, 2, 8])

/* *********************************************************************************************************************************** */
// Given arr, add 1 to odd elements ([1], [3]. etc.) console.log all values and return arr.

function addOdd(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (i % 2 === 1) {
      arr[i] += 1
    }
  }
  console.log(arr);
}
// addOdd([1, 2, 3, 5])

/* *********************************************************************************************************************************** */
// Passed with an array containing strings. Working within the same array, replace each string with a number - the length of the string at
// previous array index - and return the array.

function string(arr) {
  for (var i = 0; i < arr.length; i++) {
    arr[i] = arr[i].length
  }
  return arr;
}
// string(['hello', 'hi', 'hey'])

/* *********************************************************************************************************************************** */
// Build function that accepts array. Returns a new array with all values except first, adding 7 to each, Do not alter the original array.

function seven(arr) {
  var newArr = [];
  for (var i = 0; i < arr.length; i++) {
    if (i === 0) {
      continue;
    }
    else {
      arr[i] += 7;
      newArr.push(arr[i]);
    }
  }
  console.log(newArr);
}
// seven([1, 2, 3, 4, 5, 8])

/* *********************************************************************************************************************************** */
// Given array, write function that reverses values, in-place. Example: reverse([3, 1, 6, 4, 2]) returns same array containing [2, 4, 6, 1, 3]

function reverse(arr) {
  var newArr = [];
  for (var i = arr.length-1; i >= 0; i--) {
    newArr.push(arr[i])
  }
  console.log(newArr)
}
// reverse([1,2,3,4,5,6,7,8,9,10])

/* *********************************************************************************************************************************** */
// Given an array, create and return a new one containing all the values of the provided array, made negative (not simply multiplied by 1)
// Given [1, -3, 5], return [-1, -3, -5].

function negatives(arr) {
  var newArr = [];
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      arr[i] = -arr[i]
      newArr.push(arr[i])
    }
    else {
      newArr.push(arr[i])
    }
  }
  console.log(newArr);
}
// negatives([1, -3, 5])

/* *********************************************************************************************************************************** */
// Create a function that accepts an array, and prints "yummy" each time one of the value is equal to "food". If no array elements are
// "food", then print "I'm hungry" once.

function yum(arr) {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] === "food") {
      console.log("yummy");
      count++;
    }
  }
  if (count === 0) {
    console.log("I'm hungry");
  }

}
// yum(['string', 'food', 'food'])

/* *********************************************************************************************************************************** */
// Given array, swap first and last, third and third-to-last, etc. Input[true, 42, "Ada", 2, "pizza"] becomes ['pizza', 42, "Ada", 2, true],
// Change [1, 2, 3, 4, 5, 6] to [6, 2, 4, 3, 5, 1].

function center(arr){
  var decoy = 0;
  var third = 0;
  for (var i = 0; i < arr.length; i++) {
    decoy = arr[0];
    arr[0] = arr[arr.length-1];
    arr[arr.length-1] = decoy;
    third = arr[2];
    arr[2] = arr[arr.length-3];
    arr[arr.length-3] = third;
    return arr;
  }
}
// center([true, 42, "Ada", 2, "pizza"])
// center([1,2,3,4,5,6]);


/* *********************************************************************************************************************************** */
// Given array arr and number num, multiple each arr value by num, and return the changed arr.

function multi(arr, num) {
  var newArr = [];
  for (var i = 0; i < arr.length; i++) {
    newArr.push(arr[i] * num)
  }
  console.log(newArr);
}

// multi([1,2,3], 3)
