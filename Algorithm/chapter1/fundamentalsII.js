// Given an arr and number X, remove all except the last X elements, and return arr (changed and shorter). Given
// ([2, 4, 6, 8, 10], 3), chamge the given array to [6,8,10] and return it.
function removeAll(arr, X) {
  for (var j = 0; j < arr.length-X; j++) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i] < arr[arr.length-1]) {
        arr[i] = arr[i+1]
        }
      }
  }
  console.log(arr);
}
// removeAll([2, 4, 6, 8, 10], 3)

/* *********************************************************************************************************************************** */
// Given two numbers, coefficients M and B in the equation Y = MX + B. Build a function to return the X-intercept (the X-intercept is the
// value of X where Y equals zero.)
function coefficients(M, B) {
  var Y = 0;
  var X = (Y / M) - B;
    if (Y === 0) {
      console.log(X);
    }
}
// coefficients(3,5)

/* *********************************************************************************************************************************** */
// OUt of the last 100 days, there were 10 days with volcanos, 15 others with tsunamis, 20 earthquakes, 25 blizzards and 30 meteors (for
// 100 days total). If these probabilities continue, write whatHappensToday() to print a day's outcome.
function whatHappensToday() {
  var chance = Math.floor(Math.random()*100);
  if (chance <= 10) {
    console.log("There is a " + chance + "% chance of a volcano today.");
  }
  else if (chance <=25) {
    console.log("There is a " + chance + "% chance of a tsunami today.");
  }
  else if (chance <= 45) {
      console.log("There is a " + chance + "% chance of an earthquake today.");
  }
  else if (chance <= 60) {
      console.log("There is a " + chance + "% chance of a blizzard today.");
  }
  else {
      console.log("There is a " + chance + "% chance of a meteors today.");
  }
}
// whatHappensToday()

/* *********************************************************************************************************************************** */
// Change whatHappensToday() function to create whatReallyHappensToday(). In this new function test for each disaster independently, instead of
// assuming exactly one disaster will happen. In other words, with this new function, all give might occur today or none.
function whatReaHappensToday() {
  var chance = Math.floor(Math.random()*2); // 50/50 test for each of the disasters
  var count = 0;
  if (chance == 1) { // Test for volcano
    count += chance
  }
  if (chance == 1) { // Test for tsunami
    count += chance
  }
  if (chance == 1) { // Test for earthquake
    count += chance
  }
  if (chance == 1) { // Test for blizzard
    count += chance
  }
  if (chance == 1) { // Test for meteors
    count += chance
  }
  if (count > 0) {
    console.log("It's the end of the world!!!");
  }
  else {
    console.log("The world is safe!! For today...");
  }
}
// whatReaHappensToday()

/* *********************************************************************************************************************************** */
// Let's say a new Dojo student, Bodgdan, entered with a modest IQ of 101. Let's say during a 14 week bootcamp, his IQ rose by .01 on the first day
// then went up by an additional .02 on the second day, then up by .03 more on the third day, etc... all the way until increasing by .98
// on his 98th day. What is Bodgdan's final IQ?
function yourIQ() {
  var sum = 0;
  var smart = .01;
  for (var i = 0; i < 98; i++) {
    sum += smart
    smart += .01
  }
  console.log(sum);
}
// yourIQ()

/* *********************************************************************************************************************************** */
// Write a function that asigns and prints a letter grade given an integer representing a score from 0 to 100? Those getting 90+ get an 'A',
// 80-89 earn 'B', 70-79 is a 'C', 60-69 should get a 'D', and lower than 60 receive 'F'. For example, given 88, you should log 'Score': 88
// Grade: B". Given the score 61, log the string "Score: 61. Grade: D".
function myGrades(score) {
  var grade = '';
  var sign = '';
  if (score >= 90) {
    grade = 'A';
  }
  else if (score >= 80) {
    grade = 'B'
  }
  else if (score >= 70) {
    grade = 'C'
  }
  else if (score >= 60) {
    grade = 'D'
  }
  else {
    grade = 'F'
  }
  console.log("Score: " + score + " Grade: " + grade );
}
// myGrades(70)


/* *********************************************************************************************************************************** */
// For an additional challenge, add '-' signs to score in the bottom teo percent of A, B, C, and D scores, and '+' to the top two percent of B,
// C, and D scores. Given 99, console.log "Score: 88. Grade: B+". Given 61, log "Score: 61. Grade: D-".
function accurateGrades(score) {
  var grade = '';
  var sign = '';
  if (score >= 90) {
    grade = 'A';
  }
  else if (score >= 80) {
    grade = 'B'
  }
  else if (score >= 70) {
    grade = 'C'
  }
  else if (score >= 60) {
    grade = 'D'
  }
  else {
    grade = 'F'
  }
  if (score >= 88 && score <= 89 || score >= 78 && score <= 79 || score >= 68 && score <= 69){
    grade = grade + '+'
  }
  if (score >= 90 && score <= 91 || score >= 80 && score <= 81 || score >= 70 && score <= 71 || score >= 60 && score <= 61) {
    grade = grade + '-'
  }
  console.log("Score: " + score + " Grade: " + grade );
}
// accurateGrades(90)


/* *********************************************************************************************************************************** */
// Create a fill-in-the-blank quiz game. Ask the user's name, then refer to the user by name as you ask them a series of quesitons that you have stored
// in a n array. Use the prompt() function to get each input from the user and compare it to the answer you expected. When the users enter "Q" (for quit),
// or perhaps when the user hits [Cancel], exit the game and print the statistics of the game to the console: username, number of questions answered
// and questions correct.
