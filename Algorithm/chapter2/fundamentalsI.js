/* Assume that you have a text field is exactly 75 characters long. You want to fill it with spaces and asterisk ('*').
You should print the given number of asterisks consecutively. Depending on which functions is called, those stars should be
left-justified (first star would be very first char in the text field), or right-justified (last star would be very last char in the
text field, with potentially some number of spaces at beginning of text field before the block of stars start), or centered in the
75 character text field (with same number of spaces on either side of the block of start, plus/minus one).

Write a function drawLeftStars(num) that accepts a number and prints that many asterisks.
Write a function drawRightStars(num) that prints 75 characters total. Stars should build from right side. The last num characters should be
asterisks; the other 75 should be spaces.
Write a function drawCenteredStars(num) that prints 75 characters total. The stars should be centered in the 75. The middle num characters
should be asterisks; the rest of the 75 spaces.
(optional) Create epic text-art Empire vs Rebellion battle scenes, with ships like (=*=) and >o< */

function drawLeftStars(num) {
  var space = '';
  for (var i = 0; i < num; i++) {
    space += '*';
  }
  console.log(space);
}
// drawLeftStars(75)

function drawRightStars(num) {
  var space = '';
  for (var i = 0; i < 75; i++) {
    if (i == 75-num) {
      space += '*';
      num--;
    }
    else {
      space += '_';
    }
  }
  console.log(space);
}
// drawRightStars(7)

function drawCenteredStars(num) {
  var space = '';
  var start = Math.floor((75 - num) / 2);
  var end = Math.floor(start + num);
  for (var i = 0; i < 75; i++) {
    if (i <= start || i >= end)  {
      space +='_'
    }
    else {
      space +='*'
    }
  }
  console.log(space);
}
// drawCenteredStars(7)

/* *********************************************************************************************************************************** */
/* From the above, derive the following that accept and draw the given characters, not just asterisks:
drawLeftChars(num, char)   drawRightChars(num, char)     drawCenteredChars(num, char) */
function drawLeftChars(num, char) {
  var space = '';
  for (var i = 0; i < num; i++) {
    space += char;
  }
  console.log(space);
}
// drawLeftChars(75)

function drawRightChars(num, char) {
  var space = '';
  for (var i = 0; i < 75; i++) {
    if (i == 75-num) {
      space += char;
      num--;
    }
    else {
      space += '_';
    }
  }
  console.log(space);
}
// drawRightChars(7)

function drawCenteredChars(num, char) {
  var space = '';
  var start = Math.floor((75 - num) / 2);
  var end = Math.floor(start + num);
  for (var i = 0; i < 75; i++) {
    if (i <= start || i >= end)  {
      space += '_'
    }
    else {
      space += char
    }
  }
  console.log(space);
}
// drawCenteredChars(7)

/* *********************************************************************************************************************************** */
/* Create threeFives() that adds values from 100 and 400000(inclusive) if that value is evenly divisible by 3 or 5 but not both. Display final sum */
function threeFives() {
  var sum = 0;
  for (var i = 100; i <= 400000; i++) {
      if (i % 3 == 0 && i % 5 == 0) {
        continue;
      }
      if (i % 3 === 0 ||i % 5 === 0) {
        sum += i
        // console.log(i)
      }
  }
  console.log(sum);
}
// threeFives()

/* *********************************************************************************************************************************** */
/* Create betterThreesFives(start, end) that allows you to enter arbitrary start and end values for your range. */
function betterThreesFives(start, end) {
  var sum = 0;
  for (var i = start; i <= end; i++) {
      if (i % 3 == 0 && i % 5 == 0) {
        continue;
      }
      if (i % 3 === 0 ||i % 5 === 0) {
        sum += i
        // console.log(i)
      }
  }
  console.log(sum);
}
// betterThreesFives(100, 400000)

/* *********************************************************************************************************************************** */
/* Change is inevitable (especially when breaking a twenty). Make generateCoinChange(cents). Accept a number of American cents, compute and
print how to represent that amount with smallest number of coin. Common coins are pennies(1 cent), nickels(5 cents), dimes(10 cents), and quarter (25 cents).
Example output given(94): 3 quarters, 1 dime, 1 nickel, 4 pennies. */
function generateCoinChange(cents) {
  var arr = [];
  var coins = [25,10,5,1]
  for (var i = 0; i < 4; i++) {
    arr.push(Math.floor(cents/coins[i]))
    cents = cents % coins[i]
  }
  console.log("Quarters: " + arr[0] + " Dimes: " + arr[1] + " Nickels: " + arr[2] +" Pennies: " + arr[3]);
}
// generateCoinChange(87)

/* Add half-dollar(50 cents) and dollar (100 cents) coins with 40 additional characters or less. */
function generateMoreCoinChange(cents) {
  var arr = [];
  var coins = [100,50,25,10,5,1]
  for (var i = 0; i < 6; i++) {
    arr.push(Math.floor(cents/coins[i]))
    cents = cents % coins[i]
  }
  console.log("Dollars: " + arr[0] + " Half-Dollars: " + arr[1] + " Quarters: " + arr[2] + " Dimes: " + arr[3] + " Nickels: " + arr[4] +" Pennies: " + arr[5]);
}
// generateMoreCoinChange(197)

/* *********************************************************************************************************************************** */
/* Create a function messyMath(num) that will return the following sum: add all integers from 0 up to the given num , except for the
following special cases of our count value:
If current count (not num ) is evenly divisible by 3, don’t add to sum ; skip to the
next count ;
Otherwise, if current count is evenly divisible by 7, include it twice in sum instead of once;
Regardless of the above, if current count is exactly 1/3 of num , return -1 immediately.

For example, if given num is 4 , return 7 . If given num is 8 , return 34 . If given num is 15 , return -1 */



/* *********************************************************************************************************************************** */
Write a function that console.log s the number 1 , then " chick ", then " boom ", then " chick ", then 2 , then " chick ", " boom ", "
chick " – continuing the same  cycle for each number up to (including) 12.


/* *********************************************************************************************************************************** */
Kaitlin sees beauty in numbers, but also believes that less is more. Implement sumToOne(num) that sums a given integer’s digits
repeatedly until the sum is only one digit. Return that one-digit result. Example: sumToOne(928) returns 1 , because 9+2+8 = 19,
then 1+9 = 10, then 1+0 = 1 .


/* *********************************************************************************************************************************** */
Regardless of how hard a Dojo student works (and they should work hard), they need time now and then to unwind – like hands on a clock.
Traditional clocks are increasingly uncommon, but most can still read an analog clock’s hands of hours, minutes and seconds. Create
clockHandAngles(seconds) that, given a number of seconds since 12:00:00, prints angles (in degrees) of the hour, minute and second hands.
As review, 360 degrees form a full rotation. For input of 3600 secs (equivalent to 1:00:00), print "Hour hand: 30 degs. Minute hand:
0 degs. Second hand: 0 degs." For an input parameter seconds of 119730 (which is equivalent to 9:15:30 plus 24 hours!), you should log
"Hour hand: 277.745 degs. Minute hand: 93 degs. Second hand: 180 degs." Note: in the second example, the angle for the minute hand is not
simply 90 degrees; it has advanced a bit further, because of the additional 30 seconds in that minute so far.

Second: also calculate and print degrees for an additional “week hand” that rotates once each week.

/* *********************************************************************************************************************************** */
 Return whether a given integer is prime. Prime numbers are only evenly divisible by themselves and 1. Many highly optimized solutions exist,
 but for now just create one that is easy to understand and debug .

/* *********************************************************************************************************************************** */
 Ever since you arrived at the Dojo, you wanted one of those cool Coding Dojo sweatshirts – maybe even more than one. Let’s say they cost
 $20 (including tax), but friendly Josh gives a 9% discount if you buy two, a nice 19% discount if you buy three, or a sweet 35% discount if
 you buy four or more. He only accepts cash and says he doesn’t have coins, so you should round up to the nearest dollar. Build function
 sweatshirtPricing(num) that, given a number of sweatshirts, returns the cost.

/* *********************************************************************************************************************************** */
 Return to your previous clockHandAngles solution. Allow fractional values for input seconds , but change your implementation to print only
 integer values for angles (in degrees) of the various hands.
