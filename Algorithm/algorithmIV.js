// Given an array and a value Y, count and print the number of array values greater than Y.
function greaterY(arr, Y) {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > Y) {
      console.log(arr[i]);
      count++
    }
  }
  console.log("There are " + count + " values greater than Y.");
}
// greaterY([1,2,4,5,7,9], 2)

// Given an array, print the max, min and average values for that array.
function minMaxAvg(arr) {
  var max = arr[0];
  var min = arr[0];
  var sum = arr[0];
  for (var i = 1; i < arr.length; i++) {
    if(arr[i] > max) {
    max = arr[i];
    }
    if (arr[i] < min) {
      min = arr[i]
    }
    sum += arr[i]
  }
  var avg = sum / arr.length
  console.log("Max: " + max, "Min: " + min, "Avg: " + avg);
}
// minMaxAvg([2,3,4,])

// Given an array of numbers, create a function that returns a new array where negative values were replaced with the string ‘Dojo’.   For example, replaceNegatives( [1,2,-3,-5,5]) should return [1,2, "Dojo", "Dojo", 5].
function replaceNegatives(arr) {
  for (var i = 0; i < arr.length; i++) {
    if(arr[i] < 0) {
      arr[i] = 'Dojo';
    }
  }
  // console.log(arr);
  return arr;
}
replaceNegatives([1,2,-3,-5,5])

// Given array, and indices start and end, remove vals in that index range, working in-place (hence shortening the array).  For example, removeVals([20,30,40,50,60,70],2,4) should return [20,30,70].
function removeVals(arr,start,end) {

  var newarr = []
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] >= arr[start] && arr[i] <= arr[end]) {
      continue;
    }
    newarr.push(arr[i])
  }
  console.log(newarr);
}
removeVals([20,30,40,50,60,70],2,4)
