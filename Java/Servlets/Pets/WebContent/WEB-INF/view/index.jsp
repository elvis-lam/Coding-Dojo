<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<!DOCTYPE html>
	<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Make a Pet!</title>
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
	</head>
	
	<body>
		<div>
			<form action="Dogs" method="GET">
				<h2>Make a Dog!</h2>
				<div class="form-group">
				  <label>Name</label>
				  <input type="text" class="form-control" name="name">
				</div>
				<div class="form-group">
				  <label>Breed</label>
				  <input type="text" class="form-control" name="breed">
				</div>
				<div class="form-group">
				  <label>Weight (lb)</label>
				  <input type="number" class="form-control" name="weight">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>	
		</div>
		
		<div>
	
			<form action="Cats" method="GET">
			  <h2>Make a Cat!</h2>
				<div class="form-group">
				  <label>Name</label>
				  <input type="text" class="form-control" name="cName">
				</div>
				<div class="form-group">
				  <label>Breed</label>
				  <input type="text" class="form-control" name="cBreed">
				</div>
				<div class="form-group">
				  <label>Weight (lb)</label>
				  <input type="number" class="form-control" name="cWeight">
				</div>
				<button type="submit" class="btn btn-primary">Submit</button>
			</form>	
		</div>
	</body>
</html>