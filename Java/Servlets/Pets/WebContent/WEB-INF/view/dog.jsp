<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		<c:choose>
			<c:when test= "${doggie.weight < 30}" >
				<h3>You created a  <c:out value="${doggie.getBreed()}"/>!</h3>
				<h3><c:out value="${doggie.getName()}"/> hopped into your lap and cuddled you!</h3>
			</c:when>
			<c:otherwise>
				<h3><c:out value="${doggie.getName()}"/> curled up next to you.</h3>
			</c:otherwise>
		</c:choose>
	</body>
</html>