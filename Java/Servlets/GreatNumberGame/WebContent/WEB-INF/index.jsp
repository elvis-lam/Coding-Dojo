<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
		
		<style>
			.red, .green {
				border: 1px solid black;
				text-align: center;
				height: 200px;
				width: 200px;
				color: white;
				font-size: 16px;
			}
			.red {			
				background-color: red; 
			}
			
			.green {
				background-color: green;
			}
			
			h4 {
				margin-top: 75px;
			}
			
		</style>
	</head>
	<body>
		<div>
			<h2>Welcome to the Great Number Game!</h2>
			<h3>I am thinking of a number between 1 and 100</h3>
			<h3>Take a guess!</h3>
			
			<form method="POST">
				<input type="text" name="guess">
				<input type="submit" value="Submit">
			</form>
			
		</div>
		
      <c:if test = "${low == 1}">
		<div class="red">	
         <h4>TOO LOW!</h4>
		</div>
      </c:if>
		
      <c:if test = "${high == 2}">
		<div class="red">	
         <h4>TOO HIGH!</h4>
		</div>
      </c:if>
	
      <c:if test = "${correct == 0}">
		<div class="green">	
         <h4>CORRECT!</h4>
         
         <form method="POST" action="/GreatNumberGame/Home">
         	<input type="hidden" value="playAgain" name="playAgain">
         	<input type="submit" name="playAgain" value="Play Again?">
         </form>
         
         
		</div>
      </c:if>
	
	</body>
</html>