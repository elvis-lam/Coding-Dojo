package com.elvislam.controllers;

import java.io.IOException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Home
 */
@WebServlet("/Home")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static Integer number = 0;
    public static boolean result;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Home() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		Integer number = (Integer)session.getAttribute("number");
		Integer guess = (Integer)session.getAttribute("guess");
		
		if(number.toString().equals(guess.toString())) {
			request.setAttribute("correct", 0);
		}

		else if(number > guess) {
			request.setAttribute("low", 1);
		}

		else if(number < guess) {
			request.setAttribute("high", 2);
		}
		
		RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/index.jsp");
		view.forward(request, response);
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResposnse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		if (request.getParameter("playAgain") != null) {  
			session.invalidate();
			response.sendRedirect("Home");
			return; // <--- Here.
		}

		
		Integer guess = Integer.parseInt(request.getParameter("guess"));
		
		
		Random n = new Random();
		if (number == 0) {
			number = n.nextInt(100) + 1;
		}
		
		session.setAttribute("number", number);			
		
		request.setAttribute("guess", guess);
		session.setAttribute("guess", guess);
		System.out.println(guess);
		System.out.println(number);
		
		// TODO Auto-generated method stub
		
		response.sendRedirect("/GreatNumberGame/Home");	
	}

}
