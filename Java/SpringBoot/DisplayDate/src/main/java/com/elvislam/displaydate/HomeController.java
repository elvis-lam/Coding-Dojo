package com.elvislam.displaydate;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {
	
	@GetMapping("/")
	public String home() {
		return "index.jsp";
	}
	
	@PostMapping("/date")
	public String date() {
		return "redirect:/date.jsp";
	}
	
	@GetMapping("/date")
	public String send(Model model) {
		Date now = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, 'the' d 'of' MMMM yyyy");
		model.addAttribute("date", dateFormatter.format(now));
		return "date.jsp";
	}


	@PostMapping("/time")
	public String time() {
		return "redirect:/time.jsp";
	}
	
	@GetMapping("/time")
	public String clock(Model model) {
		Date now = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("hh:mm a");
		model.addAttribute("time", dateFormatter.format(now));
		return "time.jsp";
	}
	
}
