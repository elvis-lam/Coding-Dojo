package com.elvislam.authentication.controllers;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.elvislam.authentication.models.User;
import com.elvislam.authentication.services.UserService;
import com.elvislam.authentication.validator.UserValidator;

@Controller
public class MainController {
	private final UserService userService;
	private final UserValidator userValidator;

	public MainController(UserService userService, UserValidator userValidator) {
		this.userService = userService;
		this.userValidator = userValidator;
	}
	
	@GetMapping("/registration")
	public String register(@ModelAttribute("user") User user) {
		return "registrationPage.jsp";
	}
	
	@PostMapping("/registration")
	public String registerUser(@Valid @ModelAttribute("user") User user, BindingResult result, HttpSession session) {
		userValidator.validate(user, result);
		if(result.hasErrors()) {
			System.out.println("Errors");
			return "registrationPage.jsp";
		}
		else {
			User users = userService.registerUser(user);
			session.setAttribute("id", users.getId());
			return "redirect:/home";
		}
	}
	
	
	@GetMapping("/login") 
		public String login() {
			return "login.jsp";
	}
	
	@PostMapping("/login")
	public String loginUser(@RequestParam("email") String email, @RequestParam(value = "password") String password, Model model, HttpSession session) {
		boolean success = userService.authenicationUser(email, password);
		if(success) {
			User users = userService.findByEmail(email);
			session.setAttribute("id", users.getId());
			return "redirect:/home";
		}
		else {
			System.out.println("error loggin in");
			model.addAttribute("error", "Invalid Credentials.");
			return "login.jsp";
		}
	}
	
	
	@GetMapping("/home") 
	public String home(Model model, HttpSession session) {
		Long id = (Long) session.getAttribute("id");
		if(id == null) {
			return "redirect:/login";			
		}
		else {
			User users = userService.findUserById(id);
			model.addAttribute("user", users);
			return "home.jsp";
		}
	}
	
	@GetMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/login";
	}
}

