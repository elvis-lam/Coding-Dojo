<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Ninja Gold Game</title>
	</head>
	<body>
		<h2>Your Gold: <c:out value="${gold}"/></h2>
		
		<div id="wrapper">
			<div class="game">
				
				<form action="farm" method="POST">
					<h3>Farm</h3>
					<h4>(earns 10-20 gold)</h4>
					<input type="submit" value="Find Gold!">
				</form>
				
				<form action="cave" method="POST">
					<h3>Cave</h3>
					<h4>(earns 5-10 gold)</h4>
					<input type="submit" value="Find Gold!">
				</form>
				
				<form action="house" method="POST">
					<h3>House</h3>
					<h4>(earns 2-5 gold)</h4>
					<input type="submit" value="Find Gold!">
				</form>
				
				<form action="casino" method="POST">
					<h3>Casino!</h3>
					<h4>(earns/takes 0-50 gold)</h4>
					<input type="submit" value="Find Gold!">
				</form>
			</div> <!--  End of game div -->	
			
			<div class="activity">
				<h2>Activities:</h2>
				<div class="messages"> 
					<c:out value="${message}"/>
				</div>
			</div> <!--  End of activty div -->	
			
		</div> <!-- End of wrapper div -->
	</body>
</html>