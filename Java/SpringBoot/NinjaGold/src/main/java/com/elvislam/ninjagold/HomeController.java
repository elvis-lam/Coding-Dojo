package com.elvislam.ninjagold;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {
	public Integer gold = 0;
	public StringBuilder message = new StringBuilder();
	
	@GetMapping("/Gold")
	public String index(HttpSession session) {
		session.setAttribute("gold", gold);
		session.setAttribute("message", message);
		return "index.jsp";
	}
	
	@PostMapping("/farm")
	public String farm() {
		
		int random = (int)((Math.random()* 20 - 10) + 10);
		System.out.println(random);
		gold = (int) (gold + random);
		
		message.append("You entered a farm and earned " + random + " gold.");
		
		return "redirect:/Gold";
	}
	
	@PostMapping("/cave")
	public String cave() {
		
		int random = (int)((Math.random()* 10 - 5) + 5);
		System.out.println(random);
		gold = (int) (gold + random);
		
		message.append("You entered a Cave and earned " + random + " gold.");
		
		return "redirect:/Gold";
	}
	
	@PostMapping("/house")
	public String house() {
		
		int random = (int)((Math.random()* 5 - 2) + 2);
		System.out.println(random);
		gold = (int) (gold + random);
		
		message.append("You entered a House and earned " + random + " gold.");
		
		return "redirect:/Gold";
	}
	
	@PostMapping("/casino")
	public String casino() {
		
		int random = (int)((Math.random()* 20 - 10) +10);
		System.out.println(random);
		
		// Random to find win or lose chance
		int winLose = (int)((Math.random()* 2 -1) + 1);
		System.out.println(winLose);
		
		if (winLose == 1) {
			gold = (int) (gold + random);		
			
			message.append("You entered a Casino and earned " + random + " gold.");
		}
		else {
			gold = (int) (gold - random);	
			
			message.append("You entered a Casino and lossed " + random + ". Ouch!");
		}
		
		return "redirect:/Gold";
	}
	
}
