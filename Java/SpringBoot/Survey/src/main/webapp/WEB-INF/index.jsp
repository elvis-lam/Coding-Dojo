<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Dojo Survey Index</title>
	</head>
	<body>
	
	<form action="process" method="POST">
		<p>Your Name: <input type="text" name="name"></p>
		
		<p>Dojo Location: 
		<select name="location">
			<option value="San Jose">San Jose</option>
			<option value="Burbank">Burbank</option>
			<option value="Seattle">Seattle</option>
			<option value="Chicago">Chicago</option>
		</select></p>
		
		
		<p>Favorite Language:  
		<select name="language">
			<option value="Python">Python</option>
			<option value="MEAN">MEAN</option>
			<option value="Java">Java</option>
			<option value="Ruby">Ruby</option>
		</select></p>
		
		<p>Comment (optional):
		<textarea rows="3" cols="33" name="comment">
		</textarea>
		
		<input type="submit" value="Button">		
	</form>
	
	</body>
</html>