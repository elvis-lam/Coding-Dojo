<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
 
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		<p><a href="/languages/">Dashboard</a></p>
		
		<h1><c:out value="${lang.name}"/></h1>
		
		<h2><c:out value="${lang.creator}"/></h2>
		
		<h3><c:out value="${lang.version}"/></h3>
		
		<p><a href="edit/<c:out value="${lang.id}"/>">Edit</a></p>
		
		<p><a href="delete/<c:out value="${lang.id}"/>">Delete</a></p>
	</body>
</html>