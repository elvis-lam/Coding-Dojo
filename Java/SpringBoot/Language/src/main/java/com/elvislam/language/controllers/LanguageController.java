package com.elvislam.language.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.elvislam.language.models.Language;
import com.elvislam.language.services.LanguageService;

@Controller
@RequestMapping("/languages")
public class LanguageController {

	public final LanguageService lang;
	
	public LanguageController(LanguageService language) {
		this.lang = language;
	}
	
	@GetMapping("/")
	public String index(@Valid @ModelAttribute("lang") Language language, BindingResult result, Model model) {
		List<Language> languages = lang.allLanguages();
		model.addAttribute("languages", languages);
		
		if (result.hasErrors()) {
			return "languages/index.jsp";
		}
		else {
			return "languages/index.jsp";
		}
	}
	
	@PostMapping("/process")
	public String add(@Valid @ModelAttribute("lang") Language language, BindingResult result) {
		if (result.hasErrors()) {
			System.out.println("error");
			return "languages/index.jsp";
		}
		else {
			lang.create(language);
			return "redirect:/languages/";
		}
	}
	
	@GetMapping("/{id}")
	public String showOne(@PathVariable Long id, Model model) {
		Language languages = lang.findLanguage(id);
		if(languages == null) {
			return "redirect:/";
		}
		else {
			model.addAttribute("lang", languages);
		}
		return "languages/show.jsp";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@ModelAttribute("lang") @PathVariable("id") Long id, Model model) {
        Language languages = lang.findLanguage(id);
        model.addAttribute("lang", languages);
		return "languages/edit.jsp";
	}
	
	@PutMapping("/update/{id}")
	public String update(@Valid @ModelAttribute("lang") Language language, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("error edit");
			return "/languages/edit.jsp";
		}
		else {
			lang.update(language);
			return "redirect:/languages/";
		}
	}
	
	@DeleteMapping("/delete/{id}")
	public String destroy(@PathVariable("id") Long id) {
		lang.delete(id);
		return "redirect:/languages/";
	}
	
}
