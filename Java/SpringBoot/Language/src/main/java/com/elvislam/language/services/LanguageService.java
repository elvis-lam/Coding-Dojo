package com.elvislam.language.services;

import java.util.List;
import java.util.Optional;


import org.springframework.stereotype.Service;

import com.elvislam.language.models.Language;
import com.elvislam.language.repositories.LanguageRepository;

@Service
public class LanguageService {

		private final LanguageRepository language;
		
		public LanguageService(LanguageRepository lang) {
			this.language = lang;
		}
		
		public List<Language> allLanguages() {
			return language.findAll();
			
		}
		
		public Language create(Language id) {
			return language.save(id);
		}

		public Language findLanguage(Long id) {
			Optional<Language> optionalLang = language.findById(id);
			if(optionalLang.isPresent()) {
				return optionalLang.get();
			}
			else {				
				return null;
			}
		}

		public Language update(Language lang) {
			return language.save(lang);
		}
		
		public void delete(Long id) {
			language.deleteById(id);
		}
}
