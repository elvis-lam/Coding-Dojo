package com.codingdojo.events.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.codingdojo.events.models.Event;
import com.codingdojo.events.repositories.EventRepository;

@Service

public class EventService {
	private final EventRepository eventRepository;
	public EventService(EventRepository eventRepository) {
		this.eventRepository = eventRepository;
	}
	public List<Object[]> allEvents(String state){
		return eventRepository.findAllEventsInState(state);
	}
	public Event createEvent(Event event) {
		return eventRepository.save(event);
	}
	public Event findEvent(Long id) {
		Optional<Event> optionalEvent = eventRepository.findById(id);
		if(optionalEvent.isPresent()) {
			return optionalEvent.get();
			
		}else {
			return null;
		}
			
	}
	public List<Event> findAllEvents(String state){
		return eventRepository.findByState(state);
	}
	public List<Event> findAllEventsOut(String state){
		return eventRepository.findByStateNotIn(state);
	}
	public void deleteEvent(Long id) {
		Event event = findEvent(id);
		eventRepository.delete(event);
		return;
	}
	
	
	
	
	
	
	
	

}
