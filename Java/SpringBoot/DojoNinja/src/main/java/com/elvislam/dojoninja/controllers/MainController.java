package com.elvislam.dojoninja.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.elvislam.dojoninja.models.Dojo;
import com.elvislam.dojoninja.models.Ninja;
import com.elvislam.dojoninja.services.DojoService;
import com.elvislam.dojoninja.services.NinjaService;

@Controller
public class MainController {
	public final DojoService dojos;
	public final NinjaService ninjas;
	public MainController(DojoService dojos, NinjaService ninjas) {
		this.dojos = dojos;
		this.ninjas = ninjas;
	}
	
	@GetMapping("/dojo/new")
	public String dojo(@Valid @ModelAttribute("dojoNew") Dojo dojo) {
		return "/dojo/new.jsp";
	}
	
	@PostMapping("/dojo/new")
	public String dojoNew(@Valid @ModelAttribute("dojoNew") Dojo dojo, Model model, BindingResult result) {
		if(result.hasErrors()) {
			System.out.print("error");
			return "/dojo/new.jsp";
		}
		else {
			dojos.create(dojo);
			return "redirect:/dashboard";
		}
	}
	
	
	@GetMapping("/ninja/new")
	public String ninja(@Valid @ModelAttribute("ninjaNew") Ninja ninja, Dojo dojo, Model model) {
		List<Dojo> thisDojo= dojos.allDojo();
		model.addAttribute("thisDojo", thisDojo);
		List<Ninja> thisNinja= ninjas.allNinja();
		model.addAttribute("thisNinja", thisNinja);
		return "/ninja/new.jsp";
	}
	
	@PostMapping("/ninja/new")
	public String ninjaNew(@Valid @ModelAttribute("ninjaNew") Ninja ninja, Model model, BindingResult result) {
		if(result.hasErrors()) {
			System.out.print("error");
			return "/ninja/new.jsp";
		}
		else {
			ninjas.create(ninja);
			return "redirect:/dashboard";
		}
	}
	
	@GetMapping("/dojos/{id}")
	public String showOne(@PathVariable("id") Long id, Dojo dojo, Model model) {
		Dojo thisDojo = dojos.findDojo(id);
		model.addAttribute("thisDojo", thisDojo);
		return "/dojo/show.jsp";
	}
	
	@GetMapping("/dashboard")
	public String dashboard(Dojo dojo, Model model) {
		List<Dojo> thisDojo = dojos.allDojo();
		model.addAttribute("dojos", thisDojo);
		return "dojo/dashboard.jsp";
	}
	
		
}
