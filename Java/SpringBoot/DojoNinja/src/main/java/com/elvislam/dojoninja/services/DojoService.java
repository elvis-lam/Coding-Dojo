package com.elvislam.dojoninja.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.elvislam.dojoninja.models.Dojo;
import com.elvislam.dojoninja.repositories.DojoRepository;

@Service
public class DojoService {
	private final DojoRepository dojos;
	
	public DojoService(DojoRepository dojo) {
		this.dojos = dojo;		
	}
	
	public List<Dojo> allDojo() {
		return dojos.findAll();
	}
	
	public Dojo create(Dojo id) {
		return dojos.save(id);
	}
	
	public Dojo findDojo(Long id) {
		Optional<Dojo> optionalDojo = dojos.findById(id);
		if(optionalDojo.isPresent()) {
			return optionalDojo.get();
		}
		else {
			return null;
		}
	}
}
