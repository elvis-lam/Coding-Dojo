package com.elvislam.dojoninja.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.elvislam.dojoninja.models.Ninja;
import com.elvislam.dojoninja.repositories.NinjaRepository;

@Service
public class NinjaService {
	private final NinjaRepository ninjas;
		
		public NinjaService(NinjaRepository ninja) {
			this.ninjas = ninja;		
		}
		
		public List<Ninja> allNinja() {
			return ninjas.findAll();
		}
		
		public Ninja create(Ninja id) {
			return ninjas.save(id);
		}
		
//		public Dojo findDojo(Long id) {
//			Optional<Dojo> optionalDojo = dojos.findById(id);
//			if(optionalDojo.isPresent()) {
//				return optionalDojo.get();
//			}
//			else {
//				return null;
//			}
}
