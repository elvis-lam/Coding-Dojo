package com.elvislam.mirror.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;



@Controller
public class MainController {
	
	@GetMapping("/")
	public String index(Model model) {
		
		Date now = new Date();
		SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm a");
		SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, d MMMM yyyy");
		model.addAttribute("time", timeFormatter.format(now));
		model.addAttribute("date", dateFormatter.format(now));
		return "mirror/index.jsp";
		
		
	}
	
}
