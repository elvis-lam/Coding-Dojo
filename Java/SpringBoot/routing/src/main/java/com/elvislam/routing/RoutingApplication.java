package com.elvislam.routing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RequestMapping("/coding")
public class RoutingApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoutingApplication.class, args);
	}
	
	@GetMapping("/")
	public String index() {
		return "Hello Coding Dojo!";
	}
	
	@GetMapping("/python")
	public String python() {
		return "Python was Awesome!";
	}
	
	@GetMapping("/java")
	public String java() {
		return "Java/ Spring is better!";
	}
	
	@GetMapping("/dojo/")
	public String dojo() {
		return "The dojo is awesome!";
	}
	
	@GetMapping("/burbank-dojo/")
	public String burbank() {
		return "Burbank Dojo is located in Southen California!";
	}
	
	@GetMapping("/san-jose/")
	public String sanJose() {
		return "SJ Dojo is the headquarters!";
	}
	
	
	
}
