package com.elvislam.routing;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DojoController {
	@RequestMapping("/{track}/{module}")
	public String showLesson(@PathVariable("track") String track, @PathVariable("module") String module) {
		return "Track: " + track + ", Module: " + module;
	}

}
