package com.elvislam.dojooverflow.services;

import java.util.List;

import com.elvislam.dojooverflow.models.Tag;
import com.elvislam.dojooverflow.repositories.TagRepository;



public class TagService {
	private final TagRepository tagRepo;
	
	public TagService(TagRepository tagRepo) {
		this.tagRepo = tagRepo;
	}
	
	public List<Tag> allTag() {
		return tagRepo.findAll();		
	}
	
	public Tag create(Tag id) {
		return tagRepo.save(id);
	}
}
