package com.elvislam.dojooverflow.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.elvislam.dojooverflow.models.Question;
import com.elvislam.dojooverflow.repositories.QuestionRepository;

@Service
public class QuestionService {
	private final QuestionRepository questionRepo;

	public QuestionService(QuestionRepository questionRepo) {
		this.questionRepo = questionRepo;
	}
	
	public List<Question> allQuestion() {
		return questionRepo.findAll();		
	}
	
//	public List<Question> create(String question) {
//		return questionRepo.insert(question);
//	}

	public Question create(Question id) {
		return questionRepo.save(id);
	}
	
	public Question show(Long id) {
		Optional<Question> optionalQuest = questionRepo.findById(id);
		if(optionalQuest.isPresent()) {
			return optionalQuest.get();
		}
		else {
			return null;
		}
	}
}
