package com.elvislam.dojooverflow.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.elvislam.dojooverflow.models.Question;
import com.elvislam.dojooverflow.models.Tag;
import com.elvislam.dojooverflow.services.QuestionService;
import com.elvislam.dojooverflow.services.TagService;

@Controller
@RequestMapping("questions")
public class MainController {
	private final QuestionService questService;


	public MainController(QuestionService questService) {
		this.questService = questService;
	}
	
	@GetMapping("/new")
	public String addNew() {
		return "questions/new.jsp";
	}
	
	@PostMapping("/create")
	public String create(@RequestParam("question") String question, Model model) {
		
		System.out.println(question);
//		questService.create(question);
		
		return "redirect:/dashboard";
	}
}
