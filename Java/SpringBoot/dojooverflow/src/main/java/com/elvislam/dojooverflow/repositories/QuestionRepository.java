package com.elvislam.dojooverflow.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.elvislam.dojooverflow.models.Question;

@Repository
public interface QuestionRepository extends CrudRepository<Question, Long>{
	List<Question> findAll();
	
//	@Query(value="INSERT INTO questions (question) VALUES ?1", nativeQuery=true)
//	List<Question> insert(String question);
}
