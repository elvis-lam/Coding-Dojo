package com.elvislam.human;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HumanApplication {

	public static void main(String[] args) {
		SpringApplication.run(HumanApplication.class, args);
	}
	
	@GetMapping("/")
	public String index() {
		return "Hello Human!";
	}
	
	@RequestMapping("/")
	public String human(@RequestParam(value="name", required=false) String searchQuery) {
		if (searchQuery.isEmpty() || searchQuery == null) {
			searchQuery = "Human!";
		}		
		return "Hello " + searchQuery;
	}
}
