<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
	</head>
	<body>
		<h1>Products</h1>
		
		<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Category Count</th>
				</tr>
				
				<c:forEach items="${products}" var="product">
					<tr>
						<td><c:out value="${product.name}"/></td>
						<td><c:out value="${product.description}"/></td>
						<td>$<c:out value="${product.price}"/></td>
						<td><c:out value="${product.categories.size()}"/> categories</td>
					</tr>
				</c:forEach>
			</thead>
		</table>
	
		<h1>Categories</h1>
		
		<table>
			<thead>
				<tr>
					<th>Category Name</th>
					<th>Product Count</th>

				</tr>
				
				<c:forEach items="${categories}" var="category">
					<tr>
						<td><c:out value="${category.name}"/></td>
						<td><c:out value="${category.products.size()}"/> products</td>

					</tr>
				</c:forEach>
			</thead>
		</table>
	
	
	</body>
</html>