<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
	<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Category Page</title>
	</head>
	<body>
		<h1><c:out value="${category.name}"/></h1>
		
		<form action="/addProducts" method="POST">
		<input type="hidden" name="category" value="<c:out value="${category.id}"/>">
			<label>Add Product: </label>
			<select name="products">
				<c:forEach items="${products}" var="product">
					<option value="${product.id}"><c:out value="${product.name}"/></option>	
				</c:forEach>
			</select>
			<input type="submit" value="Add"/>
		</form>
		
		<h2>Products:</h2>
			<ul>
				<c:forEach items="${category.products}" var="product">
					<li><c:out value="${product.name}"/></li>				
				</c:forEach>
			</ul>
	</body>
</html>