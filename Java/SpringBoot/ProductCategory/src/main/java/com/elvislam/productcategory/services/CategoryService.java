package com.elvislam.productcategory.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.elvislam.productcategory.models.Category;
import com.elvislam.productcategory.repositories.CategoryProductRepository;
import com.elvislam.productcategory.repositories.CategoryRepository;

@Service
public class CategoryService {
	private final CategoryRepository categories;
	private final CategoryProductRepository categoryProductRepo;
	

	public CategoryService(CategoryRepository category, CategoryProductRepository categoryProductRepo) {
		this.categories = category;
		this.categoryProductRepo = categoryProductRepo;
	}
	
	public List<Category> allCategory() {
		return categories.findAll();
	}
	
	public Category create(Category id) {
		return categories.save(id);
	}
	
	public Category oneCategory(Long id) {
		Optional<Category> optionalCategory = categories.findById(id);
		if(optionalCategory.isPresent()) {			
			return optionalCategory.get();
		}
		else {
			return null;
		}
	}

	public void update(Category category) {
		categories.save(category);
	}

//	public void update(Long categoryId, Long productId) {
//		CategoryProduct cp = new CategoryProduct();
//		Category category = categories.findById(categoryId);
//		Product product = products.findById(categoryId);
//		cp.setCategory(category);
//		cp.setProduct(product);
//		System.out.println(cp);
//		categoryProductRepo.save(cp);
//	}
	
	
 }