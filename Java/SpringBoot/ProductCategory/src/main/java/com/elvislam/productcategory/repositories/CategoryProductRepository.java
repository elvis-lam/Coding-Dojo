package com.elvislam.productcategory.repositories;

import org.springframework.data.repository.CrudRepository;

import com.elvislam.productcategory.models.CategoryProduct;

public interface CategoryProductRepository extends CrudRepository<CategoryProduct, Long>{
	
}
