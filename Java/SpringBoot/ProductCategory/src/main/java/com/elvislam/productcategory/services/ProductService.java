package com.elvislam.productcategory.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import com.elvislam.productcategory.models.Product;
import com.elvislam.productcategory.repositories.ProductRepository;

@Service
public class ProductService {
	
	private final ProductRepository products;

	public ProductService(ProductRepository product) {
		this.products = product;
	}
	
	public List<Product> allProduct() {
		return products.findAll();
	}
	
	public Product create(Product id) {
		return products.save(id);
	}
	
	public Product oneProduct(Long id) {
		Optional<Product> optionalProduct = products.findById(id);
		if(optionalProduct.isPresent()) {
			return optionalProduct.get();
		}
		else {
			return null;
		}
	}

	public void update(Product product) {
		products.save(product);
		
	}
	
}
