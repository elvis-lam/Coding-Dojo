package com.elvislam.productcategory.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.elvislam.productcategory.models.Category;
import com.elvislam.productcategory.models.Product;
import com.elvislam.productcategory.services.CategoryService;
import com.elvislam.productcategory.services.ProductService;


@Controller
public class MainController {
	public final CategoryService categories;
	public final ProductService products;
	
	public MainController(CategoryService categories, ProductService product) {
		this.categories = categories;
		this.products = product;
	}
	
	@GetMapping("/categories/new")
	public String createCategory(@ModelAttribute("newCategory") Category category, Model model) {
		return "categoryProduct/categoryNew.jsp";
	}
	
	@PostMapping("/categories/new")
	public String createCategory(@Valid @ModelAttribute("newCategory") Category category, Model model, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("error add category");
			return "categoryProduct/categoryNew.jsp";
		}
		else {
			categories.create(category);
			return "redirect:/dashboard";
		}
	}
	
	@GetMapping("/categories/{id}")
	public String category(@PathVariable("id") Long id, @ModelAttribute("addProducts") Category category, Model model, Product product) {
		Category thisCategory = categories.oneCategory(id);
		model.addAttribute("category", thisCategory);
		List<Product> allProduct = products.allProduct();
		model.addAttribute("products", allProduct);
		return "categoryProduct/categoryShow.jsp";
	}
	
	@PostMapping("/addProducts")
	public String categoryProducts(@RequestParam("products") Long productId, @RequestParam("category") Long categoryId) {
		Category updatedCategory = categories.oneCategory(categoryId);
		Product addedProduct = products.oneProduct(productId);
		
		//get category's product list, add product to it
		List<Product> productList = updatedCategory.getProducts();
		productList.add(addedProduct);
		
		//set category's product list, update category
		updatedCategory.setProducts(productList);
		categories.update(updatedCategory);
		
		return "redirect:/categories/" + categoryId;
		
//		if(result.hasErrors()) {
//			return "categoryProduct/categoryShow.jsp";
//		}
//		else {			
////			categories.addProduct(category, product);
//			System.out.print(product);
//			System.out.print(category);
//			return "redirect:/categories";
//		}
	}
	
	
	@GetMapping("/dashboard")
	public String dashbaord(Model model) {
		List<Product> allProducts = products.allProduct();
		List<Category> allCategories = categories.allCategory();
		model.addAttribute("products", allProducts);
		model.addAttribute("categories", allCategories);
		return "categoryProduct/dashboard.jsp";
	}
	
	
	
	@GetMapping("/products/new")
	public String createCProduct(@ModelAttribute("newProduct") Product product, Model model) {
		return "categoryProduct/productNew.jsp";
	}
	
	@PostMapping("/products/new")
	public String createProduct(@Valid @ModelAttribute("newProduct") Product product, Model model, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("error add product");
			return "categoryProduct/productNew.jsp";
		}
		else {
			products.create(product);
			return "redirect:/dashboard";
		}
	}
	
	@GetMapping("/products/{id}")
	public String product(@PathVariable("id") Long id, @ModelAttribute("addCategories") Product product, Category category, Model model) {
		Product thisProduct= products.oneProduct(id);
		model.addAttribute("products", thisProduct);
		List<Category> allCategories = categories.allCategory();
		model.addAttribute("categories", allCategories);
		return "categoryProduct/productShow.jsp";
	}

	@PostMapping("/addCategories")
	public String productCategories(@RequestParam("products") Long productId, @RequestParam("categories") Long categoryId) {
		
		Product updatedProduct = products.oneProduct(productId);
		Category addCategory = categories.oneCategory(categoryId);

		List<Category> categoryList = updatedProduct.getCategories();
		categoryList.add(addCategory);
	
		updatedProduct.setCategories(categoryList);
		products.update(updatedProduct);
		return "redirect:/products/" + productId;
	}
	
	
	
}
