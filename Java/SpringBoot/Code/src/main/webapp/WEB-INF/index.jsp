<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Secret Code</title>
	</head>
	
	<body>
		<h2 style="color: red"><c:out value="${error}"/></h2>
		<form action="try" method="POST">
			<h2>What is the code?</h2>
			<input type="text" name="code">
			<input type="submit" value="Try Code">
		</form>			
	</body>
</html>