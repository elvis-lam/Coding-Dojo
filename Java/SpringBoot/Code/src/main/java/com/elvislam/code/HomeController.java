package com.elvislam.code;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController {
	
	@GetMapping("/")
	public String index() {
		return "index.jsp";
	}
	
	@PostMapping("/try")
	public String process(@RequestParam(value="code") String code, RedirectAttributes redirect) {
		if(!code.equals("bushido")) {
			redirect.addFlashAttribute("error", "You must train harder!");
			return "redirect:/";
		}
		else {			
			return "redirect:/code";
		}
	}
	
	@GetMapping("/code")
	public String code() {
		return "code.jsp";
	}
	
}
