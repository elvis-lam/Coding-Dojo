<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Search</title>
	</head>
	<body>
		<h4>Top Ten Songs: </h4>
		<h4><a href="/dashboard">Dashboard</a></h4>
		<div>
		${artist}
		${songs}
			<c:forEach items="${songs}" var="song">
			<p><c:out value="${song.rating}"></c:out> - <a href="/songs/<c:out value="${song.id}"/>"><c:out value="${song.title}"></c:out></a> - <c:out value="${song.artist}"></c:out></p>
			</c:forEach>
		</div>
		
	</body>
</html>