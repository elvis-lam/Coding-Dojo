<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Search</title>
	</head>
	<body>
		<h4><a href="/dashboard">Dashboard</a></h4>
		
		<div>
		<h1>Songs by artist: <c:out value="${artist}"/></h1>
		<form action="/search" method="POST">
			<input type="text" name="artist">
			<Input type="submit" value="New Search">
		</form>
			<table>
				<thead>
					<tr>
						<th>Name</th>
						<th>Rating</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${songs}" var="song">
						<tr>
							<td><c:out value="${song.title}"></c:out></td>
							<td><c:out value="${song.rating}"></c:out></td>
							<td><a href="/delete/<c:out value="${song.id}"></c:out>">Delete</a></td>
						</tr>
					</c:forEach>
			</tbody>	
			</table>
		</div>
	</body>
</html>