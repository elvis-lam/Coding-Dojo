package com.elvislam.lookify.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.elvislam.lookify.models.Song;
import com.elvislam.lookify.services.SongService;

@Controller
public class SongController {

		public final SongService songs;

		public SongController(SongService songs) {
			this.songs = songs;
		}
		
		@GetMapping("/")
		public String index() {
			return "songs/index.jsp";
		}
		
		@GetMapping("/dashboard")
		public String dashboard(Model model, Song song) {
			List<Song> allSong = songs.allSongs();
			model.addAttribute("songs", allSong);
			return "songs/dashboard.jsp";
		}
		
		// Adding new song to the DB
		@GetMapping("/songs/new")
		public String newSong(@Valid @ModelAttribute("song") Song song, BindingResult result) {
			return "songs/new.jsp";
		}
		
		@PostMapping("/addSong")
		public String addSong(@Valid @ModelAttribute("song") Song song, BindingResult result) {
			if(result.hasErrors()) {
				System.out.println("Error");
				return "songs/new.jsp";
			}
			else {				
				songs.create(song);
				return "redirect:/dashboard";
			}
		}
		
		@GetMapping("songs/{id}")
		public String show(@ModelAttribute("song") @PathVariable Long id, Model model) {
			Song song = songs.findSong(id);
			model.addAttribute("song", song);
			return "/songs/show.jsp";
		}
		
		@GetMapping("/search")
		public String search(Model model, Song song) {
			List<Song> search = songs.allSongs();
			model.addAttribute("songs", search);
			return "songs/search.jsp";
		}
		
		@GetMapping("/search/topTen")
		public String top(Model model, Song song) {
			List<Song> topTen = songs.topTen();
			model.addAttribute("songs", topTen);
			return "songs/song.jsp";
		}
		
		// Searching for the artist existing in DB and reroute to page with their songs
		@PostMapping("/search")
		public String search(@RequestParam("artist") String artist) {
			return "redirect:/search/" + artist;
		}
		
		// This allows mapping to get the artist and their songs
		@GetMapping("/search/{artist}")
		public String artist(Model model, @PathVariable("artist") String artist) {
			List<Song> song = songs.findArtist(artist);
			model.addAttribute("artist", artist);
			model.addAttribute("songs", song);
			return "songs/search.jsp";
		}
		
		// Delete song
		@RequestMapping("/delete/{id}")
		public String delete(@PathVariable("id") Long id) {
			songs.delete(id);
			return "redirect:/dashboard";
		}
}
