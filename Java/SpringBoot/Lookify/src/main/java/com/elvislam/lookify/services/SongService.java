package com.elvislam.lookify.services;

import java.util.List;
import java.util.Optional;


import org.springframework.stereotype.Service;

import com.elvislam.lookify.models.Song;
import com.elvislam.lookify.repositories.SongRepository;

@Service
public class SongService {
	
	private final SongRepository songs;

	public SongService(SongRepository song) {
		this.songs = song;
	}
	
	public List<Song> allSongs() {
		return songs.findAll();
	}
	
	public Song findSong(Long id) {
		Optional<Song> optionalLang = songs.findById(id);
		if(optionalLang.isPresent()) {
			return optionalLang.get();
		}
		else {				
			return null;
		}
	}

	public Song create(Song id) {
		return songs.save(id);
		
	}
	
	public List<Song> topTen() {
		return songs.findTop10ByOrderByRatingDesc();
	}

	public List<Song> findArtist(String artist) {
		return songs.findByArtist(artist);

	}

	public void delete(Long id) {
		songs.deleteById(id);
	}
	
}
