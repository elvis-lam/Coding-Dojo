package com.elvislam.countries.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.elvislam.countries.models.City;

@Repository
public interface CityRepository extends CrudRepository<City, Long>{

// What query would you run to display the total number of cities for each country? Your query should return the name of the country and the total number of cities. Your query should arrange the result by the number of cities in descending order.
	@Query(value = "SELECT countries.name, COUNT(cities.name) FROM cities JOIN countries ON cities.country_id = countries.id GROUP BY countries.name DESC", nativeQuery=true)
	List<Object[]> findAllCityInCountryDesc();
	
// What query would you run to get all the cities in Mexico with a population of greater than 500,000? Your query should arrange the result by population in descending order.
	@Query("SELECT ci.name, ci.population FROM City ci JOIN ci.country c WHERE ci.population > ?1 AND c.name = ?2 ORDER BY ci.name DESC")
	List<Object[]> findPopulationInMexico(Integer population, String name);
	
// What query would you run to get all the cities of Argentina inside the Buenos Aires district and have the population greater than 500, 000? The query should return the Country Name, City Name, District, and Population.
	@Query("SELECT c.name, ci.name, ci.district, ci.population FROM City ci JOIN ci.country c WHERE c.name = ?1 AND ci.district = ?2 AND ci.population > ?3")
	List<Object[]> findPopDist(String name, String city, Integer population);
}
