package com.elvislam.countries.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.elvislam.countries.models.Language;

@Repository
public interface LanguageRepository extends CrudRepository<Language, Long> {

// What query would you run to get all the countries that speak Slovene? Your query should return the name of the country, language, and language percentage. Your query should arrange the result by language percentage in descending order.
	@Query("SELECT c.name, l.language, l.percentage FROM Language l JOIN l.country c WHERE l.language = ?1 GROUP BY c.name ORDER BY c.name DESC")
	List<Object[]> joinCountriesAndLanugagesDesc(String name);
	
// What query would you run to get all languages in each country with a percentage greater than 89%? Your query should arrange the result by percentage in descending order.
	@Query("SELECT l.language, l.percentage FROM Language l JOIN l.country c WHERE l.percentage > ?1 ORDER BY l.language DESC")
	List<Object[]> findLanguageGreaterThanEightyNine(Integer percent);
}
