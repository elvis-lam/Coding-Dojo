package com.elvislam.countries.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.elvislam.countries.models.Country;

@Repository
public interface CountryRepository extends CrudRepository<Country, Long>{

// What query would you run to get all the countries with Surface Area below 501 and Population greater than 100,000?
	@Query("SELECT c.name, c.population FROM Country c WHERE c.surfaceArea < ?1 AND c.population > ?2")
	List<Country> countrySurfaceArea(Integer area, Integer population);
	
// What query would you run to get countries with only Constitutional Monarchy with a surface area of more than 200 and a life expectancy greater than 75 years?
//	@Query(value = "SELECT name, life_expectancy, surface_area FROM countries WHERE surface_area > ?1 AND life_expectancy > ?2 AND government_form = ?3", nativeQuery=true)
	@Query("SELECT c.name FROM Country c WHERE c.surfaceArea > ?1 AND c.lifeExpectancy > ?2 AND c.governmentForm = ?3")
	List<Country> countryLifeAreaForm(Integer area, Integer life, String form);
	
// What query would you run to summarize the number of countries in each region? The query should display the name of the region and the number of countries. Also, the query should arrange the result by the number of countries in descending order.	
//	@Query("SELECT c.region, COUNT(c.name) FROM Country c GROUP BY c.region DESC ORDER BY COUNT COUNT(c.name)")
	@Query(value="SELECT region, COUNT(name) FROM countries GROUP BY region DESC ORDER BY COUNT(name)", nativeQuery=true)
	List<Country> countryRegion();
}
