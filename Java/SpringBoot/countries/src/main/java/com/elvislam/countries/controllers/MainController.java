package com.elvislam.countries.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elvislam.countries.models.Country;
import com.elvislam.countries.services.ApiService;

@RestController
public class MainController {
	private final ApiService apiService;
	
		public MainController(ApiService apiService) {
		this.apiService = apiService;
	}

		@RequestMapping("/")
//		public List<Country> countryRegion() {
//			return apiService.countryRegionCount();
//		}
//		
//		public List<Object[]> buenoDistArea(String name, String city, Integer population) {
//			return apiService.buenosPopDist("argentina", "buenos aires", 500000);
//		}
		
//		public List<Country> countryLife(Integer area, Integer life, String form) {
//			return apiService.countryLifeArea(5010, 75, "Constitutional Monarchy");
//		}
		
//		public List<Country> countrySurfaceArea(Integer area, Integer population) {
//			return apiService.countryArea(501, 100000);	
//		}
//		
//		public List<Object[]> languageGreater(Integer percent) {
//			return apiService.findLanguagesGreater(89);
//		}
		
//		public List<Object[]> mexicoPopulation(Integer population, String name) {
//			return apiService.findMexicoPopulation(500000, "Mexico");
//		}
		
//		public List<Object[]> allCity() {
//			return apiService.findAllCity();
//		}
		
	public List<Object[]> slovene(String name) {			
		return apiService.findLanguageSlovene("Slovene");
	}
}
