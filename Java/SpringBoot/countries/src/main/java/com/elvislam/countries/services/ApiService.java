package com.elvislam.countries.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.elvislam.countries.models.Country;
import com.elvislam.countries.repositories.CityRepository;
import com.elvislam.countries.repositories.CountryRepository;
import com.elvislam.countries.repositories.LanguageRepository;

@Service
public class ApiService {
	private final LanguageRepository langRepo;
	private final CityRepository cityRepo;
	private final CountryRepository countRepo;

	
	public ApiService(LanguageRepository langRepo, CityRepository cityRepo, CountryRepository countRepo) {
		this.langRepo = langRepo;
		this.cityRepo = cityRepo;
		this.countRepo = countRepo;
	}
	
	public List<Object[]> findLanguageSlovene(String name) {
		return langRepo.joinCountriesAndLanugagesDesc(name);
	}
	
	public List<Object[]> findAllCity() {
		return cityRepo.findAllCityInCountryDesc();
	}
	
	public List<Object[]> findMexicoPopulation(Integer population, String name) {
		return cityRepo.findPopulationInMexico(population, name);
	}
	
	public List<Object[]> findLanguagesGreater(Integer percent) {
		return langRepo.findLanguageGreaterThanEightyNine(percent);
	}
	
	public List<Country> countryArea(Integer area, Integer population) {
		return countRepo.countrySurfaceArea(area, population);
	}
	
	public List<Country> countryLifeArea(Integer area, Integer life, String form) {
		return countRepo.countryLifeAreaForm(area, life, form);
	}
	
	public List<Object[]> buenosPopDist(String name, String city, Integer population) {
		return cityRepo.findPopDist(name, city, population);
	}
	
	public List<Country> countryRegionCount() {
		return countRepo.countryRegion();
	}
}
