<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>People and License</title>
	</head>
	<body>
		<h1>Information</h1>
		
		<table>
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>License#</th>
					<th>State</th>
					<th>Expiration Date</th>
				</tr>
				<c:forEach items="${person}" var="p">
					<tr>
						<td>${p.firstName}</td>
						<td>${p.lastName}</td>
						<td>${p.license.number}</td>
						<td>${p.license.state}</td>
						<td>${p.license.expirationDate}</td>
					</tr>		
				</c:forEach>
				
			</thead>
		</table>
	</body>
</html>