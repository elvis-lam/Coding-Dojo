package com.elvislam.driverlicense.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.elvislam.driverlicense.models.Person;
import com.elvislam.driverlicense.services.PersonService;

@Controller
@RequestMapping("/persons")
public class PersonController {

	public final PersonService persons;
	
	public PersonController(PersonService person) {
		this.persons = person;
	}
	
	@GetMapping("/")
	public String index(@Valid @ModelAttribute("create") Person person, BindingResult result) {
		return "person/index.jsp";
	}
	
	@PostMapping("/create")
	public String create(@Valid @ModelAttribute("create") Person person, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Error");
			return "person/index.jsp";
		}
		else {
			persons.create(person);
			return "redirect:dashboard";
		}
	}
	
	@GetMapping("/dashboard")
	public String home(Model model, Person person) {
		List<Person> allPerson = persons.allPerson();
		model.addAttribute("person", allPerson);
		return "person/dashboard.jsp";
	}
	
	@GetMapping("/{id}")
	public String show(@PathVariable("id") Long id, Model model, Person person) {
		Person onePerson = persons.findPerson(id);
		model.addAttribute("person", onePerson);
		return "person/show.jsp";
	}
}








