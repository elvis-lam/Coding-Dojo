package com.elvislam.driverlicense.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.elvislam.driverlicense.models.Person;
import com.elvislam.driverlicense.repositories.PersonRepository;

@Service
public class PersonService {
	
	private final PersonRepository persons;
	
	public PersonService(PersonRepository person) {
		this.persons = person;
	}
	
	public List<Person> allPerson() {
		return persons.findAll();
	}
	
	public Person create(Person id) {
		return persons.save(id);
	}
	
	public Person findPerson(Long id) {
		Optional<Person> optionalPerson = persons.findById(id);
		if(optionalPerson.isPresent()) {
			return optionalPerson.get();
		}
		else {				
			return null;
		}
	}
	
	
}
