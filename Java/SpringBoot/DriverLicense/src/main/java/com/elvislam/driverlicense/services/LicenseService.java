package com.elvislam.driverlicense.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.elvislam.driverlicense.models.License;
import com.elvislam.driverlicense.repositories.LicenseRepository;

@Service
public class LicenseService {
	
	private static Integer num = 000000;
	private final LicenseRepository licenses;
	
	public LicenseService(LicenseRepository license) {
		this.licenses = license;
	}
	
	public List<License> allLicense() {
		return licenses.findAll();
	}
	
	public License create(License license) {
		license.setNumber(num);
		num++;
		return licenses.save(license);
	}
	
}
