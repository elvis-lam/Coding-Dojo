package com.elvislam.driverlicense.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.elvislam.driverlicense.models.License;
import com.elvislam.driverlicense.models.Person;
import com.elvislam.driverlicense.services.LicenseService;
import com.elvislam.driverlicense.services.PersonService;

@Controller
@RequestMapping("/license")
public class LicenseController {
	
	public final LicenseService licenses;
	public final PersonService persons;
	
	public LicenseController(LicenseService license, PersonService person) {
		this.licenses = license;
		this.persons = person;
	}
	
	@GetMapping("/")
	public String index(@Valid @ModelAttribute("create") License license, Model model, BindingResult result) {
		List<Person> licensee = persons.allPerson();
		model.addAttribute("licenses", licensee);
		return "license/index.jsp";
	}
	
	@PostMapping("/create")
	public String create(@Valid @ModelAttribute("create") License license, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Error");
			return "license/index.jsp";
		}
		else {
			licenses.create(license);
			return "redirect:/persons/dashboard";
		}
	}
}
