<%@ page isErrorPage="true" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Events</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="css/home.css">
	</head>
	<body>
		<div class="welcome">
			<h1>Welcome <c:out value="${user.firstName}"/></h1>
			<h3><a href="logout" class="log">Logout</a></h3>
		</div>
				
		<c:out value="${attendees}"/>
		
		<div class="tables">
		<h4>Here are some of the events in my state</h4>
		
			<table class="table">
				<thead class="thead-dark">
					<tr>
					  <th scope="col">Name</th>
					  <th scope="col">Date</th>
					  <th scope="col">Location</th>
					  <th scope="col">Host</th>
					  <th scope="col">Action/ Status</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${events}" var="event">
					 <tr>
	   				   <td><a href="event/<c:out value="${event.id}"/>"><c:out value="${event.name}"/></a></td>
  				  	   <td><c:out value="${event.date}"/></td>
			  	   	   <td><c:out value="${event.location}"/></td>
		  	   	   	   <td><c:out value="${event.user.firstName}"/>
	  	   	   	   		<td><a href="join">Join</a>
						Joining <a href="">Cancel</a></td>
					 </tr>
				 </c:forEach>
				</tbody>
			</table>
		</div>
		
		
		<div class="tables">
			<h4>Here are some of the events in other state</h4>
			
			<table class="table">
				<thead class="thead-dark">
					<tr>
					  <th scope="col">Name</th>
					  <th scope="col">Date</th>
					  <th scope="col">Location</th>
					  <th scope="col">Host</th>
					  <th scope="col">Action/ Status</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${events}" var="event">
					 <tr>
	   				   <td><c:out value="${event.name}"/></td>
  				  	   <td><c:out value="${event.date}"/></td>
			  	   	   <td><c:out value="${event.location}"/></td>
		  	   	   	   <td><c:out value="${event.user.firstName}"/>
	  	   	   	   		<td><a href="join">Join</a>
						Joining <a href="">Cancel</a></td>
					 </tr>
				 </c:forEach>
				</tbody>
			</table>
		</div>
		
		
		<div class ="event">
			<h3 class="create">Create and Event</h3>
			<form:form action="/newEvent" method="POST" modelAttribute="newEvent">	
		  	<div class="form-group">
				<form:label path="name">Name: </form:label>
				<form:input path="name" class="form-control"/>
			</div>
			
		  	<div class="form-group">
				<form:label path="date">Date: </form:label>
				<form:input path="date" class="form-control"/>
			</div>
			
			<div class="form-row">
				<div class="form-group col-md-8">
					<form:label path="location">Location: </form:label>
					<form:input path="location" class="form-control"/>
				</div>
				<div class="form-group col-md-4">
					<label for="state">State</label>
					<form:select path="state" class="custom-select form-control">
						<option selected>Choose...</option>
						<form:option value="CA">CA</form:option>
						<form:option value="WA">WA</form:option>
						<form:option value="NY">NY</form:option>
						<form:option value="FL">FL</form:option>
					</form:select>
				</div>
			</div>
		
			<input type="submit" class="btn btn-primary" value="Register"/>
			</form:form>
		</div>
		
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</body>
</html>