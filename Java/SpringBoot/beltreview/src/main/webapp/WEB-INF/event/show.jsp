<%@ page isErrorPage="true" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title><c:out value="${event.name}"/></title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	</head>
	<body>
		<h1><c:out value="${event.name}"/></h1>
		
		<h3>Host: <c:out value="${event.user.firstName}"/> <c:out value="${event.user.lastName}"/></h3>
		<h4>Location: <c:out value="${event.location}"/>, <c:out value="${event.state}"/></h4>
		<h5>People who are attending this event: </h5>
		
		<table class="table">
			<thead class="thead-dark">
				<tr>
				  <th scope="col">Name</th>
				  <th scope="col">Location</th>

				</tr>
			</thead>
			<tbody>
				 <tr>
   				   <td></td>
			  	   <td></td>

				 </tr>
			</tbody>
		</table>
		
		<h2>Message Wall</h2>
		<div>
		
		</div>
		
		<h4>Add Comment:</h4>
		<form:form action="/newComment" method="POST" modelAttribute="comment">
			<p><form:textarea rows="4" cols="50" path="comment"></form:textarea></p>
			<input type="submit" value="Submit"/>
		</form:form>
		
		
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</body>
</html>