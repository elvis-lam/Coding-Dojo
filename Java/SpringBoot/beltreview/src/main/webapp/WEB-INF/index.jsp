<%@ page isErrorPage="true" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Welcome</title>
    	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
	
		<h1>Welcome</h1>
		
		<div id ="wrapper">
			
			<div class ="register">
				<h2>Register</h2>
				<p><c:out value="${errors}" /></p>
				<p><form:errors path="registerUser.*" /></p>
				
				<form:form action="register" method="POST" modelAttribute="registerUser">
  		
				
				  	<div class="form-group">
						<form:label path="firstName">First Name: </form:label>
						<form:input path="firstName" class="form-control"/>
					</div>
					
				  	<div class="form-group">
						<form:label path="lastName">Last Name: </form:label>
						<form:input path="lastName" class="form-control"/>
					</div>
					
				 	<div class="form-group">
						<form:label path="email">Email: </form:label>
						<form:input path="email" class="form-control"/>
					</div>
						
					<div class="form-row">
						<div class="form-group col-md-8">
							<form:label path="location">Location: </form:label>
							<form:input path="location" class="form-control"/>
						</div>
						<div class="form-group col-md-4">
							<label for="inputState">State</label>
							<form:select path="state" class="custom-select form-control">
								<option selected>Choose...</option>
								<form:option value="CA">CA</form:option>
							</form:select>
						</div>
					</div>
				
				  	<div class="form-group">
						<form:label path="password">Password: </form:label>
						<form:input path="password" type="password" class="form-control"/>
					</div>
				
		          	<div class="form-group">
			            <form:label path="passwordConfirmation">Password Confirmation:</form:label>
			            <form:password path="passwordConfirmation" class="form-control"/>
			        </div>
			
					<input type="submit" class="btn btn-primary" value="Register"/>
				</form:form>
			</div>
			
			
			<div class="login">
				
				<h2>Login</h2>
				<p><c:out value="${error}" /></p>
				<form action="login" method="POST">					
				  	<div class="form-group">		
							<label>Email: </label>
							<input type="text" name="l_email" class="form-control"/>
					</div>
					<div class="form-group">
							<label>Password: </label>
							<input type="password" name="l_password" class="form-control"/>
					</div>
					<input type="submit" class="btn btn-primary" value="Log In"/>
				</form>
			</div>
		
			
		</div>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	</body>
</html>