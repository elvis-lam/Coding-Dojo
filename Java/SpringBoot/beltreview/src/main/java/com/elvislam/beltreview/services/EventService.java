package com.elvislam.beltreview.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.elvislam.beltreview.models.Event;
import com.elvislam.beltreview.repositories.EventRepository;

@Service
public class EventService {
	
	private final EventRepository eventRepo;

	public EventService(EventRepository eventRepo) {
		this.eventRepo = eventRepo;
	}
	
	public List<Event> allEvent() {
		return eventRepo.findAll();
	}
	
	public Event createEvent(Event event) {
		return eventRepo.save(event);
	}
	
	public List<Event> getAllState(String state) {
		return eventRepo.findByState(state);
	}
	
	
	public Event findEvent(Long id) {
		Optional<Event> optionalEvent = eventRepo.findById(id);
		if(optionalEvent.isPresent()) {
			return optionalEvent.get();
		}
		else {				
			return null;
		}
	}
	
}
