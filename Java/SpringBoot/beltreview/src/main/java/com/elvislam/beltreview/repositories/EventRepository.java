package com.elvislam.beltreview.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.elvislam.beltreview.models.Event;

@Repository
public interface EventRepository extends CrudRepository<Event, Long> {
	List<Event> findAll(); 
	
//	@Query("SELECT c.state FROM Event WHERE c.state LIKE ?1")
//	List<Event> getState(String state);
	
	List<Event> findByState(String state);
}
