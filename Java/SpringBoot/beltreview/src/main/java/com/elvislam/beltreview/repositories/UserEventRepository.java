package com.elvislam.beltreview.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.elvislam.beltreview.models.UserEvent;

@Repository
public interface UserEventRepository extends CrudRepository<UserEvent, Long>{
	List<UserEvent> findAll();
}
