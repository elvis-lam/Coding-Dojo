package com.elvislam.beltreview.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;


import com.elvislam.beltreview.models.Message;
import com.elvislam.beltreview.repositories.MessageRepository;

@Service
public class MessageService {
	
	private final MessageRepository messRepo;

	public MessageService(MessageRepository messRepo) {
		this.messRepo = messRepo;
	}
	
	public List<Message> allMessage() {
		return messRepo.findAll();
	}
	
	public Message findMessage(Long id) {
		Optional<Message> optionalMess = messRepo.findById(id);
		if(optionalMess.isPresent()) {
			return optionalMess.get();
		}
		else {				
			return null;
		}
	}
	
	public  Message createEvent(Message comment) {
		return messRepo.save(comment);
	}
		
}
