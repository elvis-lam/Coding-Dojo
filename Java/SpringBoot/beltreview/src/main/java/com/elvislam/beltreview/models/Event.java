package com.elvislam.beltreview.models;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table
(name="events")
public class Event {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private Date date;
	private String location;
	private String state;
	
	@Column(updatable=false)
	private Date createdAt;
	private Date updatedAt;	
	
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
    	name = "users_events",
    	joinColumns = @JoinColumn(name = "event_id"),
    	inverseJoinColumns = @JoinColumn(name = "user_id")
	)
    private List<User> manyUsers; 
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    private User user;
    
	@OneToMany(mappedBy="user", fetch = FetchType.LAZY)
    private List<Message> messages;
    
	public Event() {
		
	}
	
	public Event(String name, Date date, String location, User user, List<Message> messages, List<User> manyUsers, String state,  Date createdAt, Date updatedAt) {
		this.name = name;
		this.date = date;
		this.location = location;
		this.user = user;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.messages = messages;
		this.user = user;
		this.manyUsers = manyUsers;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<User> getManyUsers() {
		return manyUsers;
	}

	public void setManyUsers(List<User> manyUsers) {
		this.manyUsers = manyUsers;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
    public List<Message> getMessages() {
    	return messages;
    }
    
    public void setMessages(List<Message> messages) {
    	this.messages = messages;
    }
    public List<User> getUsers() {
		return manyUsers;
	}

	public void setUsers(List<User> manyUsers) {
		this.manyUsers = manyUsers;
	}
    
	@PrePersist
    protected void onCreate(){
        this.createdAt = new Date();
    }
    @PreUpdate
    protected void onUpdate(){
        this.updatedAt = new Date();
    }
}
