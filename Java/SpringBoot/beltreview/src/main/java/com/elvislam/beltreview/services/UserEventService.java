package com.elvislam.beltreview.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.elvislam.beltreview.models.UserEvent;
import com.elvislam.beltreview.repositories.UserEventRepository;

@Service
public class UserEventService {
	private final UserEventRepository userEventRepo;

	public UserEventService(UserEventRepository userEventRepo) {
		this.userEventRepo = userEventRepo;
	}
	
	public List<UserEvent> allAtendees() {
		return userEventRepo.findAll();
	}
	
}
