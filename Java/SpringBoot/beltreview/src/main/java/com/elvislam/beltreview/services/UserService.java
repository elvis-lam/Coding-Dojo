package com.elvislam.beltreview.services;

import java.util.List;
import java.util.Optional;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.elvislam.beltreview.models.User;
import com.elvislam.beltreview.repositories.UserRepository;

@Service
public class UserService {
	private final UserRepository userRepo;

	public UserService(UserRepository userRepo) {
		this.userRepo = userRepo;
	}
	
	public List<User> allUser() {
		return userRepo.findAll();
	}
	
	public User registerUser(User user) {
		String hashed = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt());
		user.setPassword(hashed);
		return userRepo.save(user);
	}
	
	public User findByEmail(String email) {
		return userRepo.findByEmail(email);
	}
	
	public User findUserById(Long id) {
		Optional<User> optionalUser = userRepo.findById(id);
		
		if(optionalUser.isPresent()) {
			return optionalUser.get();
		}
		else {
			return null;
		}
	}
	
	public boolean authenicationOneUser(String email) {
		User user = userRepo.findByEmail(email);
		if(user == null) {
			return false;
		}
		return false;
	}
	
	public boolean authenicationUser(String email, String password) {
		User user = userRepo.findByEmail(email);
		
		if(user == null) {
			return false;
		}
		else {
			if(BCrypt.checkpw(password, user.getPassword())) {		
				return true;
			}
			else {				
				return false;
			}
		}
	}
	
	
}
