package com.elvislam.beltreview.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.websocket.Session;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.elvislam.beltreview.models.Event;
import com.elvislam.beltreview.models.Message;
import com.elvislam.beltreview.models.User;
import com.elvislam.beltreview.models.UserEvent;
import com.elvislam.beltreview.services.EventService;
import com.elvislam.beltreview.services.MessageService;
import com.elvislam.beltreview.services.UserEventService;
import com.elvislam.beltreview.services.UserService;
import com.elvislam.beltreview.validator.UserValidator;

@Controller
public class MainController {
	private final UserService userService;
	private final UserValidator userValidator;
	private final EventService eventService;
	private final UserEventService userEventService;
	private final MessageService messageService;

	public MainController(UserService userService, UserValidator userValidator, EventService eventService, UserEventService userEventService, MessageService messageService) {
		this.userService = userService;
		this.userValidator = userValidator;
		this.eventService = eventService;
		this.userEventService = userEventService;
		this.messageService = messageService;
	}
	
	@GetMapping("/")
	public String index(@ModelAttribute("registerUser") User regUser) {
		return "index.jsp";
	}
	
	@PostMapping("/register")
	public String register(@Valid @ModelAttribute("registerUser") User regUser, BindingResult result, HttpSession session, Model model) {
		userValidator.validate(regUser, result);
//		userValidator.emailValidate(regUser, regUser.getEmail(), result);
		
//		boolean success = userService.authenicationOneUser(regUser.getEmail());
//		if(!success) {
//			model.addAttribute("error", "Email already exist");
//		}
	
		User existingUser = userService.findByEmail(regUser.getEmail());
		if(existingUser != null) {
			model.addAttribute("errors", "Email already exist");
			System.out.println("email exist");
			return "index.jsp";
		}
		
		
		if(result.hasErrors()) {
			System.out.println("Errors");
			return "index.jsp";
		}
		else {
			System.out.println(regUser.getFirstName());
			User users = userService.registerUser(regUser);
			session.setAttribute("id", users.getId());
			return "redirect:/home";
		}
	}
	
	@PostMapping("/login")
	public String login(@ModelAttribute("registerUser") User regUser, @RequestParam("l_email") String email, @RequestParam("l_password") String password, Model model, HttpSession session) {
		if(email.length() == 0) {
			model.addAttribute("error", "Invalid Email");
			return "index.jsp";
		}
		if(password.length() == 0) {
			model.addAttribute("error", "Invalid Password");
			return "index.jsp";
		}
		boolean success = userService.authenicationUser(email, password);
		if(success) {
			User users = userService.findByEmail(email);
			session.setAttribute("id", users.getId());
			return "redirect:/home";
		}
		else {
			System.out.println("error loggin in");
			model.addAttribute("error", "Invalid Credentials.");
			return "index.jsp";
		}
	}
	
	@GetMapping("/home") 
	public String home(@ModelAttribute("newEvent") Event event, Model model, HttpSession session) {
		Long id = (Long) session.getAttribute("id");
		
		List <Event> events = eventService.allEvent();
		model.addAttribute("events", events);
		
		List<UserEvent> attendee = userEventService.allAtendees();
		model.addAttribute("attendees", attendee);
		
		
		
		if(id == null) {
			return "redirect:/";			
		}
		else {
			User users = userService.findUserById(id);
			List<Event> states = eventService.getAllState(users.getState());
			model.addAttribute("states", states);
			
			
			
			model.addAttribute("user", users);
			return "event/home.jsp";
		}
		
	}
	
	
	@GetMapping("/event/{id}")
	public String Profile(@ModelAttribute("comment") Message comment, @PathVariable("id") Long id, Model model, Event event, Message message, HttpSession session, BindingResult result) {
		Event events = eventService.findEvent(id);
//		Message messages = messageService.findMessage(id); 
		
		session.setAttribute("eventId", events);
		model.addAttribute("event", events);
//		model.addAttribute("messages", messages);
		return "event/show.jsp";
	}
	
	
	@PostMapping("/newComment")
	public String comment(@ModelAttribute("comment") Message comment, Model model, BindingResult result, HttpSession session) {
//		Long id = (Long) session.getAttribute("eventId");
//		Event e = eventService.findEvent(id);

		System.out.println(comment);
//		System.out.println(id);
//		if(result.hasErrors()) {
//			System.out.println("error");
			return "event/show.jsp";
//		}
//		else {			
//			comment.setEvent(e);
//			session.setAttribute("eventId", e);
//			messageService.createEvent(comment);
//			return "redirect:/event/" + id;
//		}
	}
	

	
	
	
	
	
	
	@PostMapping("/newEvent")
	public String event(@ModelAttribute("newEvent") Event event, Model model, BindingResult result, HttpSession session) {
		Long id = (Long) session.getAttribute("id");
		User users = userService.findUserById(id);
		System.out.println(id);
		if(result.hasErrors()) {
			System.out.println("error");
			return "home.jsp";
		}
		else {
			event.setUser(users);
			session.setAttribute("userId", users);
			eventService.createEvent(event);
			return "redirect:/home";
		}
	}

	
	
	@PostMapping("/join")
	public String join(Model model, UserEvent attendees, HttpSession session) {
		Long userId = (Long) session.getAttribute("id");
		User user = userService.findUserById(userId);	
		session.setAttribute("user", user);
		
		
		System.out.println(user);
		
		model.addAttribute("boolean", true);
		attendees.setUser(user);
		return "redirect/home";
	}
	
	
	
	
	
	
	
	
	
	
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}
}
