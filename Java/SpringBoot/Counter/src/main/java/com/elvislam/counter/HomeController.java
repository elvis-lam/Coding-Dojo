package com.elvislam.counter;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	
	public Integer count = 0;
	
	@GetMapping("/")
	public String index(HttpSession session) {
		count++;
		session.setAttribute("count", count);
		return "index.jsp";
	}
	
	@GetMapping("/counter")
	public String counter(HttpSession session) {
		Integer count = (Integer) session.getAttribute("count");
		session.setAttribute("count", count);
		return "counter.jsp";
	}
}
