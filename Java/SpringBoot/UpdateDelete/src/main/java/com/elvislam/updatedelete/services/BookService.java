package com.elvislam.updatedelete.services;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.elvislam.updatedelete.models.Book;
import com.elvislam.updatedelete.repositories.BookRepository;

@Service
public class BookService {
    // adding the book repository as a dependency

    private final BookRepository bookRepository;
    
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }
    // returns all the books
    public List<Book> allBooks() {
        return bookRepository.findAll();
    }
    // creates a book
    public Book createBook(Book b) {
        return bookRepository.save(b);
    }
    // retrieves a book
    public Book findBook(Long id) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if(optionalBook.isPresent()) {
            return optionalBook.get();
        } else {
            return null;
        }
    }
    // update a book API
    public Book updateBook(Long id, String title, String desc, String lang, Integer pages) {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if(optionalBook.isPresent()) {
            Book book = optionalBook.get();
           book.setTitle(title);
           book.setDescription(desc);
           book.setLanguage(lang);
           book.setNumberOfPages(pages);
           return bookRepository.save(book);
        }
        else {
        	return null;
        }
    }
    
    // delete a book
    public void deleteBook(String search) {
    	bookRepository.deleteByTitleStartingWith(search);
    }
    
    // update a book @Controller
	public Book update(@Valid Book book) {
		return bookRepository.save(book);
		
	}
	public void delete(Long id) {
		// TODO Auto-generated method stub
		bookRepository.deleteById(id);
	}

}