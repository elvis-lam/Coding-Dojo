package com.elvislam.bankaccount;

public class BankAccountTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BankAccount george = new BankAccount(1200.53, 250.00);
		george.deposit(1000.11, "Checking");
		george.withdraw(200, "savings");
		george.getBalance();
		
		
		BankAccount mike = new BankAccount(1000, 1000);
		mike.deposit(550.29, "savings");
		mike.withdraw(900, "Checking");
		mike.getBalance();
	}

}
