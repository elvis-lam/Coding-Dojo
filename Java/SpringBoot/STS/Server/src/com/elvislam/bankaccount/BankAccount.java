package com.elvislam.bankaccount;

import java.util.ArrayList;

public class BankAccount {
	
	protected final String accountNumber;
	protected double checking;
	protected double savings;

	static int createdAccounts;
	static ArrayList<BankAccount> balance = new ArrayList<BankAccount>();
	
	// State how much is in the current bank account
	public BankAccount(double checking, double savings) {
		this.checking = checking;
		this.savings = savings;
		this.accountNumber = generateAccount().toString();
		createdAccounts++;
		balance.add(this);
		
		System.out.println("Customer's Account Number: " + accountNumber);
		System.out.println("Numbers of Accounts created: " + createdAccounts);
	}
	
	
	private StringBuilder generateAccount() {
		StringBuilder rand = new StringBuilder();
		for (int i = 0; i < 10; i++) {			
			rand.append((int)(Math.random() * 10));
		}
		return rand;
	}
	
	public double checkBalance() {
		System.out.print("Checking Balance: $" + this.checking);
		return this.checking;
	}
	
	public double saveBalance() {
		System.out.print("Saving Balance: $" + this.savings);
		return this.savings;
	}
	
	public void deposit(double amount, String account) {
		if (account == "savings" || account == "Savings") {			
			this.savings += amount;
			System.out.println("Deposited into Savings: $" + amount);
		}
		else if (account == "checking" || account == "Checking"){
			this.checking += amount;
			System.out.println("Deposited into Checking: $" + amount);			
		}
		else {
			System.out.println("Please enter amount to deposit and which account type.");
		}
	}	
	
	public void withdraw(double amount, String account) {
		
		if (account == "savings" || account == "Savings") {
			if (this.savings < amount) {				
				System.out.println("Insufficent funds");
			}
			else {
				this.savings -= amount;
				System.out.println("Withdrawn from Savings: $" + amount);
			}
		}
		
		else if (account == "checking" || account == "Checking"){
			if (this.checking < amount) {				
				System.out.println("Insufficent funds");
			}
			else {
				this.checking -= amount;
				System.out.println("Withdrawn from Checking: $" + amount);							
			}
		}
		
		else {
			System.out.println("Please enter amount to withdraw and which account type.");
		}
	}
	

	public static void getBalance() {
		double allCheck = 0;
		double allSave = 0;
		
		for (BankAccount total: balance) {
			allCheck += total.checking;
			allSave += total.savings;
		}
		System.out.println("Total for Checking: $" + allCheck);
		System.out.println("Total for Savings: $" + allSave);
	}
	

}
