package com.elvislam.phone;

public abstract class Phone implements Ringable {
	private String versionNumber;
    private int batteryPercentage;
    private String carrier;
    private String ringTone;
    
    public Phone(String versionNumber, int batteryPercentage, String carrier, String ringTone){
    	
        this.versionNumber = versionNumber;
        this.batteryPercentage = batteryPercentage;
        this.carrier = carrier;
        this.ringTone = ringTone;
    }

	public String getCarrier() {
		return carrier;
	}

	public String getRingTone() {
		return ringTone;
	}

	public abstract void displayInfo();


    
	    // getters and setters removed for brevity. Please implement them yourself
	}