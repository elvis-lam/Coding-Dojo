package com.elvislam.phone;

public interface Ringable {
	
	void ring();
    void unlock();
	
}
