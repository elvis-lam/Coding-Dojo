package com.elvislam.phone;

public class Galaxy extends Phone implements Ringable {
    public Galaxy(String versionNumber, int batteryPercentage, String carrier, String ringTone) {
        super(versionNumber, batteryPercentage, carrier, ringTone);
    }
    @Override
    public void ring() {
    	System.out.println(getRingTone());
    }
    @Override
    public void unlock() {
    	System.out.println("Unlocking via fingerprint");  
    }
    @Override
    public void displayInfo() {
    	System.out.println("iPhone from " + getCarrier());            
    }
}