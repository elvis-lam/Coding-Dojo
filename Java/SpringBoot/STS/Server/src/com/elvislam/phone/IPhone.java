package com.elvislam.phone;

public class IPhone extends Phone implements Ringable {
    public IPhone(String versionNumber, int batteryPercentage, String carrier, String ringTone) {
        super(versionNumber, batteryPercentage, carrier, ringTone);
    }
    
        
    @Override
    public void ring() {
    	System.out.println(getRingTone());
    	
    }

    @Override
    public void displayInfo() {
        System.out.println("iPhone from " + getCarrier());          
    }


	@Override
	public void unlock() {
		// TODO Auto-generated method stub
		System.out.println("Unlocking via facial recognition");  
		
	}
}