package com.elvislam.doublylinkedlist;

public class DLL {
	public Node head;
	public Node tail;
	
	
	public DLL() {
		this.head = null;
		this.tail = null;
	}

  public void push(Node newNode) {
    
        if(this.head == null) {
            this.head = newNode;
            this.tail = newNode;
            return;
        }
        
        Node lastNode = this.tail;
        lastNode.next = newNode;
        newNode.previous = lastNode;
        this.tail = newNode;
    }
	
  
  // This method prints the values of the nodes from the tail to the head.
	public void printValuesBackward() {
		Node lastNode = this.tail;
		
		
		while(lastNode != null) {
			System.out.println(lastNode.value);
			lastNode = lastNode.previous;
		}
	}
	
	//  This method removes the last node of our DLL and returns it.
	public void pop() {
		Node lastNode = this.tail;
		Node current = this.head;
		
		
		if (current == null) {
			lastNode = lastNode.previous;
			lastNode.next = null;
			System.out.println(lastNode.value);
		}
		
	}
	
	
	// This method returns a boolean whether the value in the argument is in the list or not. Return true if the value exists, else, return false.
	public boolean contains(Integer val) {
		Node runner = this.head;
		
        if(this.head == null) {
            return false;
        }
        
//		Node lastNode = this.tail;
		
		while (runner != null) {
			if(runner.value.equals(val)) {
				System.out.println("Found " + val);
				return true;
			}
			else {
				System.out.println("Did not find");
				return false;
			}
		}
		return true;
	}
	
	// Returns the number of nodes in the list.
	public int size() {
        if(this.head == null) {
            return 0;
        }
        Node runner = this.head;
        Integer count = 0;
        
        
        while(runner != null) {
        	count++;
        	runner = runner.next;
        }
        System.out.println("There are " + count + " Nodes in this list");
        
        return count;
        
	}
}
