package com.elvislam.objectmaster;

public class Human {
	public int health;
	public int strength;
	public int stealth;
	public int intelligence;
	public int count;
	
	public Human() {
		this.health = 100;
		this.strength = 3;
		this.stealth = 3;
		this.intelligence = 3;
	}
	
	public void attack(Human human) {
		System.out.println("Current Health: " + this.health);
		System.out.println("I hit you");
		this.health -= human.strength;
		System.out.println("Health: " + this.health);
	}
	
}
