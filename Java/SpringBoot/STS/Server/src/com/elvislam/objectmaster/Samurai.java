package com.elvislam.objectmaster;

public class Samurai extends Human {
	
	public Samurai() {
		super();
		health = 200;
		count += 1;
	}
	
	public void deathBlow(Human enemy) {
		System.out.println("Sayonara... Samurai's Health: " + this.health);
		enemy.health = 0;
		this.health -= (this.health/2);
		System.out.println("Enemy's Health: " + enemy.health +  " Samurai's Health: " + this.health);
	}
	
	public void meditate() {
		System.out.println("Peace, quiet, I regain health");
		this.health += (this.health/ 2);
		System.out.println("Samuari's Health: " + this.health);
	}
	
	public void howMany() {
		System.out.println("There are " + this.count + " Samuaris.");
	}
}
