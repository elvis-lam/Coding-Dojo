package com.elvislam.objectmaster;

public class Ninja extends Human {
	
	public Ninja() {
		super();
		stealth = 10;
	}
	
	public void steal(Human enemy) {
		System.out.println("I'm stealing your stealth");
		enemy.health -= this.stealth;
		this.health += this.stealth;
		System.out.println("Enemy's Health: " + enemy.health +  " Ninja's Health: " + this.health);
	}
	
	public void fireball(Human enemy) {
		System.out.println("I am destroying you");
		enemy.health -= (this.intelligence * 3);
		System.out.println("Enemy Health: " + enemy.health);
	}
}
