package com.elvislam.objectmaster;

public class Wizard extends Human {
	public Wizard() {
		super();
		health = 50;
		intelligence = 8;
	}
	
	public void heal(Human friend) {
		System.out.println("I'm healing you my friend");
		friend.health += this.intelligence;
		System.out.println("Friend's Health: " + friend.health);
	}
	
	public void fireball(Human enemy) {
		System.out.println("I am destroying you");
		enemy.health -= (this.intelligence * 3);
		System.out.println("Enemy Health: " + enemy.health);
	}
	
	
}
