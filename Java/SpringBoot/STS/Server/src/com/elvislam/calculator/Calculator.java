package com.elvislam.calculator;

public class Calculator {
	private double setOperandOne;
	private double setOperandTwo;
	private String operation;
	private double answer;
	
	public double getSetOperandOne() {
		return setOperandOne;
	}
	
	public void setSetOperandOne(double setOperandOne) {
		this.setOperandOne = setOperandOne;
	}
	
	public double getSetOperandTwo() {
		return setOperandTwo;
	}
	
	public void setSetOperandTwo(double setOperandTwo) {
		this.setOperandTwo = setOperandTwo;
	}
	
	public String getOperation() {
		return operation;
	}
	
	public void setOperation(String operation) {
		if (operation == "-" || operation == "+") {			
			this.operation = operation;
		}
		else {
			System.out.println("Error");
		}
	}
	
	public void performOperation() {
		if (getOperation() == "+") {
			this.answer = getSetOperandOne() + getSetOperandTwo();
		}
		else if (getOperation() == "-" ) {
			this.answer = getSetOperandOne() - getSetOperandTwo();
		}
		else {
			System.out.println("Error");
		}
	}
	
	public void results() {
		System.out.println(this.answer);
	}
	
	
}
