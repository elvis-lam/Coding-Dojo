package com.elvislam.zookeeper;

public class Gorilla extends Mammal {

	public void throwSomething() {
		System.out.println("Gorilla throw hard");
		if (this.energyLevel < 5) {
			this.energyLevel = 0;
		}
		else {			
			this.energyLevel -= 5;
		}
		System.out.println(displayEnergy());
	}
	
	public void eatBananas() {
		System.out.println("Gorilla eat yum yum");
		this.energyLevel += 10;
		System.out.println(displayEnergy());
	}
	
	public void climb() {
		System.out.println("Gorilla climb now");
		if (this.energyLevel < 10) {
			this.energyLevel = 0;
		}
		else {			
			this.energyLevel -= 10;
		}
		System.out.println(displayEnergy());
	}
}
