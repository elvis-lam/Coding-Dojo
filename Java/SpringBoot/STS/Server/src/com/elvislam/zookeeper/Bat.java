package com.elvislam.zookeeper;

public class Bat extends Mammal {
	
	public Bat() {
		super();
		energyLevel = 300;
		// TODO Auto-generated constructor stub
	}

	public void fly() {
		System.out.println("I fly fly");
		if (energyLevel < 50) {			
			this.energyLevel = 0;
		}
		else {
			this.energyLevel -= 50;
		}
		System.out.println(displayEnergy());
	}
	
	public void eatHumans() {
		System.out.println("Drinking blood");
		this.energyLevel += 25;
		System.out.println(displayEnergy());
	}
	
	public void attackTown() {
		System.out.println("Destroy");
		if (energyLevel < 100) {			
			this.energyLevel = 0;
		}
		else {
			this.energyLevel -= 100;
		}
		System.out.println(displayEnergy());
	}
}
