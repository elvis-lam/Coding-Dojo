import java.util.*;
import java.util.HashMap;
import java.util.Set;

public class HashMapTest {

  public static void main(String[] args) {

    HashMap<String, String> trackList = new HashMap<String, String>();
    trackList.put("Seven Years", "Anthony Green");
    trackList.put("Act I", "Sounds of Animals Fighting");
    trackList.put("Act II", "Sounds of Animals Fighting");
    trackList.put("Act III", "Sounds of Animals Fighting");
    Set<String> keys = trackList.keySet();
    for(String key : keys) {
      // System.out.println(key);
      // System.out.println(trackList.get(key));
      System.out.println(key + ": " + trackList.get(key));
    }
  }
}
