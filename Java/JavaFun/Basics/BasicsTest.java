
public class BasicsTest {
  public static void main(String[] args) {

    // Write a method that prints all the numbers from 1 to 255.
    Basics all = new Basics();
    all.printAll(255);

    // Write a method that prints all the odd numbers from 1 to 255.
    Basics odds = new Basics();
    odds.oddNumbers(255);

    // Write a method that prints the numbers from 0 to 255, but this time print the sum of the numbers that have been printed so far.
    Basics sum = new Basics();
    sum.sum(255);

    // Given an array X, say [1,3,5,7,9,13], write a method that would iterate through each member of the array and print each value on the screen. Being able to loop through each member of the array is extremely important.
    Basics array = new Basics();
    int[] x = {1,3,5,7,9,13};
    array.printArray(x);

    // Write a method (sets of instructions) that takes any array and prints the maximum value in the array. Your method should also work with a given array that has all negative numbers (e.g. [-3, -5, -7]), or even a mix of positive numbers, negative numbers and zero.
    Basics max = new Basics();
    int[] y = {0,-1,-3,-5, 5};
    // System.out.println(max.max(y));

    // Write a method that takes an array, and prints the AVERAGE of the values in the array. For example for an array [2, 10, 3], your method should print an average of 5.
    Basics avg = new Basics();
    int[] z = {2, 10, 3};
    // System.out.println(avg.average(z));

    // Write a method that creates an array 'y' that contains all the odd numbers between 1 to 255.
    Basics oddArr = new Basics();
    // System.out.println(oddArr.oddArray(255));

    // Write a method that takes an array and returns the number of values in that array whose value is greater than a given value y.
    Basics total = new Basics();
    int[] a = {3,5,2,6};
    // total.total(a, 4);

    // Given any array x, say [1, 5, 10, -2], write a method that multiplies each value in the array by itself.
    Basics squared = new Basics();
    int[] b = {1,5,10,-2};
    // squared.squared(b);

    // Given any array x, say [1, 5, 10, -2], write a method that replaces any negative number with the value of 0.
    Basics negative = new Basics();
    int[] c = {1,5,10,-2};
    // negative.negative(c);

    // Given any array x, say [1, 5, 10, -2], write a method that returns an array with the maximum number in the array, the minimum value in the array, and the average of the values in the array.
    Basics maxMinAvg = new Basics();
    int[] d = {1,5,10,-2};
    // maxMinAvg.maxMinAvg(d);

    // Given any array x, say [1, 5, 10, 7, -2], write a method that shifts each number by one to the front. For example, when the method is done, an x of [1, 5, 10, 7, -2] should become [5, 10, 7, -2, 0].
    Basics shift = new Basics();
    int[] e = {1,5,10,-2};
    System.out.println(shift.shift(e));
  }
}
