import java.util.ArrayList;

public class Basics {

  // Write a method that prints all the numbers from 1 to 255.
   public void printAll(int num) {
     for (int i = 1; i < num ; i++) {
       // System.out.println(i);
     }
   }

  // Write a method that prints all the odd numbers from 1 to 255.
  public void oddNumbers(int num) {
    for (int i = 1; i < num ; i++) {
      if (i % 2 == 1) {
        // System.out.println(i);
      }
    }
  }

  // Write a method that prints the numbers from 0 to 255, but this time print the sum of the numbers that have been printed so far.
  public void sum(int num) {
    int sum = 0;
    for (int i = 0; i < num ; i++) {
      sum += i;
      // System.out.println("New number: " + i + " Sum: " + sum);
    }
  }

  // Given an array X, say [1,3,5,7,9,13], write a method that would iterate through each member of the array and print each value on the screen. Being able to loop through each member of the array is extremely important.
  public void printArray(int[] array) {
    for (int i = 0; i < array.length; i++) {
      // System.out.println(array[i]);
    }
  }

  // Write a method (sets of instructions) that takes any array and prints the maximum value in the array. Your method should also work with a given array that has all negative numbers (e.g. [-3, -5, -7]), or even a mix of positive numbers, negative numbers and zero
  public Integer max(int[] array) {
    Integer max = array[0];
    for (int i = 0; i < array.length; i++) {
      if (array[i] > max) {
        max = array[i];
      }
    }
    return max;
  }

  // Write a method that takes an array, and prints the AVERAGE of the values in the array. For example for an array [2, 10, 3], your method should print an average of 5.
  public double average(int[] array) {
    double avg = 0;
    int sum = 0;
    for (int i = 0; i < array.length; i++) {
      sum += array[i];
    }
    avg = sum / array.length;
    return avg;
  }

  // Write a method that creates an array 'y' that contains all the odd numbers between 1 to 255.
  public ArrayList oddArray(int num) {
    ArrayList<Integer> myArray = new ArrayList<Integer>();
    for (int i = 0; i < num; i++ ) {
      if (i % 2 == 1) {
        myArray.add(i);
      }
    }
    return myArray;
  }

  // Write a method that takes an array and returns the number of values in that array whose value is greater than a given value y.
  public void total(int[] array, int y) {
    for (int i = 0; i < array.length; i++ ) {
      if (array[i] > y) {
        System.out.println(array[i]);
      }
    }
  }

  // Given any array x, say [1, 5, 10, -2], write a method that multiplies each value in the array by itself.
  public void squared(int[] array) {
    ArrayList<Integer> newArr = new ArrayList<Integer>();
    for (int i = 0; i < array.length; i++) {
      newArr.add(array[i] *= array[i]);
    }
    // System.out.println(newArr);
  }

  // Given any array x, say [1, 5, 10, -2], write a method that replaces any negative number with the value of 0.
  public void negative(int[] array) {
    ArrayList<Integer> arr = new ArrayList<Integer>();
    for (int i = 0; i < array.length; i++) {
      if (array[i] < 0) {
        array[i] = 0;
        arr.add(array[i]);
      }
      else {
        arr.add(array[i]);
      }
    }
    System.out.println(arr);
  }

  // Given any array x, say [1, 5, 10, -2], write a method that returns an array with the maximum number in the array, the minimum value in the array, and the average of the values in the array.
  public void maxMinAvg(int[] array) {
    int max = array[0];
    int min = array[0];
    double avg = 0;
    for (int i = 0; i < array.length; i++) {
      if (array[i] > max) {
        max = array[i];
      }
      if (array[i] < min) {
        min = array[i];
      }
      avg += array[i];
    }
    avg = avg / array.length;
    System.out.println("Minimum: " + min + " Maximum: " + max + " Average: " + avg);
  }

  // Given any array x, say [1, 5, 10, 7, -2], write a method that shifts each number by one to the front. For example, when the method is done, an x of [1, 5, 10, 7, -2] should become [5, 10, 7, -2, 0].
  public ArrayList shift(int[] array) {
    ArrayList<Integer> shiftArr = new ArrayList<Integer>();
    for (int i = 0; i < array.length-1; i++) {
      array[i] = array[i + 1];
      shiftArr.add(array[i]);
    }
    shiftArr.add(0);
    return shiftArr;
  }

}
