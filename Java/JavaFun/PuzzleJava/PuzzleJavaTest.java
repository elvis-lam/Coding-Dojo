public class PuzzleJavaTest {

  public static void main(String[] args) {

    // Create an array with the following values: 3,5,1,2,7,9,8,13,25,32. Print the sum of all numbers in the array. Also have the function return an array that only includes numbers that are greater than 10.
    PuzzleJava greaterThanTen = new PuzzleJava();
    int[] a = {3,5,1,2,7,9,8,13,25,32};
    // System.out.println(greaterThanTen.greaterThanTen(a));

    // Create an array with the following values: Nancy, Jinichi, Fujibayashi, Momochi, Ishikawa. Shuffle the array and print the name of each person. Have the method also return an array with names that are longer than 5 characters.
    PuzzleJava shuffle = new PuzzleJava();
    // shuffle.shuffle();

    //  Create an array that contains all 26 letters of the alphabet (this array must have 26 values). Shuffle the array and display the last letter of the array. Have it also display the first letter of the array. If the first letter in the array is a vowel, have it display a messag
    PuzzleJava alpha = new PuzzleJava();
    // alpha.alpha();

     // Generate and return an array with 10 random numbers between 55-100.
     PuzzleJava random = new PuzzleJava();
     // System.out.println(random.ran(55, 100));

   //  Generate and return an array with 10 random numbers between 55-100 and have it be sorted (showing the smallest number in the beginning). Display all the numbers in the array. Next, display the minimum value in the array as well as the maximum value.
   PuzzleJava randomer = new PuzzleJava();
   // System.out.println(randomer.random(55, 100));

   // Create a random string that is 5 characters long.
   PuzzleJava ransom = new PuzzleJava();
   // System.out.println(ransom.randStr(5));

   // Generate an array with 10 random strings that are each 5 characters long
   PuzzleJava ransomer = new PuzzleJava();
   // System.out.println(ransomer.randSent(5));

  }

}
