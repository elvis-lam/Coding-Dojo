import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.*;

public class PuzzleJava {

  // Create an array with the following values: 3,5,1,2,7,9,8,13,25,32. Print the sum of all numbers in the array. Also have the function return an array that only includes numbers that are greater than 10
  public String greaterThanTen(int[] array) {
    ArrayList<Integer> greatArr = new ArrayList<Integer>();
    Integer sum = 0;
    for (int i = 0; i < array.length; i++) {
      if (array[i] > 10) {
        greatArr.add(array[i]);
      }
      sum += array[i];
    }
    return "Total sum of Array: " + sum + " Values greater than 10: " + greatArr;
  }

  // Create an array with the following values: Nancy, Jinichi, Fujibayashi, Momochi, Ishikawa. Shuffle the array and print the name of each person.
  public void shuffle() {
    ArrayList<String> nameArr = new ArrayList<String>();
    nameArr.add("Nancy");
    nameArr.add("Jinichi");
    nameArr.add("Fujibayashi");
    nameArr.add("Momochi");
    nameArr.add("Ishikawa");
    Collections.shuffle(nameArr);
    // System.out.println(nameArr);

    // Have the method also return an array with names that are longer than 5 characters.
    for (int i = 0; i < nameArr.size(); i++) {
      if (nameArr.get(i).length() > 5) {
        // System.out.println(nameArr.get(i));
      }
    }
  }

   // Create an array that contains all 26 letters of the alphabet (this array must have 26 values). Shuffle the array and display the last letter of the array. Have it also display the first letter of the array. If the first letter in the array is a vowel, have it display a message
   public void alpha() {
     String[] oldArr = new String[] {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
     String[] vowels = {"a", "e", "i", "o", "u"};

     ArrayList<String> alphaArr = new ArrayList<String>();
     alphaArr.addAll(Arrays.asList(oldArr));
     Collections.shuffle(alphaArr);

     for (int i = 0; i < vowels.length; i++) {
       if (vowels[i].equals(alphaArr.get(0))) {
         System.out.println("The shuffled alphabet: " +  alphaArr.get(0) + " equals vowel: " + vowels[i]);
       }
       else {
         System.out.println("The shuffled alphabet: " + alphaArr.get(0) + " is not a vowel: " + vowels[i] + " ");
       }
     }
     System.out.println("The last letter of this shuffle is " + alphaArr.get(25));
   }

   // Generate and return an array with 10 random numbers between 55-100.
   public ArrayList ran(int num, int num2) {
     Random rand = new Random();
     ArrayList<Integer> ransom = new ArrayList<Integer>();
     for (int i = 0; i < 10; i++) {
       ransom.add(rand.nextInt(num2 - num) +  num);
     }
     return ransom;
   }

   //  Generate and return an array with 10 random numbers between 55-100 and have it be sorted (showing the smallest number in the beginning). Display all the numbers in the array. Next, display the minimum value in the array as well as the maximum value.
   public String random(int num, int num2) {
     Random ran = new Random();
     ArrayList<Integer> random = new ArrayList<Integer>();
     for (int i = 0; i < 10; i++) {
       random.add(ran.nextInt(num2 - num) +  num);
       Collections.sort(random);
     }
     Integer max = random.get(0);
     Integer min = random.get(random.size()-1);
     return "Sorted list: " + random + " Max: " + max + " Min: " + min;
   }

   // Create a random string that is 5 characters long.
   public String randStr(int num) {
     String[] oldArrr = new String[] {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
     ArrayList<String> strArr = new ArrayList<String>();
     ArrayList<String> newList = new ArrayList<String>();
     strArr.addAll(Arrays.asList(oldArrr));
     Collections.shuffle(strArr);

     for (int i = 0; i < num; i++) {
       newList.add(strArr.get(i));
     }
     return newList.toString();
   }

   // Generate an array with 10 random strings that are each 5 characters long
   public String randSent(int num) {
     String[] old = new String[] {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
     ArrayList<String> senArr = new ArrayList<String>();
     ArrayList<String> thisList = new ArrayList<String>();
     String character = "";

     senArr.addAll(Arrays.asList(old));

     for (int i = 0; i < 10; i++) {
       for (int j = 0; j < num; j++) {
         Collections.shuffle(senArr);
         character += senArr.get(j);
       }
     }
     thisList.add(character);
     return thisList.toString();
   }
}
