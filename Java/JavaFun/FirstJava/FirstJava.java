public class FirstJava {
  public static void main(String[] args) {
    String name = "Elvis";
    int age = 28;
    String city = "Los Angeles, CA";
    System.out.println(String.format("My name is: %s%n", name)); // String Interpolation
    System.out.println(String.format("I am %s years old %n", age));
    System.out.printf("My hometown is %s", city);
  }
}
