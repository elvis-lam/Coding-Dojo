public class FizzBuzz {
  public String fizzz(int number) {
    if (number % 3 == 0 && number % 5 == 0) {
      return "FizzBuzz: " + number;
    }
    else if (number % 3 == 0) {
      return "Fizz: " + number;
    }
    else if (number % 5 == 0) {
      return "Buzz: " + number;
    }
    return "" + number;
  }
}
