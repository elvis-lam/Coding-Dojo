class ProjectClass {
  private String name;
  private String description;

  // Setting up the Constructors
  public ProjectClass() {
    // empty contructor
  }

  public ProjectClass(String name) {
    this.name = name;

  }

  public ProjectClass(String name, String description) {
    this.name = name;
    this.description = description;
  }

  // Setting up the Getters and Setters
  public String getName() {
    return this.name;
  }

  public String elevatorPitch() {
    String all = this.name + ": " + this.description;
    return all;
  }
}
