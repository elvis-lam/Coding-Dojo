public class SinglyLinkedList {
  public Node head;

  public SinglyLinkedList() {
    this.head = head;
  }

  // Add a node to the list
  public void add(int value) {
    Node newNode = new Node(value);

    if(head == null) {
      head = newNode;
    }
    else {
      Node runner = head;
      while (runner.next != null) {
        runner = runner.next;
      }
      runner.next = newNode;
    }
  }

  // Remove last node in SLL
  public void remove() {
    if (this.head.next == null) {
      this.head = null;
    }
    else {
      Node runner = this.head;
      while(runner.next != null) {
        runner = runner.next;
      }
      runner = null;
    }
  }

  // Remove node at value
  public void removeVal(int value) {
    if (this.head.next == null) {
      this.head = null;
    }

    Node runner = head;
    while(runner.next != null) {

      if (value == runner.next.value) {
        runner.next = runner.next.next;
      }
      runner = runner.next;
    }
  }

  // Print node
  public void printValues() {
    if (this.head == null) {
      System.out.println("No nodes in list");
    }
    else {
      Node runner = this.head;
      int count = 0;
      while(runner != null) {
        count = count + 1;
        System.out.println("Node " + count + ": " + runner.value);
        runner = runner.next;
      }
    }
  }

}
