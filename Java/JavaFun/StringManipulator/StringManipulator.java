public class StringManipulator {

    // Trim both input strings and then concatenate them. Return the new string.
  public String trimAndConcat(String str, String str2) {
    String trimAndConcat = (str.trim().concat(str2.trim()));
    return trimAndConcat;
  }

  //  Get the index of the character and return either the index or null. If the character appears multiple times, return the first index.
  public int getIndexOrNull(String str, char str2) {
    int getIndexOrNull = str.indexOf(str2);
    return getIndexOrNull;
  }

  //  Get the index of the start of the substring and return either the index or null.
  public int getIndex(String str, String str2) {
    int getIndex = str.indexOf(str2);
    return getIndex;
  }

  //  Get a substring using a starting and ending index, and concatenate that with the second string input to our method.
  public String concatSubstring(String str, int num, int num2, String str2) {
    String concatSubstring = str.substring(num,num2).concat(str2);
    return concatSubstring;
  }


}
