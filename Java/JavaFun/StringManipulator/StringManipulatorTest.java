public class StringManipulatorTest {
  public static void main(String[] args) {

    // Trim both input strings and then concatenate them. Return the new string.
    StringManipulator manipulator = new StringManipulator();
    String trim = manipulator.trimAndConcat("   Hello    ", "    World    ");
    // System.out.println(trim);

    //  Get the index of the character and return either the index or null. If the character appears multiple times, return the first index.
    StringManipulator manipulator2 = new StringManipulator();
    char letter = 'o';
    Integer a = manipulator2.getIndexOrNull("Coding", letter);
    Integer b = manipulator2.getIndexOrNull("Hello World", letter);
    Integer c = manipulator2.getIndexOrNull("Hi", letter);
    // System.out.println(a);
    // System.out.println(b);
    // System.out.println(c);

    // Get the index of the start of the substring and return either the index or null.
    StringManipulator manipulator3 = new StringManipulator();
    String word = "Hello";
    String subString = "llo";
    String notString = "world";
    Integer d = manipulator3.getIndex(word, subString);
    Integer e = manipulator3.getIndex(word, notString);
    // System.out.println(d);
    // System.out.println(e);

    // Get a substring using a starting and ending index, and concatenate that with the second string input to our method.
    StringManipulator manipulator4 = new StringManipulator();
    String word2 = manipulator4.concatSubstring("Hello", 1, 2, "World");
    System.out.println(word2);

  }
}
 
