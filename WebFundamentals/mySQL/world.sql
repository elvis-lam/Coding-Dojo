SELECT title, description, release_year, rating, special_features
FROM film
WHERE rating = 'G' AND special_features LIKE 'behind the scenes';