/* *********************************************************************************************************************************** */
// Given an array, write a function that changes all positive numbers in the array to "big". Example: makeItBig([-1, 3, 5, -5]) returns
// that same array, changed to [-1, "big", "big", -5].

function makeItBig(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      arr[i] = "big";
    }
  }
  return arr;
}
makeItBig([-1, 3, 5, -5])

/* *********************************************************************************************************************************** */
// Create a function that takes array of numbers. The function should print the lowest value in the array, and return the highest values in the array.

function lowHigh(arr) {
var low = 0;
var high = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > high) {
      high = arr[i];
    }
    if (arr[i] < arr.length-1) {
      low = arr[i];
    }
  }
  console.log(low);
  return high;
}
lowHigh([2, 5, 10, 40, 1, -100])

/* *********************************************************************************************************************************** */
// Build a function that array of numbers. The function should print second-to-last value in the array, and return first odd value in the array.

function secondOdd(arr) {
  var firstOdd = 0;
  var secondLast = arr[arr.length-2]
  console.log(secondLast);

  for (var i = 0; i < arr.length; i++) {
    if (arr[i] % 2 === 1) {
      firstOdd = arr[i];
      return firstOdd;
    }
  }
}
// secondOdd([1, 2, 3, 4, 2])

/* *********************************************************************************************************************************** */
// Given array, create a function to return a new array where each value in the original has been doubled. Calling double([1, 2, 3]) should
// return [2, 4, 6] without changing original.

function double(arr) {
  var newArr = [];
  for (var i = 0; i < arr.length; i++) {
    newArr.push(arr[i] + arr[i]);
  }
  console.log(newArr);
}
// double([1, 2, 3])

/* *********************************************************************************************************************************** */
// Given array of numbers, create function to replace last value with number of positive values. Example, countPositives([-1, 1, 1, 1])
// changes array to [-1, 1, 1, 3].

function countPositives(arr) {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      count++;
    }
    if (i === arr.length-1) {
      arr[arr.length-1] = count;
    }
  }
  // console.log(count);
  console.log(arr);
}
// countPositives([-1, 1, 1, 1])

/* *********************************************************************************************************************************** */
// Create a function that accepts an array. Every time that array has three odd values in a row, print "That's odd!" Every time the array
// that has three evens in a row, print "Even more so!"

function threeRow(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] % 2 === 1 && arr[i+1] % 2 === 1 && arr[i+2] % 2 === 1) {
      console.log("That's odd!");
    }
    else if (arr[i] % 2 === 0 && arr[i+1] % 2 === 0 && arr[i+2] % 2 === 0) {
      console.log("Even more so!");
    }
  }
}
// threeRow([1, 3, 5, 2, 8])

/* *********************************************************************************************************************************** */
// Given arr, add 1 to odd elements ([1], [3]. etc.) console.log all values and return arr.

function addOdd(arr) {
  for (var i = 0; i < arr.length; i++) {
    if (i % 2 === 1) {
      arr[i] += 1
    }
  }
  console.log(arr);
}
// addOdd([1, 2, 3, 5])

/* *********************************************************************************************************************************** */
// Passed with an array containing strings. Working within the same array, replace each string with a number - the length of the string at
// previous array index - and return the array.

function string(arr) {
  for (var i = 0; i < arr.length; i++) {
    arr[i] = arr[i].length
  }
  return arr;
}
// string(['hello', 'hi', 'hey'])

/* *********************************************************************************************************************************** */
// Build function that accepts array. Returns a new array with all values except first, adding 7 to each, Do not alter the original array.

function seven(arr) {
  var newArr = [];
  for (var i = 0; i < arr.length; i++) {
    if (i === 0) {
      continue;
    }
    else {
      arr[i] += 7;
      newArr.push(arr[i]);
    }
  }
  console.log(newArr);
}
// seven([1, 2, 3, 4, 5, 8])

/* *********************************************************************************************************************************** */
// Given array, write function that reverses values, in-place. Example: reverse([3, 1, 6, 4, 2]) returns same array containing [2, 4, 6, 1, 3]

function reverse(arr) {
  var newArr = [];
  for (var i = arr.length-1; i >= 0; i--) {
    newArr.push(arr[i])
  }
  console.log(newArr)
}
// reverse([1,2,3,4,5,6,7,8,9,10])

/* *********************************************************************************************************************************** */
// Given an array, create and return a new one containing all the values of the provided array, made negative (not simply multiplied by 1)
// Given [1, -3, 5], return [-1, -3, -5].

function negatives(arr) {
  var newArr = [];
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] > 0) {
      arr[i] = -arr[i]
      newArr.push(arr[i])
    }
    else {
      newArr.push(arr[i])
    }
  }
  console.log(newArr);
}
// negatives([1, -3, 5])

/* *********************************************************************************************************************************** */
// Create a function that accepts an array, and prints "yummy" each time one of the value is equal to "food". If no array elements are
// "food", then print "I'm hungry" once.

function yum(arr) {
  var count = 0;
  for (var i = 0; i < arr.length; i++) {
    if (arr[i] === "food") {
      console.log("yummy");
      count++;
    }
  }
  if (count === 0) {
    console.log("I'm hungry");
  }

}
// yum(['string', 'food', 'food'])

/* *********************************************************************************************************************************** */
// Given array, swap first and last, third and third-to-last, etc. Input[true, 42, "Ada", 2, "pizza"] becomes ['pizza', 42, "Ada", 2, true],
// Change [1, 2, 3, 4, 5, 6] to [6, 2, 4, 3, 5, 1].

function center(arr){
  var decoy = 0;
  var third = 0;
  for (var i = 0; i < arr.length; i++) {
    decoy = arr[0];
    arr[0] = arr[arr.length-1];
    arr[arr.length-1] = decoy;
    third = arr[2];
    arr[2] = arr[arr.length-3];
    arr[arr.length-3] = third;
    return arr;
  }
}
// center([true, 42, "Ada", 2, "pizza"])
// center([1,2,3,4,5,6]);


/* *********************************************************************************************************************************** */
// Given array arr and number num, multiple each arr value by num, and return the changed arr.

function multi(arr, num) {
  var newArr = [];
  for (var i = 0; i < arr.length; i++) {
    newArr.push(arr[i] * num)
  }
  console.log(newArr);
}

// multi([1,2,3], 3)
