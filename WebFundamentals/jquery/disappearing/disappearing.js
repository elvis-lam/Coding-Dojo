$(document).ready(function() {

  // Giving the h1 tag a different font
  $("h1").css("font-family", "Comic Sans MS, cursive, sans-serif")

  // When the images are clicked, that image is then hidden
  $("img").click(function() {
    $(this).hide();
  });

  // Adjust the size of the image
  $("img", this).css("height", "150px");
    $(this).css("width", "150px");

  // Allowing the button to do the opposite of hide when clicked
  $("#reset").click(function() {
    $("img").show();
  })
  // Style the button
  $("#reset").css("background-color", "#009e0f");
  $("#reset").css("color", "white");
  $("#reset").css("box-shadow", "2px 2px black")
  $("#reset").css("border", "2px solid black")

});
