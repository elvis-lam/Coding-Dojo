$(document).ready(function() {

// Using the data alt src to call the alternate of the images
  $("img").click(function() {
    var pic = $(this).attr("data-alt-src");
    $(this).attr('data-alt-src', $(this).attr("src"));
    $(this).attr("src", pic);
  });

// Styling the images
  $("img").css("display", "block")
  $("img").css("padding-bottom", "5px")


});
