$(document).ready(function(){

  // Add the font of the class to be red
  $("#add-c").click(function() {
    $(".red").css("color", "red");
  });

  // When button is pressed, the image toggles
  $("#toggle").click(function() {
    $(".anges").toggle();
  });

  // The paragraph will modify and append
  $("#new-paragraph").click(function() {
    $(".append-p").append("<p>I just made this paragraph better!</p>");
  });

  // This will hide the image
  $("#hides").click(function() {
    $(".hiding").hide();
  });

  // This will show the image
  $("#appearing").click(function() {
    $("#showing").show("slow", function() {
    });
  });

  // This will allow the image to slide up
  $("#upslide").click(function() {
    $("#sliding-up").slideUp("slow", function() {
    });
  });

  // The image will slide down
  $("#downslide").click(function() {
    $("#sliding-down").slideDown("slow", function() {
    });
  });

  // Fade the image in
  $("#in-fade").click(function() {
    $(".fadings").fadeIn("slow", function() {
    });
  });

  // Fade the image out
  $("#out-fade").click(function() {
    $(".fadeouts").fadeOut("slow", function() {
    });
  });

  // The class prior called will have another p tag added before
  $("#previous").click(function() {
    $(".prior").before("<p>I told you I was here FIRST!</p>")
  });

  // The class next called will have another p tag added after
  $("#afters").click(function() {
    $(".next").after("<p>I told you I'm next</p>");
  });

  // HTML changes all content of the class/ id/ element being called
  $("#magic").click(function() {
    $(".houdini").html("<p>Whooshhh.. Houdini</p>");
  });

// The attribute returns attributes and values of the selected element
  $("#wide").click(function() {
    $("#pushing").attr("width", "100px");
  });

  // The text method sets or returns the text content
  $("#texting").click(function() {
    $(".text-change").text("That was it...");
  });
  // Sets or returns the value attribute of the selected elements
  $("#values").click(function() {
    $("input:text").val("Giggity");
  });

  // The data function stores data associated with the matched elements or return the value at the named
  // data store for the first element in the set of matched elements.
  $("#btn1").click(function(){
    $(".bye").data("Hello World", "Game Over");
  });

  $("#btn2").click(function(){
    alert($(".bye").data("Hello World"));
  });

});
