Microsoft Windows [Version 10.0.17134.48]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Users\Elvis>cd desktop

C:\Users\Elvis\Desktop>cd codingdojo

C:\Users\Elvis\Desktop\CodingDojo>cd css
The system cannot find the path specified.

C:\Users\Elvis\Desktop\CodingDojo>cd webfundamentals

C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals>git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   css/about_python/img/python.png
        new file:   css/about_python/index.html
        new file:   css/about_python/style.css
        renamed:    html/algorithmI.pdf -> css/algorithmI.pdf
        new file:   css/algorithmII.pdf
        new file:   css/javascriptbasics/index.html
        new file:   css/javascriptbasics/style.css
        modified:   css/portfolio/index.html
        modified:   css/portfolio/style.css

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   css/about_python/index.html
        modified:   css/about_python/style.css


C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals>git commit -m "Updated assignments"
[master 97cdc5f] Updated assignments
 9 files changed, 331 insertions(+), 79 deletions(-)
 create mode 100644 WebFundamentals/css/about_python/img/python.png
 create mode 100644 WebFundamentals/css/about_python/index.html
 create mode 100644 WebFundamentals/css/about_python/style.css
 rename WebFundamentals/{html => css}/algorithmI.pdf (100%)
 create mode 100644 WebFundamentals/css/algorithmII.pdf
 create mode 100644 WebFundamentals/css/javascriptbasics/index.html
 create mode 100644 WebFundamentals/css/javascriptbasics/style.css

C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals>git push origin master
Counting objects: 14, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (12/12), done.
Writing objects: 100% (14/14), 127.96 KiB | 15.99 MiB/s, done.
Total 14 (delta 4), reused 0 (delta 0)
To https://gitlab.com/elvis-lam/Coding-Dojo.git
   c842846..97cdc5f  master -> master

C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals>cd css

C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals\css>cd ..

C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals>cd css

C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals\css>dir
 Volume in drive C is Windows
 Volume Serial Number is 0823-C851

 Directory of C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals\css

05/15/2018  07:54 PM    <DIR>          .
05/15/2018  07:54 PM    <DIR>          ..
05/15/2018  06:01 PM    <DIR>          about_python
05/14/2018  01:26 PM            68,670 algorithmI.pdf
05/15/2018  05:57 PM           123,379 algorithmII.pdf
05/14/2018  01:49 PM    <DIR>          blocks
05/14/2018  02:16 PM    <DIR>          display_blocks
05/15/2018  06:02 PM    <DIR>          javascriptbasics
05/14/2018  06:03 PM    <DIR>          javascript_basics
05/15/2018  10:04 AM    <DIR>          portfolio
               2 File(s)        192,049 bytes
               8 Dir(s)  909,970,935,808 bytes free

C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals\css>atom about_python

C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals\css>git clone 97cdc5f98a4bf85a04f4a676a3552f342dd014dc
fatal: repository '97cdc5f98a4bf85a04f4a676a3552f342dd014dc' does not exist

C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals\css>git clone https://gitlab.com/elvis-lam/Coding-Dojo.git
Cloning into 'Coding-Dojo'...
remote: Counting objects: 144, done.
remote: Compressing objects: 100% (131/131), done.

Receiving objects: 100% (144/144), 6.42 MiB | 881.00 KiB/s, done.
Resolving deltas: 100% (13/13), done.

C:\Users\Elvis\Desktop\CodingDojo\WebFundamentals\css>