
C:\Users\Elvis>cd desktop

C:\Users\Elvis\Desktop>cd codingdojo

C:\Users\Elvis\Desktop\CodingDojo>dir
 Volume in drive C is Windows
 Volume Serial Number is 0823-C851

 Directory of C:\Users\Elvis\Desktop\CodingDojo

05/31/2018  07:23 PM    <DIR>          .
05/31/2018  07:23 PM    <DIR>          ..
05/31/2018  07:24 PM    <DIR>          Algorithm
05/14/2018  01:26 PM    <DIR>          PreBootcamp
05/31/2018  07:23 PM    <DIR>          WebFundamentals
               0 File(s)              0 bytes
               5 Dir(s)  915,100,299,264 bytes free

C:\Users\Elvis\Desktop\CodingDojo>git status
On branch master
Your branch is up to date with 'origin/master'.

Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   WebFundamentals/api/pokemon.html
        deleted:    WebFundamentals/css/algorithmI.pdf
        deleted:    WebFundamentals/css/algorithmII.pdf
        deleted:    WebFundamentals/css/algorithmIII.pdf
        modified:   WebFundamentals/css/blocks/index.html
        deleted:    WebFundamentals/css/git.txt
        modified:   WebFundamentals/css/media_query/index.html
        modified:   WebFundamentals/css/media_query/style.css

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        Algorithm/algorithmI.pdf
        Algorithm/algorithmII.pdf
        Algorithm/algorithmIII.pdf
        Algorithm/algorithmIV.js
        WebFundamentals/api/GOT/
        WebFundamentals/api/pokedex.html
        WebFundamentals/api/weather.html
        WebFundamentals/css/RWD/
        WebFundamentals/git/

no changes added to commit (use "git add" and/or "git commit -a")

C:\Users\Elvis\Desktop\CodingDojo>git add .

C:\Users\Elvis\Desktop\CodingDojo>git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        renamed:    WebFundamentals/css/algorithmI.pdf -> Algorithm/algorithmI.pdf
        renamed:    WebFundamentals/css/algorithmII.pdf -> Algorithm/algorithmII.pdf
        renamed:    WebFundamentals/css/algorithmIII.pdf -> Algorithm/algorithmIII.pdf
        new file:   Algorithm/algorithmIV.js
        new file:   WebFundamentals/api/GOT/img/baratheon.png
        new file:   WebFundamentals/api/GOT/img/lannister.jpg
        new file:   WebFundamentals/api/GOT/img/stark.jpg
        new file:   WebFundamentals/api/GOT/img/targaryen.jpg
        new file:   WebFundamentals/api/GOT/index.html
        new file:   WebFundamentals/api/pokedex.html
        modified:   WebFundamentals/api/pokemon.html
        new file:   WebFundamentals/api/weather.html
        new file:   WebFundamentals/css/RWD/media_query/index.html
        new file:   WebFundamentals/css/RWD/media_query/style.css
        new file:   WebFundamentals/css/RWD/responsive_blocks/index.html
        new file:   WebFundamentals/css/RWD/responsive_blocks/style.css
        new file:   WebFundamentals/css/RWD/simply_responsive/index.html
        new file:   WebFundamentals/css/RWD/simply_responsive/style.css
        modified:   WebFundamentals/css/blocks/index.html
        modified:   WebFundamentals/css/media_query/index.html
        modified:   WebFundamentals/css/media_query/style.css
        renamed:    WebFundamentals/css/git.txt -> WebFundamentals/git/git.txt
        new file:   WebFundamentals/git/man_git.txt


C:\Users\Elvis\Desktop\CodingDojo>git commit -m'commitassignments'
[master 14d2e20] 'commitassignments'
 23 files changed, 651 insertions(+), 2 deletions(-)
 rename {WebFundamentals/css => Algorithm}/algorithmI.pdf (100%)
 rename {WebFundamentals/css => Algorithm}/algorithmII.pdf (100%)
 rename {WebFundamentals/css => Algorithm}/algorithmIII.pdf (100%)
 create mode 100644 Algorithm/algorithmIV.js
 create mode 100644 WebFundamentals/api/GOT/img/baratheon.png
 create mode 100644 WebFundamentals/api/GOT/img/lannister.jpg
 create mode 100644 WebFundamentals/api/GOT/img/stark.jpg
 create mode 100644 WebFundamentals/api/GOT/img/targaryen.jpg
 create mode 100644 WebFundamentals/api/GOT/index.html
 create mode 100644 WebFundamentals/api/pokedex.html
 create mode 100644 WebFundamentals/api/weather.html
 create mode 100644 WebFundamentals/css/RWD/media_query/index.html
 create mode 100644 WebFundamentals/css/RWD/media_query/style.css
 create mode 100644 WebFundamentals/css/RWD/responsive_blocks/index.html
 create mode 100644 WebFundamentals/css/RWD/responsive_blocks/style.css
 create mode 100644 WebFundamentals/css/RWD/simply_responsive/index.html
 create mode 100644 WebFundamentals/css/RWD/simply_responsive/style.css
 rename WebFundamentals/{css => git}/git.txt (100%)
 create mode 100644 WebFundamentals/git/man_git.txt

C:\Users\Elvis\Desktop\CodingDojo>git push origin
Counting objects: 31, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (31/31), done.
Writing objects: 100% (31/31), 2.91 MiB | 566.00 KiB/s, done.
Total 31 (delta 4), reused 0 (delta 0)
To https://gitlab.com/elvis-lam/Coding-Dojo.git
   09a639e..14d2e20  master -> master

C:\Users\Elvis\Desktop\CodingDojo>