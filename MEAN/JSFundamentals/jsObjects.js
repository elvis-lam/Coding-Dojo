// Write a function that accepts an array of student objects, as shown below. Print all of the students' names and their cohorts.

let students = [
    {name: 'Remy', cohort: 'Jan'},
    {name: 'Genevieve', cohort: 'March'},
    {name: 'Chuck', cohort: 'Jan'},
    {name: 'Osmund', cohort: 'June'},
    {name: 'Nikki', cohort: 'June'},
    {name: 'Boris', cohort: 'June'}
];

function student(students) {
  for (var i = 0; i < students.length; i++) {
    console.log("Name: " + students[i].name + ", Cohort: " + students[i].cohort);
  }
}
student(students)


// Write a function that accepts an object of users divided into employees and managers, as shown below, and logs the information to the console.

let users = {
    employees: [
        {'first_name':  'Miguel', 'last_name' : 'Jones'},
        {'first_name' : 'Ernie', 'last_name' : 'Bertson'},
        {'first_name' : 'Nora', 'last_name' : 'Lu'},
        {'first_name' : 'Sally', 'last_name' : 'Barkyoumb'}
    ],
    managers: [
       {'first_name' : 'Lillian', 'last_name' : 'Chambers'},
       {'first_name' : 'Gordon', 'last_name' : 'Poe'}
    ]
 };

function people(users) {
  var count = 1
  console.log("EMPLOYEES");
  for (var i = 0; i < users.employees.length; i++) {
    var person = count + " - " + users.employees[i].last_name + ", " + users.employees[i].first_name + " - " + (users.employees[i].first_name.length + users.employees[i].last_name.length);
    console.log(person.toUpperCase());
    count++
  }

  var count = 1
  console.log("MANAGERS");
  for (var i = 0; i < users.managers.length; i++) {
    var person = count + " - " + users.managers[i].last_name + ", " + users.managers[i].first_name + " - " + (users.managers[i].first_name.length + users.managers[i].last_name.length);
    console.log(person.toUpperCase());
    count++
  }
}
people(users)
