// Create 3 instances of the Bike class

class Bike {

  constructor(
    public price: number,
    public max_speed: number,
    public miles: number = 0){
    }
    ride(){
      this.miles += 10;
      console.log("Riding");
    }
    reverse(){
      if (this.miles < 5) {
        this.miles = 0;
      }
      else {
        this.miles -= 5;
      }
      console.log("Reversing");
    }
    displayInfo(){
      console.log("Price: $" + this.price + " | Speed: " + this.max_speed + "mph | Total Miles: " + this.miles)
    }

}

// Have the first instance ride three times, reverse once and have it displayInfo().
var bike1 = new Bike(250, 25);
bike1.ride()
bike1.ride()
bike1.ride()
bike1.reverse()
bike1.displayInfo()

// Have the second instance ride twice, reverse twice and have it displayInfo().
bike2 = new Bike(200, 20);
bike.ride()
bike.ride()
bike.reverse()
bike.reverse()
bike.displayInfo()

// Have the third instance reverse three times and displayInfo().
bike3 = new Bike(100, 5);
bike.reverse()
bike.reverse()
bike.reverse()
bike.displayInfo()
