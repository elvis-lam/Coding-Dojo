const express = require('express'),
  path = require('path'),
  app = express(),
  mongoose =require('mongoose'),
  bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static(__dirname, '/public/dist/public'));


mongoose.connect('mongodb://localhost/people');

const PeopleSchema = new mongoose.Schema({
  name: {type: String}
}, {timestamps: true})

mongoose.model('People', PeopleSchema);
const People = mongoose.model('People');

/**************************************************************************************************************/

// Show all the data in json
// app.get('/', (req, res) => {
//   People.find({}, (err, data) => {
//     res.json(data);
//   });
// });

// Add a new user
app.get('/new/:name', (req, res) => {
  const newName = req.params.name;
  People.create({name: newName}, (err) => {
    if (err) {
      console.log("Error", err);
    }
      console.log("Success Create");
      res.redirect('/');
  });
});

// Remove the inputted user
app.get('/remove/:name', (req, res) => {
  People.remove({name: req.params.name}, (err) => {
    if (err) {
      console.log("Remove Error", err);
    }
      console.log("Success Remove");
      res.redirect('/');
  });
});

// Show the individual inputted
app.get('/:name', (req, res) => {
  People.find({name: req.params.name}, (err, data) => {
    if (err) {
      console.log("Find Error", err);
    }
    res.json(data);
  });
});

app.listen(8000, function() {
  console.log("listening on port 8000");
});
