import { Component, OnInit, Output } from '@angular/core';
import { HttpService } from './http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  // title = 'app';
  newCake: any;
  cakes: any;
  newComment: any;
  comments: any;
  boo: boolean;


  constructor(private _httpService: HttpService) {}
  ngOnInit() {


    this.getCake();
    this.newCake = { name: "", url: "" };
    this.newComment = { rate: "", comment: "" };
    this.boo = false;
  }

  getCake() {
    // console.log("cake cake cake")
    let observable = this._httpService.cake();
    observable.subscribe(data => {
      console.log(data)
      this.cakes = data;
    })
  }


  // cakeProcessor(cake:any) {
  //   this.boo = true;
  //   console.log("in the cake processor", cake);
  //   // let observable = this._httpService.cake()
  //   // observable.subscribe(data => {
  // }


  processComment(cakes: any, newComment) {
    console.log("Cake Component root", cakes);
    let observable = this._httpService.processComment(cakes)
    observable.subscribe(data => {
      console.log('Inside component subscribe root', cakes)
      this.newComment = { rate: "", comment: "" }
      // this.getCake();
    })
  }






  submitCake(newCake: any) {
    // console.log("Submitting")
    let observable = this._httpService.adding(this.newCake)
    observable.subscribe(data => {
      this.newCake = { name: "", url: "" }
      // console.log("Successfully Submitted")
      this.getCake();
    })
  }



}
