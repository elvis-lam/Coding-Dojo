import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpService } from '../http.service';


@Component({
  selector: 'app-cake',
  templateUrl: './cake.component.html',
  styleUrls: ['./cake.component.css']
})

export class CakeComponent implements OnInit {
  @Input() cakes: any;
  @Output() processEventEmitter = new EventEmitter();
  @Output() cakeEventEmitter = new EventEmitter();

  newComment: any;
  boo: boolean;

  constructor(private _httpService: HttpService) { }
  ngOnInit() {
    // this.commentProcess();
    // this.showCake();
    this.boo = false;
  }

  commentProcess(newComment: any) {
    // console.log("Cake child", newComment)
    this.processEventEmitter.emit(newComment)
  }



  showCake(cake: any) {
    this.boo = true;
    console.log(this.boo)
    this.cakeEventEmitter.emit(cake)
    // let observable = this._httpService.cake()
    // observable.subscribe(data => {
    console.log('Child Cake component', cake.comments[0].comment)
    // });
  }

}
