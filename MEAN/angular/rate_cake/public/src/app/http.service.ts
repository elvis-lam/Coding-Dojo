import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _http: HttpClient) {

  }

  cake() {
    // console.log("in the cake")
    return this._http.get('/all')
  }


  adding(newCake: any) {
    // console.log('In the add', newCake)
    return this._http.post('/add', newCake)
  }

  processComment(cake: any) {
    // console.log("Cake Service root", cake._id)
    return this._http.post(`/comment/${cake._id}`, cake)
  }

}
