const mongoose = require('mongoose'),
  fs = require('fs'),
  path = require('path');

mongoose.connect('mongodb://localhost/cakes');

var models_path = path.join(__dirname, './../models');
fs.readdirSync(models_path).forEach( (file) => {
  require(models_path + '/' + file);
});
