 const cakes = require('../controllers/cakes.js')

 module.exports = app => {

   app.get('/all', cakes.all);

   app.post('/comment/:id', cakes.comment);

   app.post('/add', cakes.add);

   app.get('/show/:id', cakes.show);

   app.put('/update/:id', cakes.update);

   app.delete('/delete/:id', cakes.delete);

 }
