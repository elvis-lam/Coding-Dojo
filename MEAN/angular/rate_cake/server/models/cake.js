const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
  rate: {type: Number},
  comment: {type: String}
}, {timestamps: true})

const CakeSchema = new mongoose.Schema({
  name: {type: String},
  url: {type: String},
  comments: [CommentSchema]
}, {timestamps: true})

mongoose.model('Comment', CommentSchema);
mongoose.model('Cake', CakeSchema);
