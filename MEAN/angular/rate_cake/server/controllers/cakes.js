 const mongoose = require('mongoose');
 const Cake = mongoose.model('Cake');
 const Comment = mongoose.model('Comment');

module.exports = {

  // app.get('/', function(req, res) {
  //   Message.find({}, function(err, message) {
  //     if (err) {
  //       console.log('ERROR');
  //     }
  //     else {
  //       Comment.find({_id: req.params.id}, function(err, comment) {
  //         if (err) {
  //           console.log('ERROR, ERRRO');
  //         }
  //         console.log(comment);
  //         res.render('index', { message: message} )
  //       });
  //     }
  //   });
  // });

  add: (req, res) => {
    Cake.create(req.body, (err, data) => {
      if (err) {
        res.json(err);
      }
      else {
        console.log("Successfully Added");
        res.json(data);
      }
    })
  },

  all: (req, res) => {
    Cake.find(req.body, (err, data) => {
      if (err) {
        res.json(err);
      }
      else {
        res.json(data);
      }
    });
  },


  comment: (req, res) => {

    Cake.findOneAndUpdate({_id: req.params.id}, {$push:  {comments: req.body}}, (err, data) => {
        if (err) {
          console.log("update err", err);
        }
        else {
          console.log("Success update", data);
        }
    })

    // Comment.create(req.body, (err, data) => {
    //   console.log("In the comment body", data);
    //   if (err) {
    //     console.log("err comment", err);
    //     res.json(err);
    //   }
    //   else {
    //     console.log("this right here", data, this.cakes)
    //     Cake.findOneAndUpdate({_id: req.params.id}, {$push:
    //     {comments: req.body}}, (err, data) => {
    //       if (err) {
    //         console.log("cake err", err);
    //         res.json(err)
    //       }
    //       else {
    //         console.log("success add", data);
    //         res.json(data);
    //       }
    //     });
    //   }
    // });
  },


  show: (req, res) => {
    Cake.find({_id: req.params.id}, (err, data) => {
      if (err) {
        res.json(err);
      }
      else {
        res.json(data);
      }
    });
  },

  update: (req, res) => {
    Cake.update({_id: req.params.id}, (err, data) => {
      if (err) {
        res.json(err);
      }
      else {
        res.json(data);
      }
    });
  },

  delete: (req, res) => {
    Cake.remove({_id: req.params.id}, (err, data) => {
      if (err) {
        res.json(err);
      }
      else {
        res.json(data);
      }
    });
  },


}
