const express = require('express'),
  bodyParser = require('body-parser'),
  app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public/dist/public'));

require('./server/config/mongoose.js');
require('./server/config/routes.js')(app);

// app.all('*', (req, res) => {
//   res.sendFile(path.resolve('./public/dist/public/index.html'))
// });


app.listen(8888, () =>
  console.log("Listening on port 8888"))
