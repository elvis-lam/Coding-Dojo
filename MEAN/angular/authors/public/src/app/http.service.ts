import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  // add: any;
  constructor(private _http: HttpClient) { }

  getAuthor() {
    console.log("Show in the service")
    return this._http.get('/api/author/show')
  }

  addNew(add: any) {
    console.log("Add service", add);
    return this._http.post('/api/author/add', add)
  }

  delete(id: any) {
    console.log("Deleted Service", id);
    return this._http.delete(`/api/author/delete/${id}`);
  }

  editing(data: any, id) {
    console.log("Edit Service", data, id)
    return this._http.put(`/api/author/edit/${id}`, data);
  }

}
