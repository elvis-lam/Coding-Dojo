import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ShowComponent } from './show/show.component';


const routes: Routes = [
  {path: 'author/add', component: AddComponent},
  {path: 'author/edit/:id', component: EditComponent},
  {path: 'author/show', component: ShowComponent},
  {path: '', pathMatch: 'full', redirectTo: '/author/show'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
