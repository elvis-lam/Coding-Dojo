import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})

export class AddComponent implements OnInit {

  addNew: any;
  error: string;
  constructor(
    private _httpService: HttpService,
    private router: Router,

  ) { }

  ngOnInit() {
    this.addNew = { name: "" }
    // this.addAuthor();
  }

addAuthor(addNew: any) {
  console.log("Add component")
  let observable = this._httpService.addNew(this.addNew);
  observable.subscribe(data => {
    
    if(data.name === 'ValidationError') {
      this.error = data;
    }
    else {
      console.log("Added", data.name)
      this.router.navigate(['/author/show'])
    }
  });
}


}
