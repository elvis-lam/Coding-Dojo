import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {
  authors: any;
  // editing: boolean;
  editing: any;

  constructor(
    private _httpService: HttpService,
    private router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.showAll();
    this._route.params.subscribe((params: Params) => editing = (params['id'])
  }

  showAll() {
    let observable = this._httpService.getAuthor();
    observable.subscribe(data => {
      console.log("Got All after subscribe", data)
      this.authors = data
    })
  }

  deleteAuthor(del: any) {
    console.log('Delete show component', del)
    let observable = this._httpService.delete(del)
    observable.subscribe(data => {
      // console.log('im deleting')
      this.showAll();
    })
  }

  editClick(theId: any, editing) {
    console.log("Edit Component", theId, editing);
    this.router.navigate(['/author/edit/' + theId._id], editing)
  }

}
