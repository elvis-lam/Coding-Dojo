import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Params, Router } from '@angular/router'

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  id: any;
  editing: any;

  constructor(private _httpService: HttpService,
  private _route: ActivatedRoute,
  private router: Router,
  ) { }

  ngOnInit() {
    this.editing = { name: "" };
    this._route.params.subscribe((params: Params) => console.log(params))
    this.id = this._route.params._value.id
  }

  editAuthor(editing: any) {
    console.log("in the edit", editing, this.id)
    let observable = this._httpService.editing(editing, this.id)
    observable.subscribe(data => {
    console.log("Hello edit", data, data.name);
    this.router.navigate(['/author/show'])
    })
  }

}
