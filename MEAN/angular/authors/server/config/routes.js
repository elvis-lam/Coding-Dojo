const authors = require('../controllers/authors.js');
const path = require('path');

module.exports = app => {

  app.get('/api/author/show', authors.all);

  app.post('/api/author/add', authors.add);

  app.put('/api/author/edit/:id', authors.edit);

  app.delete('/api/author/delete/:id', authors.delete);

  app.all('*', (req, res) => {
    res.sendFile(path.resolve('./public/dist/public/index.html'))
  });

}
