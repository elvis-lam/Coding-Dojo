const mongoose = require('mongoose');
const Author = mongoose.model('Author');

module.exports = {

  all: (req, res) => {
    Author.find(req.body, (err, data) => {
      if (err) {
        console.log("All err - controller", err);
        res.json(err)
      }
      else {
        console.log("All success - controller", data);
        res.json(data)
      }
    });
  },

  add: (req, res) => {
    Author.create(req.body, (err, data) => {
      if (err) {
        console.log("Adding err - controller", err);
        res.json(err)
      }
      else {
        console.log("Add success - controller", data);
        res.json(data)
      }
    });
  },

  edit: (req, res) => {
    Author.update({_id: req.params.id}, req.body, (err, data) => {
      console.log(data);
      if (err) {
        console.log("Edit err - controller", err);
        res.json(err)
      }
      else {
        console.log("Edit success - controller", data);
        res.json(data)
      }
    });
  },



  delete: (req, res) => {
    Author.remove({_id: req.params.id}, (err, data) => {
      if (err) {
        console.log("Delete err - controller", err);
        res.json(err)
      }
      else {
        console.log("Delete success - controller", data);
        res.json(data)
      }
    });
  },



}
