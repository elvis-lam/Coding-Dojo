const mongoose = require('mongoose');

const AuthorSchema = new mongoose.Schema( {
  name: {type: String, required: [true, "Name cannot be blank"], minLength: [3, "Name needs 3 or more characters"]}
}, {timestamps: true})

mongoose.model('Author', AuthorSchema);
