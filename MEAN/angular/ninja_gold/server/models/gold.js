const mongoose = require('mongoose');

const GoldSchema = new mongoose.Schema( {
  gold: {type: Number}
}, {timestamps: true})

mongoose.model('Gold', GoldSchema)
