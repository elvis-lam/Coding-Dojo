const golds = require('../controllers/golds.js');

module.exports = app => {
  app.get('/all', golds.all);
}
