const weathers = require('../controllers/weathers.js');
const path = require('path');

module.exports = app => {

  app.get('/api/burbank', weathers.burbank);

  app.get('/api/dallas', weathers.dallas);

  app.get('/api/chicago', weathers.chicago);

  app.get('/api/dc', weathers.dc);

  app.get('/api/sanjose', weathers.sanjose);

  app.get('/api/seattle', weathers.seattle);

  app.all('*', (req, res) => {
    res.sendFile(path.resolve('./public/dist/public/index.html'))
  });

}
