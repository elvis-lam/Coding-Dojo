import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _http: HttpClient) { }

  getBurbank(){
    console.log("in serv burbank")
    return this._http.get('https://api.openweathermap.org/data/2.5/weather?q=Burbank&appid=08614069011a03f99cd6fccd625ad684')
  }

  getChicago(){
    console.log("in serv chicago")
    return this._http.get('https://api.openweathermap.org/data/2.5/weather?q=chicago&appid=08614069011a03f99cd6fccd625ad684')
  }

  getDallas(){
    console.log("in serv dallas")
    return this._http.get('https://api.openweathermap.org/data/2.5/weather?q=dallas&appid=08614069011a03f99cd6fccd625ad684')
  }

  getWashington(){
    console.log("in serv Washington")
    return this._http.get('https://api.openweathermap.org/data/2.5/weather?id=4366164&appid=08614069011a03f99cd6fccd625ad684')
  }

  getSanJose(){
    console.log("in serv sanjose")
    return this._http.get('https://api.openweathermap.org/data/2.5/weather?id=5460459&appid=08614069011a03f99cd6fccd625ad684')
  }

  getSeattle(){
    console.log("in serv Seattle")
    return this._http.get('https://api.openweathermap.org/data/2.5/weather?q=Seattle&appid=08614069011a03f99cd6fccd625ad684')
  }

}
