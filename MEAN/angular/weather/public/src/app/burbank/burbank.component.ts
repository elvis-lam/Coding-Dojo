import { Component, OnInit } from '@angular/core';
import { HttpService } from './../http.service'

@Component({
  selector: 'app-burbank',
  templateUrl: './burbank.component.html',
  styleUrls: ['./burbank.component.css']
})
export class BurbankComponent implements OnInit {
  allWeather: any;
  temp: number;
  average: number;
  min: number;
  max: number;
  status: string;
  name: string;

  constructor(private _httpService: HttpService) { }
  ngOnInit() {
    this.allWeather = [];
    this.burbank();
  }

  burbank() {
    let observable = this._httpService.getBurbank()
    observable.subscribe(data => {

      this.temp = (((Math.floor(data['main'].temp - 273.15) * 9/5) +32));
      this.min = (((Math.floor(data['main'].temp_min - 273.15) * 9/5) +32));
      this.max = (((Math.floor(data['main'].temp_max - 273.15) * 9/5) +32));
      this.average = ((Math.floor(this.min + this.max)/ 2));


      this.name = data['name'];
      this.status = data['weather'][0].description;
      console.log("Burbank!!", this.status, data)
      this.allWeather.push(this.name);
      this.allWeather.push(this.temp);
      this.allWeather.push(this.min);
      this.allWeather.push(this.max);
      this.allWeather.push(this.average);
      this.allWeather.push(this.status)
      console.log(this.allWeather);
    });
  }
}
