const express = require('express'),
  bodyParser = require('body-parser'),
  path = require('path'),
  app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public/dist/public'));

// require('./server/config/mongoose.js');

app.all('*', (req, res) => {
  res.sendFile(path.resolve('./public/dist/public/index.html'))
});

app.listen(8000, () =>
  console.log("Listening on port 8000"));
