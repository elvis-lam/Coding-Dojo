import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// @Injectable({
//   providedIn: 'root'
// })
@Injectable()
export class HttpService {

  constructor(private _http: HttpClient) {
    this.getPokemon();
  }

  getPokemon() {
    var chlorophyll = 0;
    var overgrow = 0;
    for( var i = 1; i < 10; i++) {
      let pokemon = this._http.get('https://pokeapi.co/api/v2/pokemon/' + i);
      if ( i === 1 ) {
        pokemon.subscribe(data =>
          console.log(data, data.name + " abilities are " + data.abilities[0].ability.name + " and " + data.abilities[1].ability.name ));
      }
      pokemon.subscribe(data =>
        if (data.abilities[0].ability.name == "chlorophyll" || data.abilities[1].ability.name == "chlorophyll") {
          chlorophyll++
        }
        if (data.abilities[0].ability.name == "overgrow" || data.abilities[1].ability.name == "overgrow") {
          overgrow++
        }

        console.log(chlorophyll + " Pokemon have the ability chlorophyll");
        console.log(overgrow + " Pokemon have the ability overgrow");
      )
      // console.log(chlorophyll);
    }
  }
  // console.log(overgrow);

}
