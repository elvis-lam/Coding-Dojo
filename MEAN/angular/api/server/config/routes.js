const tasks = require('../controllers/tasks.js');

module.exports = app => {

  app.get('/all', tasks.all);

  app.post('/add', tasks.add);

  app.get('/show/:id', tasks.show);

  app.put('/update/:id', tasks.update);

  app.delete('/delete/:id', tasks.delete);
}
