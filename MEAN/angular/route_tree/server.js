const express = require('express'),
  bodyParser = require('body-parser'),
  app = express(),
  path = require('path');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public/dist/public'));


app.all('*', (req, res) => {
  res.sendFile(path.resolve('./public/dist/public/index.html'))
});

app.listen(8000, () => {
  console.log("Listening on port 8000");
})
