import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { BrandComponent } from './brand/brand.component';
import { CategoryComponent } from './category/category.component';

import { ReviewComponent } from './review/review.component';
import { ReviewDetailComponent } from './review-detail/review-detail.component';
import { AuthorComponent } from './author/author.component';
import { AllreviewsComponent } from './allreviews/allreviews.component';

const routes: Routes = [
  { path: 'product', component: ProductComponent, children: [
    { path: 'details/:id', component: ProductDetailComponent},
    { path: 'brand/:brand', component: BrandComponent},
    { path: 'category/:cat', component: CategoryComponent}]
  },
  { path: 'review', component: ReviewComponent, children: [
    { path: 'details/:id', component: ReviewDetailComponent},
    { path: 'author/:id', component: AuthorComponent},
    { path: 'all/:id', component: AllreviewsComponent}]
  },
  { path: '', pathMatch: 'full', redirectTo: '/product'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
