import { Component, OnInit} from '@angular/core';
import { HttpService } from './http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // @Input() recordToShow: any;
  newTask: any;
  tasks: any;
  id: string;
  task: any;
  editing: any;
  selecting: any;

  constructor(private _httpService: HttpService) {}
  ngOnInit() {
    // this.getRecords();
    // this.getDetails();
    this.newTask = { title: "", description: ""}
  }

  getRecords() {
    // console.log("hi")
    let obs = this._httpService.getRecord();
    obs.subscribe(data => {
      console.log(data)
      this.tasks = data;
      // this.recordToShow();
    })
  }

// If we want to Grab the Record and show by ID

  // showThisRecord(taskId: string) {
  //   // console.log('Here in Details', taskId)
  //   this.id = taskId
  //   let obs = this._httpService.showRecord(this.id);
  //   obs.subscribe(data => {
  //     // console.log("Details", data)
  //     this.tasks = data
  //     // console.log("Got it", data[0].title)
  //   })
  // }

// Delete a Record
  deleteRecord(taskId: string) {
    console.log('Here in Delete', taskId);
    this.id = taskId
    let obs = this._httpService.removeRecord(this.id);
    console.log("Delete me");
    obs.subscribe(data => {
      console.log("Deleting");
      this.getRecords();
    })
  }

// Adding a Record
  addThisRecord(newTask: any) {
    // console.log(newTask)
    let obs = this._httpService.addRecord(this.newTask);
    obs.subscribe(data => {
      this.newTask = { title: "", description: "" }
      console.log("Success", data)
      this.getRecords();
      this.editing = null;
    })
  }

// Clicking the button of an edit
  editRecord(taskId: any) {
    console.log("Edit Here", taskId)
    this.editing = { ...taskId};
  }

// Submitting the edit
  submitEdit() {
    let obs = this._httpService.editRecord(this.editing)
    obs.subscribe(data => {
      console.log(data);
      this.getRecords();
      // null will set to empty after complete
      this.editing = null;
    })
  }

  show(record) {
    console.log('hi', record)
    this.selecting = { ...record};
     this.selecting = true;
    // let obs = this._httpService.showRecord(this.selected);
    //  obs.subscribe(data => {
    //    console.log("Showing", data)
    // })
  }


}
