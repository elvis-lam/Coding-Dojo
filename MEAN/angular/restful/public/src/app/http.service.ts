import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
// @Injectable()
export class HttpService {

  constructor(private _http: HttpClient) {
    this.getRecord();
    // this.getTasks(id);
    // this.addRecord();
  }

  getRecord() {
      return this._http.get('/all')
  }

  // getTask(id) {
  //   // console.log("this is the " + id)
  //   return this._http.get('/show/' + id)
  // }

  removeRecord(id) {
    // console.log("this is the " + id)
    return this._http.delete('/delete/' + id)
  }

  addRecord(newTask: any) {
    // console.log("hey, add me", newTask)
    return this._http.post('/add', newTask)
  }

  editRecord(edit: any) {
    console.log("I'm in the edit", edit)
    return this._http.put(`/update/${edit._id}`, edit)
  }

  showRecord(id: any) {
    console.log("Showing the", id)
    return this._http.get('/show/' + id)
  }

}
