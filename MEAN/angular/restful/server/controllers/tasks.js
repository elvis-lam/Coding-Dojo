const mongoose = require('mongoose');
const Task = mongoose.model('Task');

module.exports = {

  all: (req, res) => {
    Task.find(req.body, (err, data) => {
      if (err) {
        console.log(err);
      }
      else {
        res.json(data);
      }
    })
  },

  add: (req, res) => {
    Task.create(req.body, (err,data) => {
      if (err) {
        console.log(err);
        }
      else {
        res.json(data);
      }
    })
  },

  show: (req, res) => {
    Task.find({_id: req.params.id}, (err, data) => {
      if (err) {
        console.log(err);
      }
      else {
        res.json(data);
      }
    });
  },

  update: (req, res) => {
    Task.update({_id: req.params.id}, req.body, (err, data) => {
      if (err) {
        console.log(err);
      }
      else {
        console.log("Success Update");
        res.json(data)
      }
    });
  },

  delete: (req, res) => {
    Task.remove({_id: req.params.id}, (err, data) => {
      if (err) {
        console.log(err);
      }
      else {
        console.log("Sucessfully Deleted");
        res.json(data)
      }
    });
  }

}
