import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private _http: HttpClient) {

  }

    newProduct(product: any) {
      console.log("In the Service New", product)
      return this._http.post('/api/products/new', product)
    }

    allProducts() {
      console.log("All product Service");
      return this._http.get('api/products');
    }

    delete(d_id) {
      console.log("Deleting Service", d_id);
      return this._http.delete(`/api/products/delete/${d_id}`)
    }

    edit(edit, editing) {
      console.log("In the Edit Services", edit, editing)
      return this._http.put(`/api/products/edit/${editing}`, edit)
    }
}
