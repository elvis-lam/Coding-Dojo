import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main.component';
import { EditComponent } from './edit/edit.component';
import { NewComponent } from './new/new.component';
import { ProductComponent } from './product/product.component';

const routes: Routes = [
  { path: 'main', component: MainComponent },
  { path: 'products', component: ProductComponent},
  { path: 'products/edit/:id', component: EditComponent },
  { path: 'products/new', component: NewComponent },
  { path: '', pathMatch: 'full', redirectTo: '/main'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
