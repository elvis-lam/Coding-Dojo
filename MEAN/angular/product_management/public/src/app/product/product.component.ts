import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service'
import { Router, Params, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  allProduct: any;
  edit: any;

  constructor(
    private _httpService: HttpService,
    private router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.showProduct()
    this._route.params.subscribe((params: Params) => this.edit = params['id'])
  }

  showProduct() {
    this._httpService.allProducts().subscribe(data => {
      console.log("all products", data)
      this.allProduct = data;
    })
  }

  editClick(edit: any) {
    console.log("In the Product Component Edit", edit);
    this.router.navigate(['/products/edit/' + edit._id])
  }

  deleteProduct(del: any) {
    console.log("Delete Button Clicked", del)
    this._httpService.delete(del).subscribe(data => {
      console.log("Deleting Component", data)
      this.showProduct();
    })
  }

}
