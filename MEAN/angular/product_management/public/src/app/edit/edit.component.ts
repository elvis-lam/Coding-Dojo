import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, Params, ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  edit: any;
  editing: any;
  cake: Object;

  constructor(
    private _httpService: HttpService,
    private router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.edit = {title: "", price: "", image: ""}
    this._route.params.subscribe((params: Params) => this.editing = params['id'])
    // this.cake = {};
    // [(ngModel)]="cake.name"

  }

  deleteProduct(del: any) {
    console.log("Delete Button Clicked", del)
    this._httpService.delete(del).subscribe(data => {
      console.log("Deleting Component", data)
      // this.showProduct();
    })
  }

  editProduct(edit: any, this.editing) {
    console.log("In the Edit Components", edit, this.editing)
    this._httpService.edit(edit, this.editing).subscribe(data => {
      console.log("Finish the Edit", data)
      this.edit = {title: "", price: "", image: ""}
    })
  }

}
