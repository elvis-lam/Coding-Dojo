import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service'

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  product: any;
  errors = [];
  constructor(private _httpService: HttpService) { }

  ngOnInit() {
    this.product = {title: "", price: "", image: ""}
  }

  addProduct() {
    // console.log("In the new Component")
    this._httpService.newProduct(this.product).subscribe(data => {
      if(data['errors']) {
        for (var key in data['errors']) {
          console.log(data['errors'][key]['message']);
          this.errors.push(data['errors'][key]['message']);
        }
      }
      else {
        this.product = {title: "", price: "", image: ""}
        console.log("Success", data)
      }
    })
  }

}
