const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema( {
  title: {type: String, required: [true, "Title cannot be blank"], minLength: [4, "Title must have minimum of 4 characters"]},
  price: {type: Number, required: [true, "Price cannot be blank"]},
  image: {type: String, required: [true, "Image URL cannot be blank"]}
}, {timestamps: true})

mongoose.model('Product', ProductSchema);
