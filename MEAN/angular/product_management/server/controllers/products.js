const mongoose = require('mongoose');
const Product = mongoose.model('Product');

module.exports = {
  main: (req, res) => {
    Product.find(req.body, (err, data) => {
      if (err) {
        console.log("Main Err", err);
        res.json(err);
      }
      else {
        console.log("Success Main", data);
        res.json(data);
      }
    });
  },

  add: (req, res) => {
    Product.create(req.body, (err, data) => {
      if (err) {
        console.log("Add Err", err);
        res.json(err);
      }
      else {
        console.log("Success Add", data);
        res.json(data);
      }
    });
  },


  product: (req, res) => {
    Product.find(req.body, (err, data) => {
      if (err) {
        console.log("Show All Product Err", err);
        res.json(err);
      }
      else {
        console.log("Success Show All Product", data);
        res.json(data);
      }
    });
  },


  edit: (req, res) => {
    Product.findOneAndUpdate({_id: req.params.id}, req.body, (err, data) => {
      if (err) {
        console.log("Edit Err", err);
        res.json(err);
      }
      else {
        console.log("Success Edit", data);
        res.json(data);
      }
    });
  },


  delete: (req, res) => {
    Product.remove({_id: req.params.id}, (err, data) => {
      if (err) {
        console.log("Delete Err", err);
        res.json(err);
      }
      else {
        console.log("Success Delete", data);
        res.json(data);
      }
    });
  },



}
