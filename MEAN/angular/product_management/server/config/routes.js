const products = require('../controllers/products.js');
const path = require('path');

module.exports = app => {

  app.get('/api/main', products.main);

  app.post('/api/products/new', products.add);

  app.get('/api/products', products.product);

  app.put('/api/products/edit/:id', products.edit);

  app.delete('/api/products/delete/:id', products.delete)

  app.all("*", (req,res,next) => {
    res.sendFile(path.resolve("./public/dist/public/index.html"))
  });

}
