// require express
var express = require("express");
// path module -- try to figure out where and why we use this
var path = require("path");
// moudel for session to be used
var session = require('express-session');
// create the express app
var app = express();
// use session
app.use(session({
  secret: "thisTheSecret",
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000}
}))

app.use(express.static(path.join(__dirname, "./static")));

// setting up ejs and our views folder
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

// root route to render the index.ejs view
app.get('/', function(req, res) {
  if (!req.session.counts) {
    req.session.counts = 0
  }
  req.session.counts++
  console.log(req.session.counts);
  res.render("index", {count: req.session.counts});
})

app.post('/process', function(req, res) {
  req.session.counts += 1
  res.redirect('/');
})


app.post('/clear', function(req, res) {
  req.session.destroy();
  res.redirect('/');
})

// tell the express app to listen on port 9000
app.listen(9000, function() {
 console.log("listening on port 9000");
});
