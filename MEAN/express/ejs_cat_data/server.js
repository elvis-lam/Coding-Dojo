var express = require('express');
var app = express();
var path = require('path');

app.use(express.static(path.join(__dirname + "/static")));
app.use(express.static(path.join(__dirname + "/images")));
app.use(express.static(path.join(__dirname + "/stylesheets")));

app.set('views'), path.join(__dirname + '/views');

app.set('view engine', 'ejs');

app.get('/cats', function(req, res) {
  res.render('cats');
})

app.get('/cuddles', function(req, res) {
  var cats_array = [
    {photo: '/images/cat0.jpg', name: "Winkles", favorite_food: "Pizza", age: 3, sleeping_spots: ["Under the basement", " in a hammock"]},
  ];
  res.render('details', {cat: cats_array});
})

app.get('/wrinkles', function(req, res) {
  var cats_array = [
    {photo: '/images/cat1.jpg', name: "Winkles", favorite_food: "Dookie", age: 3, sleeping_spots: ["With the fishes", " in a boat"]}
  ];
  res.render('details', {cat: cats_array});
})

app.get('/kathy', function(req, res) {
  var cats_array = [
    {photo: '/images/cat2.jpg', name: "Kathy", favorite_food: "Chocolate", age: 7, sleeping_spots: ["Under the sun", " with my woes"]}
  ];
  res.render('details', {cat: cats_array});
})

app.listen(8000, function() {
  console.log("listening to port 8000");
})
