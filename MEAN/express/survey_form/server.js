var express = require("express");
var path = require("path");
var session = require("express-session");
var app = express();
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: true}));

app.use(session( {
  secret: "thisBeBrazy",
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000}
}))

app.use(express.static(path.join(__dirname, "./static")));
app.set('views', path.join(__dirname, './views'));

app.set('view engine', 'ejs');
app.get('/', function(req, res) {
  res.render('index')
})

app.post('/process', function(req, res) {
  req.session.first = req.body.first
  req.session.location = req.body.location
  req.session.language = req.body.language
  req.session.comment = req.body.comment
  res.redirect('results');
})

app.get('/results', function(req, res){
  users = {
    first : req.session.first,
    location : req.session.location,
    language : req.session.language,
    comment : req.session.comment
  }
  res.render('results', {user: users})
})

app.listen(8888, function() {
  console.log("Listening on port 8888");
})
