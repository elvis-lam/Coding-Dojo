var express = require("express");
var number = 0;
var path = require("path");
var app = express();

app.use(express.static(path.join(__dirname + './static')));

app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
  res.render('index')
});

var server = app.listen(8888, function(){});
var io = require('socket.io').listen(server);
io.sockets.on('connection', function (socket) {
	var count = 0;

	socket.on("button_click", function(data){
		count++;
    console.log(count);
		io.emit("update_count", {count: count})
	});

	socket.on("reset", function(data){
		count = 0;
		io.emit("update_count", {count: count})
	});
});
