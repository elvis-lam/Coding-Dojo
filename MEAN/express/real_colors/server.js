var express = require('express');
var path = require('path');
var app = express();

app.use(express.static(path.join(__dirname + './static')));

app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
  res.render('index')
});

var server = app.listen(8000, function(){});
var io = require('socket.io').listen(server);

io.sockets.on('connection', function (socket) {
  socket.on("button_click", function(data) {
    // console.log(data);
    console.log("The color of the background is currently " + data.button);
    io.emit("update_background", { button: data.button});

  })

});
