var express = require('express');
var path = require('path');
var axios = require('axios');
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static(path.join(__dirname, "./static")));
app.set('views', path.join(__dirname, './views'));

app.set('view engine', 'ejs');
app.get('/', function(req, res) {
  res.render('index')
})

app.get('/people', function(req, res) {
  axios.get("https://swapi.co/api/people/?page=")
  .then(data => {
  console.log(data.data.results[0]);
  res.json(data);
  })

  .catch(error => {
    console.log(error);
    res.json(error);
  })
});

app.listen(8000, function() {
  console.log("Listening on port 8000");
})
