var express = require('express');
console.log("Let's find out what the app is", express);

var app = express();
console.log("Let's find out what the app is", app);
var path = require('path');
var bodyParse = require('body-parser');
app.use(bodyParse.urlencoded({extended: true}));

app.use(express.static(path.join(__dirname + "/static")));
app.use(express.static(path.join(__dirname + "./images")));
app.use(express.static(path.join(__dirname + "/stylesheets")));
app.set('views', path.join(__dirname + '/views'));


app.set('view engine', 'ejs');
app.get('/cars', function(req, res) {
  res.render('cars')
})

app.get("/cats", function(req, res) {
  res.render("cats");
})

app.get("/cars/new", function(req, res) {
  res.render("forms");
})

app.listen(8000, function() {
  console.log("listening on port 7000");
})
