var express = require('express');
console.log("Let's find out what the app is", express);

var app = express();
console.log("Let's find out what the app is", app);
// var path = require('path');

app.use(express.static(__dirname + "/static"));
app.use(express.static(__dirname + "./images"));
app.use(express.static(__dirname + "./stylesheets"));

// app.set('views', __dirname + '/views');

// app.set('view engine', 'ejs');
// app.set('view engine', 'html');
app.get('/cars', function(request, response) {
  console.log("The request object", request);
  console.log("The request object", response);
  // response.send("<h1>Hello Cars</h1>");
  response.render('cars')
})

app.get("/cats", function(request, response) {
  console.log("The request object", request);
  console.log("The request object", response);
  response.send("<h1>Hello Cats</h1>");
})

app.listen(7000, function() {
  console.log("listening on port 7000");
})
