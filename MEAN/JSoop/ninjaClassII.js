// Create a new object constructor called Ninja with the following attributes:
//     name
//     health
//     speed (private)
//     strength (private)
// Speed and strength should be 3 by default. Health should be 100 by default.

// Ninja should have the following methods:
//     sayName() - This should log that Ninja's name to the console.
//     showStats() - This should show the Ninja's name, strength, speed, and health.
//     drinkSake() - This should add +10 Health to the Ninja

function Ninja(name, health = 100) {
  var self = this;
  this.name = name;
  this.health = health;
  var speed = 3;
  var strength = 3;
  var privateMethod = function() {
    console.log(self);
  }

  this.sayName = function() {
    console.log("Hello my name is " + this.name);
  }

  this.showStats = function() {
    console.log("Ninja Name: " + this.name +  ", Ninja Health: " + this.health + ", Ninja Speed: " + speed + ", Ninja Strength: " + strength);
    privateMethod();
  }

  this.drinkSake = function() {
    this.health += 10;
  }


// Add a new method to Ninja called .punch(). This method will take another Ninja instance and subtract 5 Health from the Ninja we passed in. Your .punch() should display a console message like the below example.
  this.punch = function() {
    this.health -= 5;
  }

  this.kick = function() {
    this.health -= 15;
  }

}

var blueNinja = new Ninja("Goemon");
var redNinja = new Ninja("Bill Gates");
redNinja.punch(blueNinja);
blueNinja.kick(redNinja);
redNinja.showStats();
blueNinja.showStats();
