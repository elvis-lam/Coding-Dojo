// Create a new object constructor called Ninja with the following attributes:
//     name
//     health
//     speed (private)
//     strength (private)
// Speed and strength should be 3 by default. Health should be 100 by default.

// Ninja should have the following methods:
//     sayName() - This should log that Ninja's name to the console.
//     showStats() - This should show the Ninja's name, strength, speed, and health.
//     drinkSake() - This should add +10 Health to the Ninja

function Ninja(name, health = 100) {
  var self = this;
  this.name = name;
  this.health = health;
  var speed = 3;
  var strength = 3;
  var privateMethod = function() {
    console.log(self);
  }

  this.sayName = function() {
    console.log("Hello my name is " + this.name);
  }

  this.showStats = function() {
    console.log("Ninja Name: " + this.name +  ", Ninja Health: " + this.health + ", Ninja Speed: " + speed + ", Ninja Strength: " + strength);
    privateMethod()
  }

  this.drinkSake = function() {
    this.health += 10
  }

}
var el = new Ninja("elvis")
el.drinkSake()
el.showStats()
