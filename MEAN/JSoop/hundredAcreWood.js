// Let's draw a map of where the different characters live in the Hundred Acre Wood. To program this map in JavaScript, we will need to create many objects to represent the different characters' homes and where they are in relation to each other.

var tigger = { character: "Tigger" };
var pooh = { character: "Winnie the Pooh" };
var piglet = { character: "Piglet" };
var owl = { character: "Owl" };
var christopher = { character: "Christopher Robin" };
var rabbit = { character: "Rabbit" };
var gopher = { character: "Gopher" }
var bees = { character: "Bees" };
var kango = { character: "Kango" };
var eeyore = { character: "Eeyore" };
var heffalumps = { character: "Heffalumps" };

// Connection with pooh and tigger
tigger.north = pooh;
pooh.south = tigger;

// Connection with piglet and tigger
piglet.east = tigger.north
tigger.north.west = piglet

// Connection with piglet and owl
piglet.north = owl
owl.south = piglet

// Connection with pooh and piglet
piglet.east = pooh
pooh.west = piglet

// Connection with pooh and christopher
pooh.north = christopher
christopher.south = pooh

// Connection with pooh and bees
pooh.east = bees
bees.west = pooh

// Connection with bees and tigger
tigger.north.east = bees
bees.east = tigger.north

// Connection with bees and rabbit
bees.north = rabbit
rabbit.south = bees

// Connection with rabbit and gopher
rabbit.east = gopher
gopher.west = rabbit

// Connection with gopher and bees
gopher.west.south = bees
bees.north = gopher.west

// Connection with kango and christopher
christopher.north = kango
kango.south = christopher

// Connection with owl and christopher
christopher.west = owl
owl.east = christopher

// Connection with kango and owl
owl.east.north = kango
kango.south = owl.east

// Connection with christopher and rabbit
rabbit.west = christopher
christopher.east = rabbit

// Connection with kango and rabbit
rabbit.west.north = kango
kango.south = rabbit.west

// Connection with kango and eeyore
kango.north = eeyore
eeyore.south = kango

// Connection with kango and eeyore
heffalumps.west = eeyore
eeyore.east = heffalumps

// Connection with kango and heffalumps
heffalumps.west.south = kango
kango.north = heffalumps.west
