var http = require('http');
var fs = require('fs');
var server = http.createServer(function (request, response) {
  console.log('client request URL: ', request.url);
  // This route should serve a view file called index.html and display a greeting.
  if(request.url === '/') {
    fs.readFile('index.html', 'utf8', function (errors, contents) {
      response.writeHead(200, {'Content-Type': 'text/html'});
      response.write(contents);
      response.end();
    })
  }
  // This route should serve a view file called ninjas.html and display information about ninjas.
  else if (request.url === "/ninjas") {
    fs.readFile('ninjas.html', 'utf8', function (errors, contents) {
      response.writeHead(200, {'Content-type': 'text/html'});
      response.write(contents);
      response.end();
    });
  }

  // This route should serve a view file called dojos.html and have a form
  else if (request.url === "/dojos/new") {
    fs.readFile('dojos.html', 'utf8', function (errors, contents) {
      response.writeHead(200, {'Content-type': 'text/html'});
      response.write(contents);
      response.end();
    });
  }
  else {
    response.writeHead(404);
    response.end('URL requested is not available');
  }
});

server.listen(7000);

console.log("Running in localhost at port 7000");
