const mongoose = require('mongoose'),
  Task = mongoose.model('Task');

function Tasks() {
  this.index = (req, res) => {
    const task = Task.find({}, (err, data) => {
      if (err) {
        console.log('Error');
      }
      res.json(data)
    });
  },



  this.add = (req, res) => {

    Task.insertMany({title: req.params.title, description: req.params.description}, (err, data) => {
      if (err) {
        console.log("Add Error", err);
      }
      else {
        res.json(data);
      }
    });
  },

  this.show = (req, res) => {
    Task.find({_id: req.params.id}, (err, data) => {
      if (err) {
        console.log('Show Error', err);
      }
      res.json(data);
    });
  },

  this.update = (req, res) => {
    Task.update({_id: req.params.id}, {$set: {title: req.params.title, description: req.params.description, completed: req.params.completed}}, (err, data) => {
      if (err) {
        console.log("Update Error");
      }
      console.log("Sucessfully Update");
      res.json(data)
    });
  },

  this.delete = (req, res) => {
    Task.remove({_id: req.params.id}, (err, data) => {
      if (err) {
        console.log("Delete Erro");
      }
      console.log("Successfully Deleted");
      res.json(data)
    });
  }
}
module.exports = new Tasks();
