const mongoose = require('mongoose');
const Task = mongoose.model('Task');

const tasks = require('../controllers/tasks.js');

module.exports = (app) => {

  app.get('/', (req, res) => {
    tasks.index(req, res)
  });

  app.post('/add', (req, res) => {
    tasks.add(req, res)
  });

  app.post('/show/:id', (req, res) => {
    tasks.show(req, res)
  });

  app.put('/update/:id', (req, res) => {
    tasks.update(req, res)
  });

  app.delete('/delete/:id', (req, res) => {
    tasks.delete(req, res)
  });

}
