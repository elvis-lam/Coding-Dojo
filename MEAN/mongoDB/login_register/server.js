var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('express-flash');
var bcrypt = require('bcrypt-nodejs');

app.use(flash());

app.use(session( {
  secret: 'logRegisterSecret',
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000}
}))

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, './static')));

app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/user');

var validateEmail = function(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

/**************************************************************************************************************************************************************/

const UserSchema = new mongoose.Schema({
  first_name: {type: String, required: [true, "First name is required"], minlength: [3, "First name must have at least 3 characters"]},
  last_name: {type: String, required: [true, "Last name is required"], minlength: [2, "Last name must have at least 2 characters"]},

  email: {type: String, required: [true, "E-mail is required"], validate: [validateEmail, "Invalid E-mail address"], unique:  [true, "E-mail is already taken"]},
  birthday: {type: Date, required: [true, "Birthday is required"], default: Date.now},

  password: {type: String, required: [true, "Password cannot be blanked"], minlength: [8, "Password must have at least 8 - 255 characters"], maxlength: [255, "Password must have at least 8 - 255 characters"], validate: { validator: (value) => {
    return value;
  }}},

  confirm_password: {type: String, validate: { validator: (value) => {
    return value == this.password;
  }, message: "Passwords must match"}},

}, {timestamps: true});

mongoose.model('User', UserSchema);
const User = mongoose.model('User');

UserSchema.pre('save', (next) => {
    console.log(next);
    var hashed_password = bcrypt.hashSync(this.password, bcrypt.genSaltSync(10));
    this.password = hashed_password;
    this.confirm_password = undefined;
    next();
});
/**************************************************************************************************************************************************************/

app.get('/', (req, res) => {
  User.find({}, (err, user) => {
    if (err) {
      console.log('Error');
      res.redirect('/')
    };
    console.log('Success');
    res.render('index')
  });
});




app.post('/process_register', (req, res) => {
  console.log("POST DATA", req.body);
  var user = new User(req.body);

  user.save(err => {
    console.log('Winning');

    if (err) {
      console.log("ERROR");
      for (var key in err.errors) {
        req.flash('registration', err.errors[key].message);
      }
      res.redirect('/')
    }

    else {
      console.log("Successful");
      req.session.name = req.body.first_name + " " + req.body.last_name
      req.session._id = res.id;
      console.log(req.session.name);
      console.log(req.session._id);
      res.redirect('/')
    }

  });
});

app.post('/process_login', (req, res) => {
  console.log("POST Data", req.body);
  User.findOne({email: req.body.email}, function(err, user){
      if(err){
          console.log('No such User, try registering',err)
      }
      else {
        if(!user){
          console.log("Nope")
        }
        else {
          console.log(bcrypt.compareSync(req.body.password, user.password))
         if(bcrypt.compareSync(req.body.password, user.password) != true){
            err;
            console.log("No match")
         } else {
          return res.redirect('users')
          }
        }
      }
  })
})


app.listen(8000, () => {
  console.log("Listening on port 8000");
})
