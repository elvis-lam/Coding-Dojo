var mongoose = require('mongoose');
var Quote = mongoose.model('Quote');

module.exports = {
  index: (req, res) => {
    Quote.find({}, function(err, quotes) {
      if(err){
        console.log(err);
      }
      else {
        console.log("Got em", quotes)
      }
    });
    res.render('index');
  },

  create: (req, res) => {
    // console.log("POST DATA", req.body);
    var quote = new Quote(req.body);
    // console.log(quote);
    quote.save(function(err) {
      // console.log("Saving quote now");
      if(err) {
        // console.log("We have an error", err);
        for (var key in err.errors) {
          req.flash('registration', err.errors[key].message);
        }
        res.redirect('/');
      }
      else {
        console.log("Successfully entered");
        res.redirect('/quotes')
      }
    });
  },

  show: (req, res) => {
    // var quotes = Quote.find();
    // var data = JSON.stringify(quotes)
    Quote.find({}, function(err, quotes) {
      console.log(quotes);
      res.render('quote', { quotes: quotes })
    });
  }
}
