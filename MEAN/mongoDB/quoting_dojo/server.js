var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('express-flash');
var mongoose = require('mongoose');

app.use(flash());

app.use(session( {
  secret: "thisSomeQuotes",
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000}
}))

app.use(bodyParser.urlencoded({extended: true }));
app.use(express.static(path.join(__dirname, './client/static')));

app.set('views', path.join(__dirname,'./client/views'));
app.set('view engine', 'ejs');
// mongoose.connect('mongodb://localhost/quotes');
require('./server/models/quote.js')
require('./server/config/routes.js')(app);
require('./server/config/mongoose.js');

app.listen(8000, function() {
  console.log("Listening on port 8000");
})
