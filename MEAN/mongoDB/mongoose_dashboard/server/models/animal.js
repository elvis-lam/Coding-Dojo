var mongoose = require('mongoose');

var AnimalSchema = new mongoose.Schema({
  animal: {type: String, required: true, minlength: 3},
  health: {type: String, required: true},
  age: {type: Number, required: true},
  description: {type: String, required: false, minlength: 10}
}, {timestamps: true })

mongoose.model('Animal', AnimalSchema);
