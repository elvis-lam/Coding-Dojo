var mongoose = require('mongoose');
var Animal = mongoose.model('Animal');

// Display all the information of the exisitng animals in the database
module.exports = {
  index: (req, res) => {
    var animal = Animal.find({}, function(err, animal) {

      if (err) {
        console.log('ERROR');
      }
      else {

        // console.log(animal);
        console.log(req.body);
      }
      res.render('index', { animal: animal })
    });
  },

  find: (req, res) => {
    Animal.find({}, function(err, animal) {
      if(err) {
        console.log(err);
      }
      else {
        console.log("Got em", animal);
      }
    });
    res.render('new');
  },

  // Insert/ Add the Animal's information to the database
  add: (req, res) => {
    // console.log("POST DATA", req.body);

    var animal = new Animal(req.body);
    // console.log(animal);

    animal.save(function(err){
      // console.log("Saving animal now");
      if (err) {
        // console.log("We have an error", err);
        for (var key in err.errors) {
          req.flash('regisration', err.errors[key].message);
        }
        res.redirect('/')
      }
      else {
        console.log("Successfully entered");
        res.redirect('/')
      }
    });
  },

  // Grab the Animal's ID from database
  edit: (req, res) => {
    Animal.find({_id: req.params.id}, function(err, animals) {
      console.log(animals);
      if (err) {
        console.log('ERROR');
      }
      res.render('edit', { animals: animals })
    });
  },

  // Updating the Animal from the database
  update: (req, res) => {
    Animal.update({_id: req.params.id}, {$set: {animal: req.body.animal, age: req.body.age, description: req.body.description}}, (err,data)=>{
      if (err) {
        console.log('ERROR');
      }
      res.redirect('/')
    });
  },

  // Deleteing the Animal from the database
  delete: (req, res) => {
    Animal.deleteOne({_id: req.params.id}, (err,data)=>{
      if (err) {
        console.log('ERROR');
      }
      res.redirect('/')
    });
  }
}
