var mongoose = require('mongoose');
var Animal = mongoose.model('Animal');

var animals = require('../controllers/animals.js');

module.exports = function(app) {

  // Display all the information of the exisitng animals in the database
  app.get('/', function(req, res) {
    animals.index(req, res)
  });

  app.get('/new', function(req, res) {
    animals.find(req, res)
  });

  // Insert/ Add the Animal's information to the database
  app.post('/process_animal', function(req, res) {
    animals.add(req, res)
  });

  // Grab the Animal's ID from database
  app.get('/edit/:id', function(req, res) {
    animals.edit(req, res)
  });

  // Updating the Animal from the database
  app.post('/update/:id', function(req,res) {
    animals.update(req, res)
  })

  // Deleteing the Animal from the database
  app.get('/delete/:id', function(req,res) {
    animals.delete(req, res)
  })
}
