var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('express-flash');
var mongoose = require('mongoose');

app.use(flash());

app.use(session( {
  secret: "mySecretAnimal",
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000}
}))

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, './client/static')));

app.set('views', path.join(__dirname, './client/views'));
app.set('view engine', 'ejs');

require('./server/models/animal.js');
require('./server/config/mongoose.js');
require('./server/config/routes.js')(app);

app.listen(8888, () => {
  console.log("Listening on port 8888");
})
