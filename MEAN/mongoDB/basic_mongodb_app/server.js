var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');

app.use(session({
  secret: "thisTheSecret",
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000}
}))


app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname + './static')));

app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

var mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/mongoose');

var UserSchema = new mongoose.Schema({
  first_name: { type: String, required: true, minlength: 6},
  last_name: { type: String, required: true, minlength: 20},
  age: { type: Number, min: 1, max: 150},
  email: { type: String, required: true}
}, {timestamps: true })
mongoose.model('User', UserSchema); // Setting the Schema in our Models as User
var User = mongoose.model('User') // Retrieving this Schema from our Models, name User



app.get('/', function(req, res) {
  User.find({}, function(err, users) {
    // ...create a new instance of the User Schema and save it to the DB.
    var userInstance = new User()
    userInstance.name = 'Andrianaa'
    userInstance.age = 29
    userInstance.save(function(err){
      // This code will run when Mongo has attempted to save the record.
      // If (err) exists, the record was not saved, and (err) contains validation errors.
      // If (err) does not exist (undefined), Mongo saved the record successfully.
    })

  })
  res.render('index')
});

var flash = require('express-flash');
app.use(flash());

app.post('/users', function(req, res) {
  console.log("POST DATA", req.body);
   // create a new User with the name and age corresponding to those from req.body
  var user = new User({ first_name: req.body.first_name, last_name: req.body.last_name, age: req.body.age, email: req.body.email });
    // Try to save that new user to the database (this is the method that actually inserts into the db) and run a callback function with an error (if any) from the operation.
  user.save(function(err) {
    if(err) {
      console.log("Something went wrong", err);
      for (var key in err.errors) {
        req.flash('registration', err.errors[key].message);
      }
      res.redirect('/')
    }
    else {
      // console.log("Successfully added a user!");
      res.redirect('/users')
    }
  });
});

app.listen(8888, function() {
  console.log("Listening on port 8888");
})
