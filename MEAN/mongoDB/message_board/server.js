var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('express-flash');

app.use(flash());

app.use(session( {
  secret: "sssSecrect",
  resave: false,
  saveUninitialized: true,
  cookie: { maxAge: 60000}
}))

app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, './static')));

app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/message');

/**************************************************************************************************************************************************************/

const CommentSchema = new mongoose.Schema({
  name: {type: String, required: [true, "Name is required"], minlength: [3, "Name must have at least 3 characters"]},
  comment: {type: String, required: [true, "Comment must have a comment"], minlength: [10, "Comment must have minimum of 10 characters"], maxlength: [100, "Comment has a  maximum of 100 characters"]}
}, {timestamps: true})

const MessageSchema = new mongoose.Schema({
  name: {type: String, required: [true, "Name is required"], minlength: [3, "Name must have at least 3 characters"]},
  message: {type: String, required: [true, "Message must have a message"], minlength: [10, "Comment must have minimum of 10 characters"], maxlength: [100, "Message has a maximum of 100 characters"]},
  comments: [CommentSchema]
}, {timestamps: true})

mongoose.model('Comment', CommentSchema);
mongoose.model('Message', MessageSchema);
const Comment = mongoose.model('Comment');
const Message = mongoose.model('Message');
mongoose.Promise = global.Promise;

/**************************************************************************************************************************************************************/

app.get('/', function(req, res) {
  Message.find({}, function(err, message) {
    if (err) {
      console.log('ERROR');
    }
    else {
      Comment.find({_id: req.params.id}, function(err, comment) {
        if (err) {
          console.log('ERROR, ERRRO');
        }
        console.log(comment);
        res.render('index', { message: message} )
      });
    }
  });
});

app.post('/process_message', function(req, res) {
  console.log("POST DATA", req.body);
  var message = new Message(req.body);
  console.log(message);

  message.save(function(err){
    console.log("Message here");
    if (err) {
      for (var key in err.errors) {
        req.flash('registration', err.errors[key].message);
      }
      res.redirect('/')
    }
    else {
      console.log("Successful Message");
      res.redirect('/')
    }
  });
});


app.post('/process_comment/:id', function(req, res) {
  Comment.create(req.body, function(err, data) {
    console.log("POST DATA", req.body);
    if(err) {
      console.log('AW SNAP');
    }
    else {
      Message.findOneAndUpdate({_id: req.params.id}, {$push: {comments: data}}, function(err, data) {
        if (err) {
          console.log('MORE ERRORS!');
        }
        else {
          console.log('Success!');
          res.redirect('/');
        }
      });
    }
  });
});

app.listen(8888, function() {
  console.log("Listening on port 8888");
})
